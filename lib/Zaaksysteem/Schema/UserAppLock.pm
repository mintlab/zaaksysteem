package Zaaksysteem::Schema::UserAppLock;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'Zaaksysteem::Result';


=head1 NAME

Zaaksysteem::Schema::UserAppLock

=cut

__PACKAGE__->table("user_app_lock");

=head1 ACCESSORS

=head2 type

  data_type: 'char'
  is_nullable: 0
  size: 40

=head2 type_id

  data_type: 'char'
  is_nullable: 0
  size: 20

=head2 create_unixtime

  data_type: 'integer'
  is_nullable: 0

=head2 session_id

  data_type: 'char'
  is_nullable: 0
  size: 40

=head2 uidnumber

  data_type: 'integer'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "type",
  { data_type => "char", is_nullable => 0, size => 40 },
  "type_id",
  { data_type => "char", is_nullable => 0, size => 20 },
  "create_unixtime",
  { data_type => "integer", is_nullable => 0 },
  "session_id",
  { data_type => "char", is_nullable => 0, size => 40 },
  "uidnumber",
  { data_type => "integer", is_nullable => 0 },
);
__PACKAGE__->set_primary_key("uidnumber", "type", "type_id");


# Created by DBIx::Class::Schema::Loader v0.07010 @ 2014-03-18 11:07:01
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:z86QRo/YsQGCdlj+wonSWQ





# You can replace this text with custom content, and it will be preserved on regeneration
1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

