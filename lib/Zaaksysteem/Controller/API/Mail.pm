package Zaaksysteem::Controller::API::Mail;

use Moose;
use namespace::autoclean;

use File::Basename;
use File::Temp;
use Mail::Track;
use Try::Tiny;

BEGIN { extends 'Zaaksysteem::Controller' }

with qw(MooseX::Log::Log4perl);

=head2 intake

E-mail intake, accepts the MIME message as plain text and processes it depending on the interfaces which are configured.

=cut

sub intake : Chained('/api/base') : PathPart('mail') {
    my ($self, $c) = @_;

    my $params = $c->req->params;
    my $message = exists($c->stash->{message})
        ? $c->stash->{message}
        : $params->{message};

    die('No message found') unless $message;
    my $mailfile = _save_message_as_file($c->model('DB::File'), $message);

    my $outgoing = $c->model('DB::Interface')->search_active({module => 'emailconfiguration'})->first;

    if ($outgoing) {
        $c->stash->{interface} = $outgoing;
        my $config = $outgoing->get_interface_config;
        my $regexp = qr/(\d+-[a-z0-9]{6})/;

        my $mt = Mail::Track->new(
            subject_prefix_name  => $config->{subject},
            identifier_regex     => qr/$regexp/,
        );

        my $msg = $mt->parse($message);

        if ($msg->identifier) {
            return $self->_process_mail($c, $mailfile);
        }
    }

    my $interfaces = $c->model('DB::Interface')->search_active({module => 'emailintake'});
    if ($interfaces->count == 0) {
        $self->log->error("No active mail intake interfaces, detaching to 'intake_process'");
        # Be backward compatible, future e-mail intakes will have specific
        # interfaces based on the username so we can get rid of the
        # eval { rci/yucay } construction below
        $mailfile->delete();
        $mailfile->filestore->delete();
        $c->detach('intake_process');
    }

    my $mt = Mail::Track->new();
    my $msg = $mt->parse($message);
    my ($to) = Email::Address->parse($msg->to);
    $to = lc($to->address);

    my $interface;
    while ($interface = $interfaces->next) {
        my $config = $interface->get_interface_config;
        if (lc($config->{api_user}) eq $to) {
            $c->stash->{interface} = $interface;
            return $self->_process_mail($c, $mailfile);
        }
    }

    my $logmsg = "No e-mail interface found for e-mail address '$to'";
    $self->log->fatal($logmsg);
    die $logmsg;
}

sub _process_mail {
    my ($self, $c, $mailfile) = @_;

    # Mails are now transactionable, so no need for postfix to queue the mail.
    # It will only generate tons of new transactions for the exact same e-mail.
    try {
        $c->stash->{interface}
            ->process_trigger('process_mail', { message => $mailfile });
    }
    catch {
        $self->log->error("Error processing e-mail due to error: $_");
    };

    $c->res->body('ok');
    return 1;
}

=head2 intake_process

Old skool e-mail intake process. Not based on any interface. Just grabs the attachments and adds them to the document queue.

=cut

sub intake_process : Local {
    my ($self, $c) = @_;

    die('No message found') unless $c->req->params->{message};
    eval {
        $c->forward('/api/mail/intake/handle');
    };

    if ($@) {
        $c->log->error('Mail error: ' . $@);
        $c->res->body($@);
    }
    else {
        $c->res->body('ok');
    }
}

=head2 input

Deprecated function. Have a look at L<http://wiki.zaaksysteem.nl> for more information about mail handling by Zaaksysteem.

=cut

sub input : Local {
    my ($self, $c) = @_;
    $c->res->body('Unsupported mail handling, please have a look at http://wiki.zaaksysteem.nl for supported mail handling.');
}

sub _save_message_as_file {
    my $rs  = shift;
    my $msg = shift;
    my $fh = File::Temp->new(UNLINK => 1, SUFFIX => '.mime');
    my $path = $fh->filename;
    print $fh $msg;
    close($fh);
    my ($filename, undef, $ext) = fileparse($path, '\.[^.]*');

    my $admin = $rs->result_source->schema->resultset('Subject')->search(
        {
            username => 'admin',
            subject_type => 'employee',
        }
    )->first;

    my $mailfile = $rs->file_create({
        name       => $filename . $ext,
        file_path  => $path,
        ignore_extension => 1,
        db_params  => {
            created_by => "betrokkene-medewerker-" . $admin->id,
            created_by => 'admin',
            queue      => 0,
        },
    });
    unlink($path);
    return $mailfile->id;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
