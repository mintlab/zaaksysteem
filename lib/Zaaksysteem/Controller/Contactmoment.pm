package Zaaksysteem::Controller::Contactmoment;

use Moose;

use Zaaksysteem::Profiles;
use Zaaksysteem::Tools;

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Contactmoment - Frontend library for the Zaaksysteem
contactmoment structure.

=cut

=head2 create

Create a new contactmoment.

=head3 Arguments

=over

=item TBA

=back

=head3 Location

GET/POST: /contactmoment/create_notitie

A GET will redirect to the form, a POST will attempt to create it.

=head3 Returns

A JSON structure containing contactmoment properties.

=cut

sub create : Local {
    my ($self, $c) = @_;
    my $opts = $c->req->params;


    my $dv = $c->forward('/page/dialog', [{
        validatie           => PROFILE_NOTITIE_CREATE,
        user_permissions    => [qw/contact_nieuw zaak_eigen/],
        template            => 'widgets/contactmoment/notitie/bewerken.tt',
    }]);

    if ($dv) {
        $c->model('DB::Contactmoment')->contactmoment_create({
            type         => $opts->{ type },
            subject_id   => $opts->{ subject_id },
            created_by   => $c->model('DB::Zaak')->current_user->betrokkene_identifier,
            case_id      => $opts->{ case_id },
            medium       => $opts->{ medium },
            message      => $opts->{ content },
        });

        my @logging_params = (
            'subject/contactmoment/create',
            {
                component => $opts->{case_id} ? 'zaak' : 'betrokkene',
                created_for => $opts->{subject_id},
                data        => {
                    case_id => $opts->{case_id} || undef,
                    content => $opts->{content},
                    subject_id      => $opts->{subject_id},
                    contact_channel => $opts->{medium}
                }
            }
        );

        my $event;
        if ($opts->{case_id}) {
            my $case = $c->model('DB::Zaak')->find($opts->{case_id});
            $event = $case->trigger_logging(@logging_params);
        }
        else {
            $event = $c->model('DB::Logging')->trigger(@logging_params);
        }

        $c->push_flash_message($event->onderwerp);
        $c->res->redirect($c->req->referer);
    }
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

