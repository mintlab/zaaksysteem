package Zaaksysteem::DB::Component::Logging::Case::Document::Revert;

use Moose::Role;
use HTML::Entities qw(encode_entities);

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::Document::Replace - Event message
handler for document historic revertion events.

=head1 DESCRIPTION

See L<Zaaksysteem::DB::Component::Logging::Event>.

=head1 METHODS

=head2 onderwerp

Defines the logline for this event. Assumes C<< $self->data >> to have this
structure:

    {
        "original_file_name": "original_file.ext",
        "original_file_version": 1,
        "new_file_name": "new_file.ext",
        "new_file_version": 2
    }

=cut

sub onderwerp {
    my $self = shift;

    return sprintf(
        "Document '%s' (versie %d) toegevoegd. Hersteld op basis van '%s' (versie %d)",
        encode_entities($self->data->{ new_file_name }),
        $self->data->{ new_file_version },
        encode_entities($self->data->{ original_file_name }),
        $self->data->{ original_file_version }
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
