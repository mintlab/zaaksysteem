package Zaaksysteem::BR::Subject::Types::ExternalSubscription;

use Moose::Role;
use Zaaksysteem::Tools;


=head1 NAME

Zaaksysteem::BR::Subject::Types::ExternalSubscription - Bridge specific role for this type

=head1 DESCRIPTION

Applies logic for the L<Zaaksysteem::BR::Subject> bridge for inflating various types.

=cut

=head2 new_from_row

    $object->new_from_dbix(row => $row, password => 'blabla');

Will generate a new authentication object into the database, checking for a unique
loginname, a valid password etc.

=cut


=head2 new_from_related_row

    my $object = Zaaksysteem::Object::Types::Subject->new_from_row($schema->resultset('NatuurlijkPersoon')->first);

=cut

sub new_from_related_row {
    my $class       = shift;
    my $rel_row     = shift;

    return unless $rel_row && $rel_row->result_source->name =~ /^(?:bedrijf|natuurlijk_persoon)$/;

    my $row;
    if ($rel_row->cached_subscriptions) {
        $row = $rel_row->cached_subscriptions->[0];
    } else {
        $row         = $rel_row->result_source->schema->resultset('ObjectSubscription')->search(
            {
                local_table             => $rel_row->result_source->source_name,
                local_id                => $rel_row->id,
            },
            {
                prefetch    => 'interface_id',
            }
        )->first;
    }

    return unless $row;

    return $class->new(
        interface_uuid      => $row->interface_id->uuid,
        external_identifier => ($row->external_id || ''),
        internal_identifier => $row->id,
    );
}

=head2 new_from_params

    $self->new_from_params(%opts)

Exactly the same as C<new>, with the exception that undef C<%opts> will be removed

=cut

sub new_from_params {
    my $class        = shift;
    my %opts        = @_;

    ### Do not allow undef params
    %opts           = map {
        $_ => $opts{$_}
    } grep({ defined $opts{$_} } keys %opts);

    return $class->new(%opts);
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
