package Zaaksysteem::Backend::Sysin::Modules::Roles::StUFPRSNPS;

use Moose::Role;

use Zaaksysteem::StUF;
use Zaaksysteem::Exception;

with qw/
    Zaaksysteem::Backend::Sysin::Modules::Roles::StUF
    Zaaksysteem::BR::Subject::Queue::Person
/;

=head1 NAME

Zaaksysteem::Backend::Sysin::Modules::Roles::STUFPRSNPS - STUFPRSNPS engine for StUF PRS/NPS related queries

=head1 SYNOPSIS

    # See testfile:
    # t/170-sysin-modules-stufprs.t

=head1 DESCRIPTION

STUFPRS engine for StUF PRS related queries

=head1 TRIGGERS

=head2 search($params)

=cut

sub search_prs {
    my $self            = shift;
    my $params          = shift || {};
    my $interface       = shift;

    throw(
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/search_prs/no_params',
        'Cannot search with empty query parameters'
    ) unless scalar keys %{ $params };

    my $module_cfg      = $self->get_config_from_config_iface();
    my $config_module   = $self->config_interface;

    my %certs           = $self->_get_certificates($config_module);


    # Translate the name/key of any given search parameter to their internal equivalent
    my $converted_params;
    my @param_keys = keys %$params;

    for my $attribute (@{ $self->attribute_list }) {
        if (grep {$attribute->{internal_name}} @param_keys) {
            $params->{$attribute->{external_name}} = $params->{$attribute->{internal_name}};
            delete $params->{$attribute->{internal_name}};
        }
    }

    my $stufmsg     = Zaaksysteem::StUF->new(
        entiteittype    => $self->stuf_object_type,
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie => $module_cfg->{mk_ontvanger},
            },
        ),
        soap_ssl_crt => $certs{SSL_cert_file},
        soap_ssl_key => $certs{SSL_key_file},
    )->search(
        {
            reference_id            => 'T' . time(),
            date                    => DateTime->now(),
            %{ $params }
        }
    );

    $stufmsg->load_from_interface($self->config_interface);

    my $reply   = $stufmsg->dispatch;

    my @subjects;
    for (my $i = 0; $i < scalar(@{ $reply->body }); $i++) {
        my $params      = $reply->get_params_for_natuurlijk_persoon($i);
        my $adr_params  = $reply->get_params_for_natuurlijk_persoon_adres($i);

        my %params      = (%$params, %$adr_params);

        $params{$config_module->module_object->get_primary_key($config_module)} = $reply->as_params->[$i]->{$config_module->module_object->get_primary_key($config_module)};

        push(@subjects, \%params);
    }

    return \@subjects;
}

sub import_prs {
    my $self            = shift;
    my $params          = shift || {};
    my $interface       = shift;

    my $config_module                       = $self->config_interface;

    throw(
        'sysin/modules/stuf' . lc($self->stuf_object_type) . '/import_prs/no_params',
        'Cannot import with no param: sleutelGegevensbeheer'
    ) unless $params->{$config_module->get_primary_key};

    my $module_cfg                  = $self->get_config_from_config_iface;

    my %certs  = $self->_get_certificates($config_module);

    my $stufmsg     = Zaaksysteem::StUF->new(
        entiteittype    => $self->stuf_object_type,
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => $module_cfg->{mk_ontvanger},
            },
        ),
        soap_ssl_crt => $certs{SSL_cert_file},
        soap_ssl_key => $certs{SSL_key_file},
    )->search(
        {
            reference_id            => 'T' . time(),
            date                    => DateTime->now(),
            $config_module->module_object->get_primary_key($config_module)   => $params->{$config_module->module_object->get_primary_key($config_module)},
        }
    );

    $stufmsg->load_from_interface($self->config_interface);

    my $reply       = $stufmsg->dispatch;

    $interface->process(
        {
            external_transaction_id => 'unknown',
            input_data              => $reply->parser->xml,
            processor_params        => {
                processor               => '_process_import_entry',
                $config_module->module_object->get_primary_key($config_module)   => $params->{$config_module->module_object->get_primary_key($config_module)}
            },
            direct                  => 1,
        }
    );
}

sub _process_import_entry {
    my $self                        = shift;
    my ($record,$object)            = @_;

    my $params                      = $record
                                    ->transaction_id
                                    ->get_processor_params();

    my $module_cfg                  = $self->get_config_from_config_iface;
    my $config_module               = $self->config_interface;

    my %certs           = $self->_get_certificates($config_module);

    my $stufmsg                     = Zaaksysteem::StUF->new(
        entiteittype    => $self->stuf_object_type,
        stuurgegevens   => Zaaksysteem::StUF::Stuurgegevens->new(
            zender              => {
                applicatie          => 'ZSNL',
            },
            ontvanger           => {
                applicatie          => $module_cfg->{mk_ontvanger},
            },
            kennisgeving => {
                mutatiesoort      => 'T',
                indicatorOvername => 'I',
            }
        ),
        soap_ssl_crt => $certs{SSL_cert_file},
        soap_ssl_key => $certs{SSL_key_file},
    )->set_afnemerindicatie(
        {
            reference_id            => $record->transaction_id->id,
            date                    => $record->transaction_id->date_created,
            $config_module->module_object->get_primary_key($config_module)   => $params->{$config_module->module_object->get_primary_key($config_module)}
        }
    );

    $stufmsg->load_from_interface( $self->config_interface );


    eval {
        my $reply_object = $stufmsg->dispatch;
    };

    if ($@) {
        if (
            UNIVERSAL::isa($@, 'Zaaksysteem::Exception::Base') &&
            $@->type eq 'stuf/soap/no_answer'
        ) {
            throw(
                $@->type,
                "\nResponse:\n" .
                $@->object->response->content
            );
        }
        die ($@);
    }

    my $stuf_prs    = Zaaksysteem::StUF->from_xml(
        $record->transaction_id->input_data,
        {
            cache => ($record->result_source->schema->default_resultset_attributes->{'stuf_cache'} ||= {})
        }
    );

    my $entry       = $self->stuf_create_entry(
        $record, $stuf_prs
    );

    $self->_add_subscription_for_entry(
        $record, $stufmsg, $entry
    );
}



=head1 PROCESSORS

=head2 CREATE SUBJECT

=head2 $module->stuf_create_entry($transaction_record, $rowobject)

Return value: $ROW_NATUURLIJK_PERSOON

Creates a new L<Zaaksysteem::DB::Component::NatuurlijkPersoon> into our database,
and sets a subscription between our data record and theirs via C<ObjectSubscription>

B<Params>

=over 4

=item $transaction_record

The L<Zaaksysteem::Backend::Sysin::Transaction::Component> row. It will be filled
with object_preview, a preview string describing the data object changed.

=item $rowobject

The source of the data, in case of a CSV this would proba

=back

=cut

sub _assert_gemeentecode {
    my $self                        = shift;
    my $record                      = shift;
    my $object                      = shift;
    my $gemeentecode                = shift;

    my $stufversion                 = $self->_get_stuf_version($record->transaction_id->interface_id);
    if ($stufversion eq '0204') {
        if (!$self->get_config_from_config_iface()->{gemeentecode}) {
            $self->stuf_throw(
                $object,
                'sysin/modules/stufprs/stuf_create_entry/no_gemeente_code_set',
                'No gemeentecode found in configuration, please set it'
            );
        }

        if (!$gemeentecode) {
            $self->stuf_throw(
                $object,
                'sysin/modules/stufprs/stuf_create_entry/no_gemeente_code',
                'No gemeentecode found in StUF message, invalid XML'
            );
        }
    }
}

sub stuf_create_entry {
    my $self                = shift;
    my ($record, $object)   = @_;  

    my $natuurlijk_persoon  = $self->_create_subject_natuurlijk_persoon(@_);

    if ($self->got_address(@_)) {
        my $adres               = $self->_create_subject_adres(@_, $natuurlijk_persoon);

        $self->_assert_gemeentecode($record, $object, $adres->gemeente_code)
            if (!$adres->landcode || $adres->landcode == 6030);

        if ($self->_binnen_gemeente($record, $object, $adres)) {
            $natuurlijk_persoon->in_gemeente(1);
        }

        $natuurlijk_persoon->adres_id($adres->id);
        $natuurlijk_persoon->update;
    }

    $record->preview_string(
        "Toegevoegd: " .
        join(', ', grep {defined $_} ($natuurlijk_persoon->burgerservicenummer, $natuurlijk_persoon->voornamen, $natuurlijk_persoon->naamgebruik))
    );

    return $natuurlijk_persoon;
}

=head2 got_address

    if ($stuf->got_address($record, $object)) {
        print "Got a valid address!";
   
    }

B<Return value>: true on success

Returns a true value when subject got an address (either vbl or cor)

=cut

sub got_address {
    my $self                = shift;
    my ($record, $object, $natuurlijk_persoon)   = @_;

    my ($coradres, $vbladres) = $self->_get_cor_vbl_adres(@_, $natuurlijk_persoon);

    if ($self->_has_address($coradres) || $self->_has_address($vbladres)) {
        return 1;
    }

    return;
}

sub _create_subject_natuurlijk_persoon {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $subject_params      = $object->get_params_for_natuurlijk_persoon;

    my $np_rs               = $record
                            ->result_source
                            ->schema
                            ->resultset('NatuurlijkPersoon');

    $subject_params      = {
        map(
            { $_ => $subject_params->{$_} }
            grep (
                { exists $subject_params->{ $_ } }
                $record->result_source->schema->resultset('NatuurlijkPersoon')->result_source->columns
            )
        )
    };

    ### Special case, REMOVE the damn voorloopnul
    $subject_params->{burgerservicenummer} = int($subject_params->{burgerservicenummer});

    ### BACKWARDS compatability
    my $entry               = $np_rs->create(
        {
            %{ $subject_params },
            authenticated       => 1,
            authenticatedby     => 'gba'
        }
    );

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'NatuurlijkPersoon',
                                table_id    => $entry->id,
                                create      => 1
                            );

    $mutation_record->from_dbix($entry);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    return $entry;
}

sub _create_subject_adres {
    my $self                = shift;
    my ($record, $object, $natuurlijk_persoon)   = @_;

    my $address_params      = $object->get_params_for_natuurlijk_persoon_adres;

    ### We need to fix subject in ZS real soon...
    my ($vbladres, $coradres);

    $coradres = { map({ my $adreskey = $_; $adreskey =~ s/^correspondentie_//; $adreskey => $address_params->{$_} } grep ( { $_ =~ /^correspondentie_/ } keys %{ $address_params })) };
    $coradres->{functie_adres} = 'B';

    $vbladres = { map({ $_ => $address_params->{$_} } grep ( { $_ !~ /^correspondentie_/ } keys %{ $address_params })) };
    $vbladres->{functie_adres} = 'W';

    if ($vbladres->{straatnaam} || $vbladres->{adres_buitenland1}) {
        my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                                ->new(
                                    table       => 'Adres',
                                    create      => 1
                                );

        $natuurlijk_persoon->add_address($vbladres, {mutation_record => $mutation_record});

        push(
            @{ $self->process_stash->{row}->{mutations} },
            $mutation_record
        );
    }

    if ($coradres->{straatnaam} || $coradres->{adres_buitenland1}) {
        my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                                ->new(
                                    table       => 'Adres',
                                    create      => 1
                                );

        $natuurlijk_persoon->add_address($coradres, {mutation_record => $mutation_record});

        push(
            @{ $self->process_stash->{row}->{mutations} },
            $mutation_record
        );
    }

    return $natuurlijk_persoon->adres_id;
}

=head2 UPDATE SUBJECT

=head2 $module->stuf_update_entry($record, $object)

Updates a PRS entry in our database

=cut


sub stuf_update_entry {
    my $self                = shift;
    my ($record, $object)   = @_;

    my $natuurlijk_persoon  = $self->get_entry_from_subscription(@_, 'NatuurlijkPersoon');

    $self->_update_subject_natuurlijk_persoon(
        @_, $natuurlijk_persoon
    ) or return $natuurlijk_persoon;        # Return on undef, empty person probably

    my ($coradres, $vbladres) = $self->_get_cor_vbl_adres(@_, $natuurlijk_persoon);

    my $updated_components  = $object->updated_components();

    my ($has_moved);
    if (
        (
            $updated_components->{verblijfsadres} ||            ### There are updates given for these components, else
            $updated_components->{correspondentie}              ### we should not even look at the address
        ) &&
        (
            !$self->_has_address($coradres) && !$self->_has_address($vbladres)
        )
    ) {
        ### No addresses. Maybe this person has moved to the "outside"

        if (
            $natuurlijk_persoon->verblijfsadres &&
            $natuurlijk_persoon->verblijfsadres->gemeente_code
        ) {
            if ($vbladres && $vbladres->{gemeente_code} && $vbladres->{gemeente_code} ne $natuurlijk_persoon->verblijfsadres->gemeente_code) {
                $has_moved = 1;
            }
        }
    }

    ### Update address
    $self->_update_subject_adres(@_, $natuurlijk_persoon);

    $record->preview_string(
        "Bijgewerkt: " .
        join(', ', ($natuurlijk_persoon->burgerservicenummer, $natuurlijk_persoon->voornamen, $natuurlijk_persoon->naamgebruik))
    );

    my $subject_params      = $object->get_params_for_natuurlijk_persoon;

    my $module_cfg                  = $self->get_config_from_config_iface();
    my $config_module               = $self->config_interface;

    ### Make sure entry comes back to live
    if ($natuurlijk_persoon->deleted_on) {
        $natuurlijk_persoon->deleted_on(undef);
        $natuurlijk_persoon->adres_id->deleted_on(undef) if $natuurlijk_persoon->adres_id;

        my $subscription    = $natuurlijk_persoon->subscription_id;
        $subscription->date_deleted(undef);

        $natuurlijk_persoon->adres_id->update if $natuurlijk_persoon->adres_id;
        $natuurlijk_persoon->update;
        $subscription->update;
    }

    ### Special CASE: Overlijden
    if ($subject_params->{datum_overlijden} || $has_moved) {
        ### Person died, let's disconnect afnemerindicatie, and make sure
        ### we immediatly delete this person from our database
        my $object_params       = $object->as_params;

        my $object_subscription = $record
                                ->result_source
                                ->schema
                                ->resultset('ObjectSubscription')
                                ->search(
                                    {
                                        interface_id    => $record
                                                        ->transaction_id
                                                        ->interface_id
                                                        ->id,
                                        local_table     => $self->stuf_subscription_table,
                                        external_id     => $object_params->{$config_module->module_object->get_primary_key($config_module)},
                                        config_interface_id => $self->config_interface->id,
                                    }
                                )->first;

        if (
            $object_subscription &&
            $config_module->module_object->can_connect_to_other_sbus($config_module)
        ) {
            $self
                ->process_stash
                ->{transaction}
                ->interface_id
                ->process_trigger(
                    'disable_subscription',
                    {
                        config_interface_id => $self->config_interface->id,
                        subscription_id => $object_subscription,
                    }
                );
        }

        $self->stuf_delete_entry($record, $object);
    }

    ### Special CASE: Verhuizen
    if ($natuurlijk_persoon->verblijfsadres) {
        if ($self->_binnen_gemeente($record, $object, $natuurlijk_persoon->verblijfsadres)) {
            if (!$natuurlijk_persoon->in_gemeente) {
                $natuurlijk_persoon->in_gemeente(1);
                $natuurlijk_persoon->update;
            }
        } else {
            $natuurlijk_persoon->in_gemeente(0);
            $natuurlijk_persoon->update;
        }
    } else {
        $natuurlijk_persoon->in_gemeente(0);
        $natuurlijk_persoon->update;
    }

    ### Stolen from Zaaksysteem::BR::Subject::Types::Person, should be run from there, but this is old code :(
    $self->run_post_triggers($natuurlijk_persoon);

    return $natuurlijk_persoon;
}

sub _update_subject_natuurlijk_persoon {
    my $self                                = shift;
    my ($record, $object, $entry)           = @_;

    my $subject_params      = $object->get_params_for_natuurlijk_persoon;
    my %old_values          = $entry->get_columns;

    ### Remove the damn voorloopnul when inserting in DB
    $subject_params->{burgerservicenummer} = int($subject_params->{burgerservicenummer}) if exists $subject_params->{burgerservicenummer};

    eval { $entry->update($subject_params)->discard_changes };
    if ($@)  {
        warn $@;
        $self->stuf_throw(
            $object,
            'sysin/modules/stufprs/process/np_update_error',
            "Impossible to update NatuurlijkPersoon in our database: $@"
        );
    }

    my $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                            ->new(
                                table       => 'NatuurlijkPersoon',
                                table_id    => $entry->id,
                                update      => 1
                            );

    ### Log mutation
    $mutation_record->from_dbix($entry, \%old_values);

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );

    return $entry;
}

sub _get_cor_vbl_adres {
    my $self                                        = shift;
    my ($record, $object, $natuurlijk_persoon)      = @_;

    my $address_params      = $object->get_params_for_natuurlijk_persoon_adres;

    my $coradres = { map({ my $adreskey = $_; $adreskey =~ s/^correspondentie_//; $adreskey => $address_params->{$_} } grep ( { $_ =~ /^correspondentie_/ } keys %{ $address_params })) };
    $coradres->{functie_adres} = 'B';

    my $vbladres = { map({ $_ => $address_params->{$_} } grep ( { $_ !~ /^correspondentie_/ } keys %{ $address_params })) };
    $vbladres->{functie_adres} = 'W';

    return ($coradres, $vbladres);
}

sub _has_address {
    my $self                                = shift;
    my $address                             = shift;

    if ($address->{straatnaam} || $address->{adres_buitenland1}) {
        return 1;
    }

    return;
}

sub _update_subject_adres {
    my $self                                = shift;
    my ($record, $object, $natuurlijk_persoon)    = @_;

    my $updated_components      = $object->updated_components();

    return unless (
        $updated_components->{verblijfsadres} ||
        $updated_components->{correspondentie} ||
        $updated_components->{buitenland} 
    );

    my ($coradres, $vbladres)   = $self->_get_cor_vbl_adres(@_);
    if ($updated_components->{correspondentie}) {
        my ($coradres, $vbladres) = $self->_get_cor_vbl_adres(@_);

        if ($self->_has_address($coradres)) {
            $self->_update_subject_adres_by_type($natuurlijk_persoon,$coradres,$object);
        } else {
            $natuurlijk_persoon->delete_address_by_function('B');
        }
    }

    if ($updated_components->{verblijfsadres}) {
        if ($self->_has_address($vbladres)) {
            $self->_update_subject_adres_by_type($natuurlijk_persoon,$vbladres,$object);

            $self->_assert_gemeentecode($record, $object, $vbladres->{gemeente_code})
                if (!$vbladres->{landcode} || $vbladres->{landcode} == 6030);
        } else {
            $natuurlijk_persoon->delete_address_by_function('W');
        }
    }

    $natuurlijk_persoon->update_address_links;

    return $natuurlijk_persoon->adres_id;
}

sub _update_subject_adres_by_type {
    my $self        = shift;
    my $np          = shift;
    my $params      = shift;
    my $object      = shift;
    my ($current,$mutation_record);

    ### Get old address
    if (uc($params->{functie_adres}) eq 'B') {
        $current = $np->correspondentieadres;
    } else {
        $current = $np->verblijfsadres;
    }

    if ($current) {
        my %old_values          = $current->get_columns;

        unless ($current->update($params)->discard_changes) {
            $self->stuf_throw(
                $object,
                'sysin/modules/stufprs/process/adres_update_error',
                'Impossible to update Adres in our database, unknown error'
            );
        }

        $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation->new(
            table       => 'Adres',
            table_id    => $current->id,
            update      => 1
        );

        $mutation_record->from_dbix($current, \%old_values);
    } else {
        $mutation_record     = Zaaksysteem::Backend::Sysin::Transaction::Mutation
                                ->new(
                                    table       => 'Adres',
                                    create      => 1
                                );

        $np->add_address($params, {mutation_record => $mutation_record});
    }

    push(
        @{ $self->process_stash->{row}->{mutations} },
        $mutation_record
    );
}

=head2 DELETE SUBJECT

=head2 stuf_delete_entry

=cut

sub stuf_delete_entry {
    my $self                                = shift;
    my ($record, $object, $subscription)    = @_;


    my $natuurlijk_persoon                  = $self->get_entry_from_subscription(
                                                $record,
                                                $object,
                                                'NatuurlijkPersoon',
                                                $subscription
                                            );

    $record->preview_string(
        "Verwijderd: " .
        join(', ', ($natuurlijk_persoon->burgerservicenummer, $natuurlijk_persoon->voornamen, $natuurlijk_persoon->naamgebruik))
    );

    ### Delete subject from database
    my $address             = $natuurlijk_persoon->adres_id;

    if ($address) {
        $address->deleted_on(DateTime->now());
        $address->update;
    }

    $natuurlijk_persoon->deleted_on(DateTime->now());
    $natuurlijk_persoon->update;

    $self->_remove_subscription_from_entry($record, $object, $natuurlijk_persoon);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 import_prs

TODO: Fix the POD

=cut

=head2 search_prs

TODO: Fix the POD

=cut

