package Zaaksysteem::Backend::Filestore::Component;
use Moose;

use Digest::MD5::File qw(file_md5_hex);
use File::Basename;
use File::Temp;
use File::stat;
use Image::Magick;
use OpenOffice::OODoc;
use Params::Profile;
use Zaaksysteem::Constants;
use Zaaksysteem::Tools;
use Zaaksysteem::ZTT;

with qw(
    MooseX::Log::Log4perl
    Zaaksysteem::Roles::UStore
);

extends 'DBIx::Class';

use Exception::Class (
    'Zaaksysteem::Backend::Filestore::Component::Exception' => {fields => 'code'},
    'Zaaksysteem::Backend::Filestore::Component::Exception::General' => {
        isa         => 'Zaaksysteem::Backend::Filestore::Component::Exception',
        description => 'General exception',
        alias       => 'throw_general_exception',
    },
    'Zaaksysteem::Backend::Filestore::Component::Exception::Parameter' => {
        isa         => 'Zaaksysteem::Backend::Filestore::Component::Exception',
        description => 'Parameter exception',
        alias       => 'throw_parameter_exception',
    },
    'Zaaksysteem::Backend::Filestore::Component::Exception::Logic' => {
        isa         => 'Zaaksysteem::Backend::Filestore::Component::Exception',
        description => 'Logic exception',
        alias       => 'throw_logic_exception',
    },
    'Zaaksysteem::Backend::Filestore::Component::Exception::ImageMagick' => {
        isa         => 'Zaaksysteem::Backend::Filestore::Component::Exception',
        description => 'ImageMagick error',
        alias       => 'throw_magick_exception',
    },
);

=head2 name

The name of the file without the extension

=cut

has name => (
    is       => 'ro',
    isa      => 'Str',
    lazy     => 1,
    default => sub {
        my $self = shift;
        my ($filename) = fileparse($self->original_name, '\.[^\.]*');
        return $filename;
    }
);

=head2 extension

The extension of the file, includes the C<.>.
Eg C<.txt>

=cut

has extension => (
    is       => 'ro',
    isa      => 'Str',
    lazy     => 1,
    default => sub {
        my $self = shift;
        my (undef, undef, $suffix) = fileparse($self->original_name, '\.[^\.]*');
        return lc($suffix);
    }
);

=head2 mime_mapping

Returns a hashref of mime types and if we are allowed to convert, copy them.
Uses L<Zaaksysteem::Constants/MIMETYPES_ALLOWED>

=cut

has mime_mapping => (
    isa     => 'HashRef',
    is      => 'ro',
    lazy    => 1,
    default => sub {
        return MIMETYPES_ALLOWED;
    },
);

=head2 $filestore->verify_integrity

Check a file's integrity by comparing the database md5 with the filesystem md5.

=head3 Returns

True on succes, 0 on failure.

=cut

sub verify_integrity {
    my $self = shift;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $file_path = $self->ustore->getPath($self->uuid);
    if (defined $file_path && -f $file_path) {
        if ($self->md5 eq file_md5_hex($file_path)) {
            return 1;
        }
    }

    Zaaksysteem::StatsD->statsd->end('filestore.verify.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('file.verify', 1);

    return 0;
}

=head2 $filestore->generate_thumbnail

Generates a thumbnail for the filestore object.

=head3 Returns

The newly updated Filestore object with thumbnail_uuid set.

=cut

sub generate_thumbnail {
    my $self = shift;


    my $thumbnail_file = $self->_generate_thumbnail;

    my $ustore_id = $self->ustore->add($thumbnail_file->filename);

    return $self->update({
        thumbnail_uuid => $ustore_id
    });
}

sub _generate_thumbnail {
    my $self = shift;
    my $geometry = shift;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    # Create tempfiles
    my $pdf_handle = File::Temp->new(SUFFIX => '.pdf', UNLINK => 1);
    my $tn_handle  = File::Temp->new(SUFFIX => '.jpg', UNLINK => 1);

    Zaaksysteem::StatsD->statsd->end('filestore.generate_thumnail.tmp_files.time', $t0);

    my $pdf_file = $pdf_handle->filename;
    my $tn_file  = $tn_handle->filename;

    my $is_imagemagick_mimetype = grep {
        $_->{mimetype} eq $self->mimetype
        && defined $_->{conversion}
        && $_->{conversion} eq 'imagemagick'
    } values %{ MIMETYPES_ALLOWED() };

    # Imagemagick has no problem handling images and PDF, so they can be skipped
    # for conversion.
    my $magick_file;

    # If the format is directly supported by imagemagick, pass along the file
    # path. Otherwise convert to PDF first.
    if ($self->mimetype eq 'application/pdf' || $is_imagemagick_mimetype) {
        $magick_file = $self->ustore->getPath($self->uuid);
    } else {
        $magick_file = $self->convert({
            target_format => 'pdf',
            target_file   => $pdf_file,
        });
    }

    $self->create_thumbnail({
        # [0] indicates first-page only
        source_file  => sprintf('%s[0]', $magick_file),
        target_file  => $tn_file,
        geometry     => $geometry
    });



    return $tn_handle;
}

=head2 $filestore->get_path

Returns the path for the file.

=cut


sub get_path {
    my $self = shift;
    return $self->ustore->getPath($self->uuid);
}

=head2 $filestore->convert

Convert to a different format. Returns a location of the newly converted file. It
is the responsibility of the caller to unlink this (temporary) file.

=cut

define_profile convert => (
    required => {
        target_format         => 'Str',
        target_file           => 'Str',
    },
    optional => {
        magic_strings_convert => 'Bool',
        case_id               => 'Int',
    },
    constraint_methods => {
        target_format => qr/^(?:pdf|doc)$/i,
    },
    field_filters => {
        target_format => ['lc'],
    }
);

sub convert {
    my $self = shift;
    my $opts = assert_profile(shift)->valid;

    my $target_format = $opts->{target_format};
    my $target_file   = $opts->{target_file};
    my $case_id       = $opts->{case_id};
    my $magic_strings_convert = $opts->{magic_strings_convert};

    if ($self->extension eq ".$target_format") {
        return $self->get_path;
    }

    $self->assert_convert;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $method = "convert_to_$target_format";
    my $rv = $self->$method(
        $self->extension, $target_file, $magic_strings_convert, $case_id
    );

    Zaaksysteem::StatsD->statsd->end('filestore.convert.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('file.convert', 1);

    return $rv;

}

=head2 may_convert_to_doc

Checks if a file may be converted to a Word document

=cut

sub may_convert_to_doc {
    my ($self, $suffix) = @_;

    $suffix = lc($suffix // $self->extension);
    if (exists $self->mime_mapping->{$suffix}{copy2doc} && $self->mime_mapping->{$suffix}{copy2doc}) {
        return 1;
    }
    return 0;
}


=head2 may_preview_as_pdf

Checks if a file may be previewed as PDF

=cut

sub may_preview_as_pdf {
    my ($self, $suffix) = @_;

    $suffix = lc($suffix // $self->extension);
    if (!exists $self->mime_mapping->{$suffix}{preview} || $self->mime_mapping->{$suffix}{preview}) {
        return 1;
    }
    return 0;
}

=head2 may_copy_to_pdf

Checks if a file may be copied to PDF by Zaaksysteem.

=cut


sub may_copy_to_pdf {
    my ($self, $suffix) = @_;

    $suffix = lc($suffix // $self->extension);
    if (!exists $self->mime_mapping->{$suffix}{copy2pdf} || $self->mime_mapping->{$suffix}{copy2pdf}) {
        return 1;
    }
    return 0;
}

=head2 assert_convert

Asserts if a file may be converted by Zaaksysteem.

=cut

sub assert_convert {
    my ($self, $suffix) = @_;

    if ($self->may_preview_as_pdf($suffix) || $self->may_copy_to_pdf($suffix) || $self->may_convert_to_doc($suffix)) {
        return 1;
    }

    throw('filestore/convert', sprintf('Not allowed to convert %s to alternate filetype', $self->extension));
}

=head2 $filestore->convert_to_doc

Convert a filestore entry to DOC and return the new file's location.

=head3 Returns

Returns a file path.

=cut

sub convert_to_doc {
    my $self = shift;
    my ($extension, $target_file, $magic_strings_convert, $case_id) = @_;

    if ($extension ne '.odt') {
        throw_general_exception(
            code  => '/filestore/convert/source_and_target_format_incompatible',
            error => "Bronbestand-extensie '$extension' kan niet omgezet worden naar '.doc'",
        );
    }

    my $can_convert = MIMETYPES_ALLOWED;

    my $content;
    # Handle magic string conversion when desired
    if ($magic_strings_convert && $case_id) {
        my $tmp_file = $self->save_magic_string_document($case_id);
        open FILE, '<', $tmp_file or die "Could not open converted document: $tmp_file";
        $content = join "", <FILE>;
        unlink($tmp_file);
    }
    else {
        open FILE, '<', $self->get_path or die "Could not open file at ".$self->get_path;
        $content  = join "", <FILE>;
    }

    my $converter_result = $self->result_source->schema->resultset('Filestore')->send_to_converter(
        Content      => $content,
        Content_Type => $can_convert->{$extension}{mimetype},
        Accept       => 'application/msword'
    );

    open TARGET, '>', $target_file or die "could not write to: " . $target_file . ": " . $!;
    print TARGET $converter_result;
    close TARGET;

    return $target_file;
}

=head2 $filestore->convert_to_pdf

Convert a filestore entry to PDF and return the data.

=head3 Returns

The raw data as received from Office.

=cut

sub convert_to_pdf {
    my $self = shift;
    my ($extension, $target_file, $magic_strings_convert, $case_id) = @_;

    my $ext = lc($extension);

    my $can_convert = MIMETYPES_ALLOWED;
    # Images are converted with ImageMagick, the rest with LibreOffice
    if (   exists($can_convert->{$ext}{conversion})
        && $can_convert->{$ext}{conversion} eq 'imagemagick') {
        my $magick = Image::Magick->new;

        $magick->Set('disk-limit'   => '512MB');
        $magick->Set('memory-limit' => '1GB');

        my $error  = $magick->Read($self->get_path);
        if ($error) {
            throw_magick_exception(
                code  => '/filestore/create_thumbnail/imagemagick_error',
                error => "ImageMagick error: $error",
            );
        }
        $magick->Write("pdfa:$target_file");

        return $target_file;
    }
    elsif (   $can_convert->{$ext}{conversion} eq 'xpstopdf'
           || $can_convert->{$ext}{conversion} eq 'xhtml2pdf'
    ) {
        use autodie qw(:all);
        system($can_convert->{$ext}{conversion}, $self->get_path, $target_file);

        return $target_file;
    }
    elsif (   !exists($can_convert->{$ext}{conversion})
           || $can_convert->{$ext}{conversion} ne 'jodconvertor'
    ) {
        throw_logic_exception(
            code  => '/filestore/convert_to_pdf/unsupported_filetype',
            error => "Cannot convert filetype $extension to PDF",
        );
    }

    my $content;
    # Handle magic string conversion when desired
    if ($magic_strings_convert && $case_id) {
        my $tmp_file = $self->save_magic_string_document($case_id);
        open FILE, '<', $tmp_file or throw "Could not open converted document: $tmp_file";
        $content = join "", <FILE>;
        unlink($tmp_file);
    }
    else {
        open FILE, '<', $self->get_path or die "Could not open file at ".$self->get_path;
        $content  = join "", <FILE>;
    }

    my $converter_result = $self->result_source->schema->resultset('Filestore')->send_to_converter(
        Content      => $content,
        ### Do NOT pass the mimetype stored in filestore.mimetype. While it is
        ### likely 100% correct, jodconvertor's mimetype list is limited. In
        ### particular for .doc documents there are simply too many variants.
        Content_Type => $can_convert->{$ext}{mimetype},
        Accept       => 'application/pdf',
    );

    open TARGET, '>', $target_file or die "could not write to: " . $target_file . ": " . $!;
    print TARGET $converter_result;
    close TARGET;

    return $target_file;
}

=head2 $filestore->create_thumbnail

Convert a PDF or JPEG to a JPEG thumbnail.

=head3 Parameters

=over

=item source_file

The file that needs converting.

=item target_file

Where the new file will be saved.

=back

=head3 Returns

True on succes, 0 on failure.

=cut

Params::Profile->register_profile(
    method  => 'create_thumbnail',
    profile => {
        required => [qw[source_file target_file]],
        optional => [qw[geometry]]
    }
);

sub create_thumbnail {
    my $self = shift;
    my $opts = $_[0];

    my $t0 = Zaaksysteem::StatsD->statsd->start;


    # Check database parameters
    my $dv = Params::Profile->check(
        params  => $opts,
    );
    if ($dv->has_invalid) {
        my @invalid = join ',',$dv->invalid;
        throw_parameter_exception(
            code  => '/filestore/create_thumbnail/invalid_parameters',
            error => "Invalid options given: @invalid",
        );
    }
    if ($dv->has_missing) {
        my @missing = join ',',$dv->missing;
        throw_parameter_exception(
            code  => '/filestore/create_thumbnail/missing_parameters',
            error => "Missing options: @missing",
        );
    }

    # Shove the PDF/JPEG through ImageMagick and store as new JPEG.
    my $image = Image::Magick->new;
    $image->Set('disk-limit'   => '512MB');
    $image->Set('memory-limit' => '1GB');
    # Image::Magick throws non-fatal error messages. Even if the error is fatal. Die on
    # every message returned. (Succes is always undefined)
    my $error = $image->Read($opts->{source_file});
    if ($error) {
        throw_magick_exception(
            code  => '/filestore/create_thumbnail/imagemagick_error',
            error => "ImageMagick error: $error",
        );
    }

    $error = $image->Resize(geometry => $opts->{ geometry } || '440x');

    # pull the different layers together. this will prevent black thumbnails
    # in some cases. mysteriously became necessary on Dec 7 2013, in combination
    # with Resize
    $image = $image->Flatten();

    if ($error) {
        throw_magick_exception(
            code  => '/filestore/create_thumbnail/imagemagick_error',
            error => "ImageMagick error: $error",
        );
    }
    $error = $image->Write($opts->{target_file});
    if ($error) {
        throw_magick_exception(
            code  => '/filestore/create_thumbnail/imagemagick_error',
            error => "ImageMagick error: $error",
        );
    }

    Zaaksysteem::StatsD->statsd->end('filestore.create_thumbnal.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.create_thumbnal', 1);


    return $opts->{source_file};
}

=head2 $filestore->get_thumbnail_path

Returns the path for the file's thumbnail.

=cut

sub get_thumbnail_path {
    my $self = shift;
    return $self->ustore->getPath($self->thumbnail_uuid);
}

=head2 $filestore->get_thumbnail

Return the file's thumbnail file handle.

=cut

sub get_thumbnail {
    my $self = shift;
    return $self->ustore->get($self->thumbnail_uuid);
}

=head2 $filestore->name_without_extension

Return the file's name stripped of its extension.

=cut

sub name_without_extension {
    my $self = shift;
    return $self->name;
}

sub filestat {
    return stat(shift->get_path);
}

=head2 save_magic_string_document

Converts and stores a given filestore entry. Case_id is NOT optional.

=cut

sub save_magic_string_document {
    my $self = shift;
    my ($case_id) = @_;

    my $ztt = Zaaksysteem::ZTT->new;
    $ztt->add_context(
        $self->result_source->schema->resultset('Zaak')->find($case_id)
    );

    return $self->save_template_document($ztt);
}

=head2 save_template_document

Converts and stores the filestore entry, using the specified
L<Zaaksysteem::ZTT> instance.

=cut

sub save_template_document {
    my $self = shift;
    my ($ztt) = @_;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $dir = File::Temp->newdir();
    my $tmp_fh = File::Temp->new(
        TEMPLATE => "jodconverter-XXXXXX",
        UNLINK   => 1,
        DIR      => $dir,
        # If there is no suffix, and the name of the DIR above contains a ".",
        # a call to Archive::ZIP done by "save" below will do bad things (it
        # strips everything after the last "." and adds ".zbk")
        SUFFIX => '.tmp',
    );
    my $tmp_file = $tmp_fh->filename;

    odfWorkingDirectory($dir);
    my $encoding = $OpenOffice::OODoc::XPath::LOCAL_CHARSET;
    my $document = odfDocument(
        file            => $self->get_path || undef,
        local_encoding  => $encoding,
    );

    $document = $ztt->process_template($document)->document;

    $document->save($tmp_file);

    Zaaksysteem::StatsD->statsd->end('filestore.save_template_doc.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.save_template_doc', 1);

    return $tmp_fh;
}

=head2 content

Returns content of this file as a string

=cut

sub content {
    my $self = shift;

    my $t0 = Zaaksysteem::StatsD->statsd->start;

    my $file = $self->read_handle();
    my $content = join "", <$file>;
    close $file;

    Zaaksysteem::StatsD->statsd->end('filestore.content.time', $t0);
    Zaaksysteem::StatsD->statsd->increment('filestore.content', 1);

    return $content;
}

=head2 read_handle

Return a handle to the file (on disk), ready for reading.

=cut

sub read_handle {
    my $self = shift;

    my $path = $self->get_path;

    open my $file, '<', $path
        or throw ('filestore/cant_open', "Could not open filestore $path");

    return $file;
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 filestat

TODO: Fix the POD

=cut

