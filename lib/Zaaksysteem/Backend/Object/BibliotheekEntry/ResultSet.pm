package Zaaksysteem::Backend::Object::BibliotheekEntry::ResultSet;

use Moose;
use namespace::autoclean;

extends 'DBIx::Class::ResultSet';

=head1 NAME

Zaaksysteem::Backend::Object::BibliotheekEntry::ResultSet

=head1 DESCRIPTION

This class exists to link the new object model to the old catalogue
infrastructure. It's an almost empty shim to make things work.

=head1 METHODS

=head2 create_or_update

Implements the logic to update a BibliotheekEntry row with data from a
L<ObjectData|Zaaksysteem::Backend::Object::Data::Component> instance.

    $c->model('DB::ObjectTypeBibliotheekEntry')->create_or_update($object_data_row);

=cut

sub create_or_update {
    my $self = shift;
    my $object_data = shift;

    my $entry = $self->find({ object_uuid => $object_data->uuid });

    unless($entry) {
        return $self->create($object_data->bibliotheek_entry_args);
    }

    return $entry->update($object_data->bibliotheek_entry_args);
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

