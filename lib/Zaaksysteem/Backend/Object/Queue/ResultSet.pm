package Zaaksysteem::Backend::Object::Queue::ResultSet;

use Moose;

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw[UUID];

extends 'Zaaksysteem::Backend::ResultSet';

=head1 NAME

Zaaksysteem::Backend::Object::Queue::ResultSet - Additional behavior for the
C<queue> table

=head1 DESCRIPTION

=head1 METHODS

=head2 create_item

Wrapper for L<DBIx::Class::ResultSet/create>. This method mostly exists to
provide a stable API for the module should the underlying storage change.

    my $item = $queue_rs->create_item('my_queued_item_type_name', {
        object_id => UUID,
        label => 'Str',
        data => { ... }
    });

=cut

define_profile create_item => (
    required => {
        label => 'Str'
    },
    optional => {
        object_id => UUID,
        data => 'HashRef'
    }
);

sig create_item => 'Str, ?HashRef';

sub create_item {
    my $self = shift;
    my $type = shift;
    my $opts = assert_profile(shift)->valid;

    $opts->{ type } = $type;

    $self->create($opts)->discard_changes;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
