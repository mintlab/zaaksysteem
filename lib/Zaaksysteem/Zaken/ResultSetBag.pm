package Zaaksysteem::Zaken::ResultSetBag;

use strict;
use warnings;

use Moose;
use Data::Dumper;

extends 'DBIx::Class::ResultSet';

sub create_bag {
    my  $self   = shift;

    my $row     = $self->create(@_);

    $row->update_bag;

    return $row;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 create_bag

TODO: Fix the POD

=cut

