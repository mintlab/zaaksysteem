package Zaaksysteem::Zaken::Roles::KenmerkenSetup;

use Moose::Role;
use Data::Dumper;

use Zaaksysteem::Exception;

around '_create_zaak' => sub {
    my $orig                = shift;
    my $self                = shift;
    my ($opts)              = @_;
    my ($zaak_kenmerken);

    $self->log->trace('Role [KenmerkenSetup]: started');

    my $zaak = $self->$orig(@_);

    ### Fix kenmerken
    my %kenmerken = map { my ($id, $values) = each %$_; $id => $values } @{ $opts->{kenmerken} };

    my @manual_attributes = keys %kenmerken;

    ### Mangle kenmerken with defaults
    my $mangled_properties  = $zaak
                            ->zaaktype_node_id
                            ->zaaktype_kenmerken
                            ->mangle_defaults(
                                \%kenmerken
                            );


    # mangle_defaults may return a hashref
    my $ref = ref $mangled_properties;
    if ($ref eq 'HASH' && keys %$mangled_properties) {
        $zaak->zaak_kenmerken->update_fields({
            zaak                        => $zaak,
            new_values                  => $mangled_properties,
            set_values_except_for_attrs => \@manual_attributes,
        });
    }

    ### Recache field values
    delete($zaak->{cached_field_values});
    $zaak->touch();

    $self->log->trace('Kenmerken toegevoegd');

    return $zaak;
};

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
