package Zaaksysteem::Zaken::Roles::HStore;

use Moose::Role;

=head1 NAME

Zaaksysteem::Zaken::Roles::HStore - Role that adds hstore column updating ability.

=head1 METHODS

=head2 update_hstore

Update the "hstore_properties" column with the value of the
C<object_attributes> attribute.

=cut

sub update_hstore {
    my $self = shift;

    my $case_object = $self->object_data;

    return if not $case_object;

    $case_object->class_uuid($self->zaaktype_id->_object->uuid);

    $self->clear_object_attributes();
    $case_object->replace_object_attributes(@{ $self->object_attributes });

    $self->_update_object_relationships($case_object);
    $self->_update_object_acl($case_object);
    $self->_update_object_casetype($case_object);

    $case_object->update;
}

=head2 _update_object_acl

This method will update the L<Zaaksysteem::Schema::ObjectAclEntry>s associated
with the case instance. It does a destructive replace of all ACL rules with
freshly generated rules.

=cut

# XXX TODO
# That said, this thing can be performance buzzkill in the long run, we can
# update more efficiently by just saving the diff to the rules (hell, lets
# start by not updating if nothing's changed)

sub _update_object_acl {
    my $self = shift;
    my $case_object = shift;

    my @acl_args;
    my @existing_acls = $case_object->object_acl_entries->all;
    my $rs = $self->result_source->schema->resultset('ObjectAclEntry');

    # Iterator over aanvrager, coordinator and behandelaar objects
    # Skip unless defined, or skip when btype != medewerker
    # Store in hashmap indexed on betrokkene_identifier, so we don't
    # generate double entries
    my %subjects = map { $_->betrokkene_identifier => $_ } grep {
        defined && $_->btype eq 'medewerker'
    } (
        $self->aanvrager_object,
        $self->coordinator_object,
        $self->behandelaar_object
    );

    # Iterator over involved subjects, creating appropriate
    # ACL entries along the way.
    for my $security_id (keys %subjects) {
        my %sec_id = $subjects{ $security_id }->security_identity;

        for my $type (keys %sec_id) {
            push @acl_args, {
                object_uuid => $case_object->uuid,
                entity_type => $type,
                entity_id => $sec_id{ $type },
                capability => 'read',
                scope => 'instance'
            }, {
                object_uuid => $case_object->uuid,
                entity_type => $type,
                entity_id => $sec_id{ $type },
                capability => 'write',
                scope => 'instance'
            };
        }
    }

    # Comperator for in-place ACL updates
    my $cmp = sub {
        my ($a, $b) = @_;

        return if grep { defined $b->{ $_ } && $a->{ $_ } ne $b->{ $_ } } keys %{ $a };
        return 1;
    };

    my @new_acl_args;
    my @delete_uuids;

    # Compare new to old, if no existing record matches, it's a new ACL
    ARGS: for my $args (@acl_args) {
        for my $existing_acl (@existing_acls) {
            if ($cmp->($args, { $existing_acl->get_columns })) {
                next ARGS;
            }
        }

        push @new_acl_args, $args;
    }

    # Compare old to new, if no new records match, it's a deletion
    EXISTING: for my $existing_acl (@existing_acls) {
        for my $args (@acl_args) {
            if ($cmp->($args, { $existing_acl->get_columns })) {
                next EXISTING;
            }
        }

        push @delete_uuids, $existing_acl->uuid;
    }

    if (scalar @delete_uuids) {
        $rs->search({ uuid => { -in => \@delete_uuids } })->delete;
    }

    if (scalar @new_acl_args) {
        $rs->populate(\@new_acl_args);
    }

    $case_object->acl_groupname($self->confidentiality eq 'confidential' ? 'confidential' : 'public');

    return;
}

=head2 _update_object_relationships

This method will update all L<Zaaksysteem::Schema::ObjectRelationships>
associated with the case instance. It does a destructive replace of all
existing relations, creating a fresh set from the currently available case
state data.

=cut

sub _update_object_relationships {
    my $self = shift;
    my ($case_object) = @_;

    my $schema = $self->result_source->schema;
    my $rs = $schema->resultset('ObjectRelationships');

    my @existing_relations = $rs->search({
        -or => [
            { object1_uuid => $case_object->uuid, object2_type => 'case' },
            { object2_uuid => $case_object->uuid, object1_type => 'case' }
        ]
    })->all;

    my @relations_args;

    my $parent = $self->pid;
    if($parent && !$parent->is_deleted) {
        my $parent_object = $parent->object_data;
        push @relations_args, {
            object1_uuid => $parent_object->uuid,
            object1_type => $parent_object->object_class,
            type1        => 'parent',

            owner_object_uuid => $case_object->uuid,

            object2_uuid => $case_object->uuid,
            object2_type => $case_object->object_class,
            type2        => 'child',
        };
    }

    my $children = $self->zaak_children->search;
    while (my $child = $children->next) {
        next if $child->is_deleted;

        my $child_object = $child->object_data;
        push @relations_args, {
            object1_uuid => $case_object->uuid,
            object1_type => $case_object->object_class,
            type1        => 'parent',

            owner_object_uuid => $child_object->uuid,

            object2_uuid => $child_object->uuid,
            object2_type => $child_object->object_class,
            type2        => 'child'
        };
    }

    # Other case relationships
    my $case_relations = $schema->resultset('CaseRelation')->search({ case_id => $self->id });
    while (my $cr = $case_relations->next) {
        if ($cr->get_column('case_id_a') == $self->id) {
            # We're A
            my $b_object = $cr->case_id_b->object_data;
            next if not $b_object;

            push @relations_args, {
                object1_uuid => $case_object->uuid,
                object1_type => $case_object->object_class,
                type1        => $cr->type_a,

                object2_uuid => $b_object->uuid,
                object2_type => $b_object->object_class,
                type2        => $cr->type_b,
            };
        }  else {
            # We're B
            my $a_object = $cr->case_id_a->object_data;
            next if not $a_object;

            push @relations_args, {
                object1_uuid => $a_object->uuid,
                object1_type => $a_object->object_class,
                type1        => $cr->type_a,

                object2_uuid => $case_object->uuid,
                object2_type => $case_object->object_class,
                type2        => $cr->type_b
            };
        }
    }

    # Relation hash comperator for in-place updates
    my $cmp = sub {
        my ($a, $b) = @_;

        return if grep { defined $b->{ $_ } && $a->{ $_ } ne $b->{ $_ } } keys %{ $a };
        return 1;
    };

    my @new_relations_args;
    my @delete_uuids;

    # Compare new to old, if no existing record matches, it's a new relation
    ARGS: for my $args (@relations_args) {
        for my $existing_relation (@existing_relations) {
            if ($cmp->($args, { $existing_relation->get_columns })) {
                next ARGS;
            }
        }

        push @new_relations_args, $args;
    }

    # Compare old to new, if no existing record matches, it's a deletion
    EXISTING: for my $existing_relation (@existing_relations) {
        for my $args (@relations_args) {
            if ($cmp->($args, { $existing_relation->get_columns })) {
                next EXISTING;
            }
        }

        push @delete_uuids, $existing_relation->uuid;
    }

    if (scalar @delete_uuids) {
        $rs->search({ uuid => { -in => \@delete_uuids } })->delete;
    }

    if (scalar @new_relations_args) {
        $rs->populate(\@new_relations_args);
    }

    return;
}

sub _update_object_casetype {
    my $self = shift;
    my $object = shift;

    # Take advantage of possible cache for object_relation rows.
    my ($casetype_rel) = grep {
        $_->object_type eq 'casetype'
    } $object->object_relation_object_ids->all;

    # We're OK if there is a casetype relation at all, updating this should
    # be required under normal circumstances. There /does/ exist code to
    # change a case's casetype, but updating the relation should happen there,
    # not in this routine.
    return $casetype_rel if defined $casetype_rel;

    return $self->_rewrite_object_casetype($object);
}

sub _rewrite_object_casetype {
    my $self = shift;
    my $object = shift;

    $self->log->info(sprintf(
        'No casetype relation found for case %s, initializing it now.',
        $self->id
    ));

    my $casetype = $self->zaaktype_id->build_casetype_object(
        zaaktype_node_id => $self->get_column('zaaktype_node_id')
    );

    # XXX serialization <hack>
    my $encoder = do {
        no warnings 'redefine';
        local *DateTime::TO_JSON = sub { shift->iso8601 };

        JSON->new->utf8->pretty->allow_nonref->allow_blessed->convert_blessed->canonical;
    };
    # </hack>

    return $object->object_relation_object_ids->create({
        name => 'casetype',
        object_type => 'casetype',
        object_embedding => $casetype,
    });
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
