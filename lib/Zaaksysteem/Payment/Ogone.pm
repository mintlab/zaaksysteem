package Zaaksysteem::Payment::Ogone;

use Moose;

use URI;
use URI::QueryParam;
use LWP::UserAgent;
use Digest::SHA qw/sha1_hex sha512_hex/;
use Text::Unidecode qw[unidecode];

use Zaaksysteem::ZTT;

my %PUBLIC_ATTR = (
    ### Variable
    orderid     => 'orderID',
    amount      => 'amount',
    pspid       => 'PSPID',
    shasign     => 'SHASign',
    com         => 'COM',

    ### Static
    'language' => 'language',
    'currency' => 'currency',
    'accepturl' => 'accepturl',
    'declineurl' => 'declineurl',
    'exceptionurl' => 'exceptionurl',
    'cancelurl' => 'cancelurl',
    'backurl' => 'backurl',
    'homeurl' => 'homeurl',
);

my @SHA1_ATTR   = (
    'amount',
    'currency',
    'orderid',
);

use constant OGONE_SERVER_DATA => {
    TEST => {
        'posturl'   => 'https://secure.ogone.com/ncol/test/orderstandard.asp',
        #'posturl'   => 'http://dev.zaaksysteem.nl:3000/plugins/ogone/test',
        'accepturl' =>
            'plugins/ogone/api/accept',
        'declineurl' =>
            'plugins/ogone/api/decline',
        'exceptionurl' =>
            'plugins/ogone/api/exception',
        'cancelurl' =>
            'plugins/ogone/api/cancel',
        'layout'    => {},
        'language'  => 'nl_NL',
        'currency'  => 'EUR',
    },
    PROD => {
        'posturl'   => 'https://secure.ogone.com/ncol/prod/orderstandard.asp',
        #'posturl'   => 'http://dev.zaaksysteem.nl:3000/plugins/ogone/test',
        'accepturl' =>
            'plugins/ogone/api/accept',
        'declineurl' =>
            'plugins/ogone/api/decline',
        'exceptionurl' =>
            'plugins/ogone/api/exception',
        'cancelurl' =>
            'plugins/ogone/api/cancel',
        'layout'    => {},
        'language'  => 'nl_NL',
        'currency'  => 'EUR',
    },
};

### OGONE PUBLIC VARS
has [ keys %PUBLIC_ATTR ] => (
    is      => 'rw',
);

### OGONE STATIC VARS
has [qw/variables zaak layout posturl shasign succes status verified baseurl/] => (
    is      => 'rw'
);

### Defaults for model
has [qw/log session params/] => (
    is      => 'rw',
);

has 'prod'  => (
    'is'        => 'rw',
);

has 'dummy' => (
    'is'        => 'rw',
);

has [qw/shapass shapassout hash_algorithm order_description_template/] => (
    'is' => 'ro',
);

sub start_payment {
    my ($self, %opt)    = @_;

    ### Clear ancient data
    $self->clear_payment;

    {
        for my $variable (qw/amount zaak/) {
            die('Missing variable ' . $variable) unless $opt{$variable};

            $self->$variable($opt{$variable});
        }

        die "amount must be a positive integer" unless $opt{amount} =~ m|^\d+$|;

        my $ztt = Zaaksysteem::ZTT->new->add_context($opt{ zaak });

        $self->com(substr(
            unidecode($ztt->process_template($self->order_description_template)->string),
            0,
            100
        ));
    }

    ### URLS
    {
        my $prod_test_data  = OGONE_SERVER_DATA;

        my $prodortest  = 'TEST';
        $prodortest     = 'PROD' if $self->prod;

        while (my ($target, $data) = each %{ $prod_test_data->{$prodortest} }) {
            if ($target =~ /url/ && $data !~ /http/) {
                $self->$target($self->baseurl . $data);
                next;
            }
            $self->$target($data);
        }
    }

    ### Order id
    $self->orderid(time() . 'z' . $self->zaak->id);

    ### SHA1
    {
        my $shapass = $self->{shapass};

        my ($field, $logfield) = ('','');
        for my $key (sort keys %PUBLIC_ATTR) {
            next if $self->$key eq '';
            $logfield .= uc($key) . '=' . $self->$key;
            $field .= uc($key) . '=' . $self->$key . $shapass;
        }

        #$field .= $shapass;

        my ($digest, $digestlog);
        if (
            $self->hash_algorithm &&
            $self->hash_algorithm eq 'sha512'
        ) {
            $digest     = uc(sha512_hex($field));
            $digestlog  = uc(sha512_hex($logfield));
        } else {
            $digest     = uc(sha1_hex($field));
            $digestlog  = uc(sha1_hex($logfield));
        }

        $self->shasign($digest);
        $self->log->debug(
            'Z:P:Ogone->start_payment: Hashing key: '
            . $logfield
        );

        $self->log->debug(
            'Z:P:Ogone->start_payment: Hashed key: '
            . $self->shasign
        );
        $self->log->debug(
            'Z:P:Ogone->start_payment: Hashed logkey: '
            . $digestlog
        );
    }


    $self->variables(\%PUBLIC_ATTR);
}

sub verify_payment {
    my ($self, %opt) = @_;

    ### Clear ancient data
    $self->clear_payment;

    ### Fill object
    {
        my $shapassout  = $self->shapassout;
        my $shatype     = $self->hash_algorithm;
        my $shagiven    = $opt{SHASIGN};
        delete($opt{SHASIGN});

        my %fields;
        $fields{uc($_)} = $opt{$_} for keys %opt;

        my ($field, $logfield) = ('','');
        for my $key (sort keys %fields) {
            my $value   = $fields{$key};
            next if $value eq '';

            $field      .= uc($key) . '=' . $value . $shapassout;
            $logfield   .= uc($key) . '=' . $value;
        }

        my ($digest, $digestlog);
        if (
            defined($shatype) &&
            $shatype &&
            $shatype eq 'sha512'
        ) {
            $digest     = uc(sha512_hex($field));
            $digestlog  = uc(sha512_hex($logfield));
        } else {
            $digest     = uc(sha1_hex($field));
            $digestlog  = uc(sha1_hex($logfield));
        }

        $self->log->debug(
            'Z:P:Ogone->verify_payment: Hashing key: '
            . $logfield
        );

        $self->log->debug(
            'Z:P:Ogone->verify_payment: Hashed key: '
            . $digest
        );
        $self->log->debug(
            'Z:P:Ogone->verify_payment: Hashed logkey: '
            . $digestlog
        );

        ### Fill information
        for my $key (keys %fields) {
            my $lckey   = lc($key);
            next unless $self->can($lckey);

            $self->$lckey($fields{$key});
        }

        if ($digest eq uc($shagiven)) {
            $self->log->debug(
                'Z:P:Ogone->start_payment: Verified ogone hash: '
                . $shagiven
            );

            #$self->succes(1);
            $self->verified(1);

            if ($fields{STATUS} eq '9') {
                $self->succes(1);
                $self->log->info(
                    'Z:P:Ogone->start_payment: Verified ogone hash and'
                    . ' succesfull transaction'
                );

            } else {
                $self->log->info(
                    'Z:P:Ogone->start_payment: Verified ogone hash but'
                    . ' NO succesfull transaction: Statuscode: '
            . $fields{STATUS}
                );
            }
        } else {
            $self->log->debug(
                'Z:P:Ogone->start_payment: Verified ogone hash, NOT VALID: '
                . $shagiven
            );

            $self->verified(undef);
            $self->succes(undef);
        }

        # Correct amount, irritating ogone:
        $self->amount(($self->amount*100));
    }

    return $self->verified;
}

sub clear_payment {
    my ($self) = @_;

    $self->$_(undef) for qw/amount zaak orderid shasign succes/;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=head2 clear_payment

TODO: Fix the POD

=cut

=head2 start_payment

TODO: Fix the POD

=cut

=head2 verify_payment

TODO: Fix the POD
