package Zaaksysteem::Search::ZQL::Command::Describe;

use Moose;

extends 'Zaaksysteem::Search::ZQL::Command';

has object_type => ( is => 'ro', isa => 'Zaaksysteem::Search::ZQL::Literal::ObjectType' );

sub new_from_production {
    my $class = shift;
    my %param = @_;

    $class->new(
        object_type => $param{ object_type }
    );
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 new_from_production

TODO: Fix the POD

=cut

