package Zaaksysteem::Object::Queue::Model::Case;

use Moose::Role;

use Zaaksysteem::Tools;
use Zaaksysteem::Types qw[UUID];

=head1 NAME

Zaaksysteem::Object::Queue::Model::Case - Case queue item handler

=head1 DESCRIPTION

=head1 METHODS

=head2 create_case_subcase

=cut

sig create_case_subcase => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case_subcase {
    my $self = shift;
    my $item = shift;

    $item->object_data->get_source_object->start_subcase(
        action_data => $item->data,
        object_model => $self->object_model,
        betrokkene_model => $self->subject_model,
        current_user => $self->subject_table->find($item->data->{ _subject_id })
    );

    return;
}

=head2 create_case_document

=cut

sig create_case_document => 'Zaaksysteem::Backend::Object::Queue::Component';

sub create_case_document {
    my $self = shift;
    my $item = shift;

    my %template_action_args = (
        action_data => $item->data
    );

    if ($item->data->{ _subject_id }) {
        $template_action_args{ current_user } = $self->subject_table->find(
            $item->data->{ _subject_id }
        );
    }

    my $file = $item->object_data->get_source_object->template_action(
        %template_action_args
    );

    my $data = $item->data;

    $data->{result} = {
        file_id        => $file->id,
        filestore_uuid => $file->filestore_id->uuid,
    };
    $item->data($data);

    return;
}

=head2 allocate_case

=cut

sig allocate_case => 'Zaaksysteem::Backend::Object::Queue::Component';

sub allocate_case {
    my $self = shift;
    my $item = shift;

    $item->object_data->get_source_object->allocation_action($item->data);

    return;
}

=head2 touch_case

Handler for asynchronous case touches.

=cut

define_profile touch_case => (
    required => {
        case_object_id => UUID
    }
);

sub touch_case {
    my $self = shift;
    my $item = shift;

    my $args = assert_profile($item->data)->valid;

    my $case = $self->object_model->new_resultset->find($args->{ case_object_id });

    $case->get_source_object->_touch;

    return;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
