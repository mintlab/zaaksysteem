package Zaaksysteem::Object::Types::Widget;

use Moose;
use namespace::autoclean;

extends 'Zaaksysteem::Object';
with qw(Zaaksysteem::Object::Roles::Security);

use Zaaksysteem::Types qw(Betrokkene JSONBoolean JSONNum NonEmptyStr);
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::Object::Types::Widget - Built-in object type implementing a class for dashboard widgets

=head1 DESCRIPTION

Dashboard widgets

=head1 ATTRIBUTES

=head2 subject_id

Type: Num

The subject_id of the owner.

=cut

has subject_id => (
    is       => 'rw',
    isa      => 'Num',
    traits   => [qw(OA)],
    label    => 'Subject id',
    required => 1,
);

=head2 type

type: Str

The type of the widget

=cut

has widget    => (
    is      => 'rw',
    isa     => NonEmptyStr,
    traits   => [qw(OA)],
    label    => 'Widget type',
    required => 1,
);

=head2 data

Type: JSON

Custom JSON data from backend

=cut

has data    => (
    is      => 'rw',
    isa     => 'HashRef',
    traits   => [qw(OA)],
    label    => 'Custom JSON data',
    required => 1,
);

=head2 row

Type: integer

The current row on the screen

=cut

has row    => (
    is      => 'rw',
    isa     => JSONNum,
    traits   => [qw(OA)],
    label    => 'Row on screen',
    required => 1,
    coerce   => 1,
);

=head2 column

Type: integer

The current column on the screen

=cut

has column    => (
    is      => 'rw',
    isa     => JSONNum,
    traits   => [qw(OA)],
    label    => 'Column on screen',
    required => 1,
    coerce   => 1,
);

=head2 size_x

Type: integer

The current width in pixels

=cut

has size_x    => (
    is      => 'rw',
    isa     => JSONNum,
    traits   => [qw(OA)],
    label    => 'Widget type',
    required => 1,
    coerce   => 1,

);

=head2 size_y

Type: integer

The current height in pixels

=cut

has size_y    => (
    is      => 'rw',
    isa     => JSONNum,
    traits   => [qw(OA)],
    label    => 'Widget type',
    required => 1,
    coerce   => 1,
);


__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
