package Zaaksysteem::Mail::Parser;
use Moose;

use namespace::autoclean;
use autodie;

use Data::Dumper;

use Email::Address;
use File::Basename;
use File::Spec::Functions qw(catfile);
use MIME::Parser;
use Zaaksysteem::Tools;

has parser => (
    is => 'ro',
    isa => 'MIME::Parser',
    default => sub {
        my $self = shift;
        my $parser = MIME::Parser->new();
        $parser->extract_uuencode(1);
        $parser->output_dir($self->tmpdir);
        return $parser;
    },
    lazy => 1,

);

has tmpdir => (
    is => 'ro',
    isa => 'Str',
    default => sub {
        my $self = shift;
        return $self->schema->tmp_path;
    },
    lazy => 1,
);

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

sub parse_message {
    my ($self, $message) = @_;

    my $entity = $self->parser->parse_data($message);
    my @parts = $entity->parts_DFS;

    my $subject;
    my @files;

    for my $p (@parts) {
        my $user = $self->_get_user($p);
        if ($user) {
            $subject = $user;
            next;
        }
        my $result = $self->_get_attachement($p);
        if (defined $result) {
            push(@files, $result);
        }
    }

    foreach my $f (@files)  {
        my $filecontents;
        my $filename = "$f->{filename}$f->{extension}";
        my $filepath = catfile($self->tmpdir, $filename ne $f->{realname} ? $filename : $f->{realname});
        my $args = {
            db_params => {
                intake_owner => $subject || 'Onbekend',
                created_by   => $subject || 'Onbekend',
            },
            name              => $filename,
            file_path         => $filepath,
        };
        $f = $self->schema->resultset('File')->file_create($args);
    }

    # Remove all temp files;
    $self->parser->filer->purge;

    return \@files;
}

sub _get_user {
    my ($self, $p) = @_;
    my $user = $p->head->get('To');
    if (defined $user) {
        my $intake_user = $self->schema->resultset('Config')->get('document_intake_user');
        ($user) = Email::Address->parse($user);
        my $rs = $self->schema->resultset('Subject')->search_active(
            { username => $user->user }
        );
        if ($rs->count != 1 && $user->user ne $intake_user) {
            throw('/no_owner', sprintf('Document intake user mismatch: %s vs %s', $user, $intake_user));
        }
        return $user->user;
    }
    return undef;
}

sub _get_attachement {
    my ($self, $p) = @_;

    my $raw_filename = $p->head->recommended_filename;
    my $bh;
    if (!$raw_filename) {
        $bh = $p->bodyhandle;
        return undef unless defined $bh;
        $raw_filename = $bh->path;
    }

    # It is called "evil", but it ain't
    my $initial_filename = $raw_filename;
    if ($self->parser->filer->evil_filename($raw_filename)) {
        $raw_filename = $self->parser->filer->exorcise_filename($raw_filename);
    }

    my ($filename, $dir, $ext) = fileparse($raw_filename, '\.[^.]*');
    return undef if $filename =~ /^msg/;

    return {
        realname  => $initial_filename,
        filename  => $filename,
        handle    => $bh,
        extension => $ext,
        directory => $dir
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 parse_message

TODO: Fix the POD

=cut

