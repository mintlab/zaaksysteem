package Zaaksysteem::StUF::0310::Processor;
use Moose;

with 'MooseX::Log::Log4perl';

use Crypt::OpenSSL::Random qw(random_pseudo_bytes);
use DateTime;
use File::Temp;
use List::Util qw(first max);
use List::MoreUtils qw(none);
use MIME::Base64;
use Zaaksysteem::Constants qw(
    MIMETYPES_ALLOWED
);
use Zaaksysteem::XML::Compile;
use Zaaksysteem::Tools;

=head1 NAME

Zaaksysteem::StUF::0310::Processor - Implementations of STUF-ZKN SOAP calls

=head1 DESCRIPTION

StUF-ZKN is a specification for an API to create and manage cases and documents
related to cases.

This class is made to be used from a "process_row" method in one of the
L<Zaaksysteem::Backend::Sysin::Interfaces>.

=head1 ATTRIBUTES

=head2 xml_backend

The L<Zaaksysteem::XML::Compile> instance to use. Defaults to a new
C<Zaaksysteem::XML::Compile> instance, with the StUF-0310 bits loaded, which
should be fine for most use cases.

=cut

has xml_backend => (
    is => 'ro',
    lazy => 1,
    default => sub {
        my $xml_backend = Zaaksysteem::XML::Compile->xml_compile;
        $xml_backend->add_class('Zaaksysteem::XML::Generator::StUF0310');
        return $xml_backend;
    },
);

=head2 schema

L<DBIx::Class> handle to use for accessing the database.

=cut

has schema => (
    is       => 'ro',
    required => 1,
);

=head2 record

The L<Zaaksysteem::Schema::TransactionRecord> instance for the current SOAP action.

=cut

has record => (
    is       => 'ro',
    required => 1,
);

=head2 object_model

The L<Zaaksysteem::Object::Model> instance to use for state advancement.

=cut

has object_model => (
    is       => 'ro',
    required => 1,
);

=head2 betrokkene_model

The L<Zaaksysteem::Betrokkene> instance to use to handle "related people".

=cut

has betrokkene_model => (
    is       => 'ro',
    required => 1,
);

=head1 METHODS

=head2 generate_case_id

Generates a new case id, and returns it in the appropriate "Du02" XML wrapper.

This implements the "genereerZaakidentificatie" SOAP call.

=cut

sig generate_case_id => 'XML::LibXML::XPathContext => Str';

sub generate_case_id {
    my ($self, $xpc) = @_;

    my $generated_id = '0000' . $self->schema->resultset('Zaak')->generate_case_id;
    $self->record->transaction_id->update({ external_transaction_id => "case-" . $generated_id });

    my %remote = $self->_parse_stuurgegevens($xpc, $xpc->findnodes('ZKN:stuurgegevens'));
    $self->_log_case_id($generated_id, \%remote);

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->generate_case_id_return(
        'writer',
        {
            stuurgegevens => {
                berichtcode => "Du02",
                functie     => "genereerZaakidentificatie",
                $self->_build_stuurgegevens($xpc),
            },
            zaak => {
                identificatie => { _ => $generated_id, },
                entiteittype  => 'ZAK',
            },
        },
    );

    return $xml;
}

sub _log_case_id {
    my $self = shift;
    my ($generated_id, $remote) = @_;

    my $logging = $self->schema->resultset('Logging');

    $logging->trigger(
        'case/id_requested',
        {
            component    => 'zaak',
            component_id => $generated_id,
            data => {
                remote_system => $remote->{sender},
                interface     => 'STUF-ZKN',
            },
        }
    );

    return;
}

=head2 get_case_document

Retrieve a case document, as specified by the case id and document id.

Expects a C<edcLv01> message as input, and returns an C<edcLa01> message
containing the file information and contents.

=cut

sig get_case_document => 'XML::LibXML::XPathContext => Str';

sub get_case_document {
    my ($self, $xpc) = @_;

    my $file_id = $xpc->findvalue('ZKN:gelijk/ZKN:identificatie');
    my $case_id = $xpc->findvalue('ZKN:scope/ZKN:object/ZKN:isRelevantVoor/ZKN:gerelateerde/ZKN:identificatie');
    my @casetype_ids = $self->_allowed_casetype_ids();

    my $file = $self->schema->resultset('File')->active()->search(
        {
            'me.id'               => $file_id,
            'me.case_id'          => $case_id,
            'case_id.zaaktype_id' => \@casetype_ids,
        },
        {
            'join' => 'case_id',
        },
    )->first;

    if (!$file) {
        throw(
            "stufzkn/get_case_document/document_not_found",
            sprintf(
                "No document found with id '%s', attached to case '%d' (of casetype '%s')",
                $file_id, $case_id, join(", ", @casetype_ids))
        );
    }

    my %rv = (
        standalone => 1,
        stuurgegevens => {
            berichtcode => "La01",
            entiteittype => "EDC",
            $self->_build_stuurgegevens($xpc),
        },
        parameters => {
            indicatorVervolgvraag => 'false',
        },
        antwoord => {
            object => $self->_format_document_full($file),
        },
    );

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->document_response(
        'writer',
        \%rv,
    );

    return $xml;
}

=head2 get_case_document_lock

Retrieve a case document, as specified by a document id.

Expects a C<geefZaakdocumentbewerken_Di02> message as input, and returns an
C<geefZaakdocumentbewerken_Du02> message containing the file information,
locking info and contents.

=cut

sig get_case_document_lock => 'XML::LibXML::XPathContext => Str';

sub get_case_document_lock {
    my ($self, $xpc) = @_;

    my $file_id = $xpc->findvalue('ZKN:edcLv01/ZKN:gelijk/ZKN:identificatie');
    my @casetype_ids = $self->_allowed_casetype_ids();

    my $file = $self->schema->resultset('File')->active()->search(
        {
            'me.id'               => $file_id,
            'me.case_id'          => { '!=' => undef },
            'case_id.zaaktype_id' => \@casetype_ids,
        },
        {
            'join' => 'case_id',
        },
    )->first;

    if (!$file) {
        throw(
            "stufzkn/get_case_document_lock/document_not_found",
            sprintf(
                "No document found with id '%s', attached to case of types '%s')",
                $file_id, join(", ", @casetype_ids))
        );
    }

    my %rv = (
        stuurgegevens => {
            berichtcode => "Du02",
            functie => "geefZaakdocumentbewerken",
            $self->_build_stuurgegevens($xpc),
        },
        parameters => {
            checked_out_id => unpack('H*', random_pseudo_bytes(16)),
        },
        antwoord => {
            object => $self->_format_document_full($file),
        },
    );

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->document_response_lock(
        'writer',
        \%rv,
    );

    return $xml;
}

sub _format_documents {
    my $self = shift;
    my $files_rs = shift;

    my @rv;

    while (my $file = $files_rs->next) {
        push @rv, {
            entiteittype => 'ZAKEDC',
            gerelateerde => $self->_format_document_minimal($file),
        };
    }

    if (@rv) {
        return (heeftRelevant => \@rv);
    }

    return;
}

sub _format_document_minimal {
    my $self = shift;
    my ($file) = @_;

    my %rv = (
        identificatie  => { _ => '0000' . $file->id },
        type_omschrijving => "",

        isRelevantVoor => {
            gerelateerde => {
                identificatie => '0000' . $file->get_column('case_id'),
            }
        },
        creatiedatum   => $self->_format_stuf_date($file->date_created),

        titel                   => { _ => $file->name },
        formaat                 => { _ => $file->extension },
        versie                  => { _ => $file->version },
        auteur                  => substr($file->created_by, 0, 200) // 'Onbekend',

        taal   => { _ => 'NIL', noValue => 'waardeOnbekend'  },

        vertrouwelijkAanduiding => { _ => 'NIL' , noValue => 'geenWaarde' },
    );

    if (my $md = $file->metadata_id) {
        $rv{vertrouwelijkAanduiding} = { _ => uc($md->trust_level) };
        if ($md->document_category) {
            $rv{type_omschrijving} = $md->document_category;
        }

        if ($md->origin eq 'Inkomend' || $md->origin eq 'Intern') {
            $rv{ontvangstdatum} = $self->_format_stuf_date($md->origin_date);
        }
        elsif ($md->origin eq 'Uitgaand') {
            $rv{verzenddatum}   = $self->_format_stuf_date($md->origin_date);
        }
    }

    return \%rv;
}

sub _format_document_full {
    my $self = shift;
    my ($file) = @_;

    my $rv = $self->_format_document_minimal($file);

    $rv->{inhoud} = {
        contentType  => MIMETYPES_ALLOWED->{$file->extension}{mimetype} // 'application/octet-stream',
        bestandsnaam => $file->filename,
        _            => $file->filestore_id->content,
    };

    return $rv;
}

=head2 get_case_details

Retrieves case details as a C<zakLa01> message, given a C<zakLv01> message on
input.

This contains all of the "base" fields in a case (no attributes, strangely), a
list of documents, the case type, a list of the status history.

=cut

sig get_case_details => 'XML::LibXML::XPathContext => Str';

sub get_case_details {
    my ($self, $xpc) = @_;

    my $case;
    if (my @nodes = $xpc->findnodes('ZKN:gelijk')) {
        $case = $self->_find_case($xpc, @nodes);
    }
    else {
        my $begin = $xpc->findvalue('ZKN:vanaf/ZKN:identificatie');
        my $end   = $xpc->findvalue('ZKN:totEnMet/ZKN:identificatie');
        if ($begin ne $end) {
            throw('stuf/zkn/get_case_details/multiple_cases',
                "Zaaksysteem does not support multiple cases in get_case_details request"
            );
        }
        $case = $self->_find_case($xpc, $xpc->findnodes('ZKN:vanaf'));
    }

    $self->_log_case_retrieval($case->id, $xpc);

    my %rv = (
        stuurgegevens => {
            berichtcode => "La01",
            entiteittype => "ZAK",
            $self->_build_stuurgegevens($xpc),
        },
        parameters => {
            indicatorVervolgvraag => 'false',
        },
        antwoord => {
            object => $self->format_case($case),
        },
    );

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->case_response(
        'writer',
        \%rv,
    );

    return $xml;
}

sub _log_case_retrieval {
    my $self = shift;
    my ($case_id, $xpc) = @_;

    my %remote = $self->_parse_stuurgegevens($xpc, $xpc->findnodes('ZKN:stuurgegevens'));

    my $logging = $self->schema->resultset('Logging');

    $logging->trigger(
        'case/view',
        {
            component    => 'zaak',
            zaak_id      => $case_id,
            data => {
                remote_system => $remote{sender},
                interface     => 'STUF-ZKN',
            },
        }
    );

    return;
}

=head2 format_case

Takes a "zaak" database row and formats it as a StUF-ZKN XML message.

Takes one (positional) argument, the L<Zaaksysteem::Schema::Zaak> row.

=cut

sub format_case {
    my $self = shift;
    my $case = shift;

    # TODO add status
    my %rv = (
        identificatie            => { _ => '0000' . $case->id },
        einddatum                => $self->_format_stuf_date($case->afhandeldatum),
        einddatumGepland         => $self->_format_stuf_date($case->streefafhandeldatum),
        startdatum               => $self->_format_stuf_date($case->registratiedatum),
        uiterlijkeEinddatum      => $self->_format_stuf_date($case->streefafhandeldatum),
        registratiedatum         => $self->_format_stuf_date($case->registratiedatum),
        datumVernietigingDossier => $self->_format_stuf_date($case->vernietigingsdatum),

        zaakniveau => $case->get_column('pid') ? 2 : 1,
        deelzakenIndicatie => { _ => $self->_format_stuf_boolean($case->zaak_pids->count()) },

        $self->_format_status($case),
        $self->_format_betrokkenen($case),
        $self->_format_documents(scalar $case->active_files),

        # TODO opschortingen/verlengingen

        isVan => {
            gerelateerde => {
                code         => $case->zaaktype_node_id->code,
                omschrijving => $case->zaaktype_node_id->titel,
                ingangsdatum => $self->_format_stuf_date($case->zaaktype_node_id->created)
            },
        },
    );

    if (my $o = $case->onderwerp) {
        if (length($o) > 80) {
            $o = substr($o, 0, 80);
        }
        $rv{omschrijving}{_} = $o;
    }

    if (my $t = $case->zaaktype_node_id->toelichting) {
        $rv{toelichting}{_} = $t;
    }

    if (my $resultaat = $case->get_zaaktype_result()) {
        $rv{resultaat} = {
            omschrijving => { _ => $case->resultaat },
            toelichting  => { _ => $resultaat->label // $resultaat->resultaat },
        };
    }

    if ($self->log->is_trace) {
        $self->log->trace("format case: " . dump_terse(\%rv, 5));
    }
    # TODO Related cases?

    return \%rv;
}

sub _format_status {
    my $self = shift;
    my ($case) = @_;

    my $node = $case->zaaktype_node_id;

    # Key on status
    my %zaaktype_statuses = map {
        ($_->status => $_)
    } $node->zaaktype_statussen->search(
        {},
        { order_by => { -asc => 'me.status' } }
    )->all;
    my $last_status = max(keys(%zaaktype_statuses));

    my @statuses;
    for my $status (1 .. $case->milestone) {
        my $is_last = $zaaktype_statuses{$status}->status == $last_status;

        push @statuses, {
            entiteittype => 'ZAKSTT',
            toelichting => $zaaktype_statuses{$status}->naam,
            indicatieLaatsteStatus => $self->_format_stuf_boolean($is_last),
            gerelateerde => {
                entiteittype => 'STT',
                volgnummer => $status,
                code => $node->code . "/" . $status,
                omschrijving => $zaaktype_statuses{$status}->naam,
                omschrijvingGeneriek => $zaaktype_statuses{$status}->fase,
                zaaktype_code => $node->code,
                zaaktype_omschrijving => $node->titel,
            },
        };
    }

    # StUF-ZKN requires statuses to be in a "newest first" order
    return (
        heeft => [ reverse @statuses ]
    );
}

sub _format_betrokkenen {
    my $self = shift;
    my $case = shift;

    my %rv;
    if (my $initiator = $self->_format_person('ZAKBTRINI', $case->aanvrager_object)) {
        $rv{heeftAlsInitiator} = $initiator;
    }
    if (my $uitvoerende = $self->_format_person('ZAKBTRUTV', $case->behandelaar_object)) {
        $rv{heeftAlsUitvoerende} = $uitvoerende;
    }
    if (my $verantwoordelijke = $self->_format_person('ZAKBTRVRA', $case->coordinator_object)) {
        $rv{heeftAlsVerantwoordelijke} = $verantwoordelijke;
    }

    my @gemachtigden = $case->get_betrokkene_objecten({ rol => 'Gemachtigde' });
    for my $gemachtigde (@gemachtigden) {
        push @{ $rv{heeftAlsGemachtigde} }, $self->_format_person('ZAKBTRGMC', $gemachtigde);
    }

    my @belanghebbenden = $case->get_betrokkene_objecten({ rol => 'Belanghebbende' });
    for my $belanghebbende (@belanghebbenden) {
        push @{ $rv{heeftAlsBelanghebbende} }, $self->_format_person('ZAKBTRBLH', $belanghebbende);
    }

    my @overig_betrokkenen = $case->get_betrokkene_objecten(
        {
            -and => [
                { rol => { -not_in => ['Gemachtigde', 'Belanghebbende'] } },
                { rol => { '!=' => undef } },
            ],
        }
    );
    for my $overig_betrokkene (@overig_betrokkenen) {
        push @{ $rv{heeftAlsOverigBetrokkene} }, $self->_format_person('ZAKBTROVR', $overig_betrokkene);
    }

    return %rv;
}

sub _synchronise_betrokkenen {
    my $self = shift;
    my ($case, $type, $current_betrokkenen, $betrokkene_nodes) = @_;

    my @new_betrokkenen = map {
        $self->_parse_related($_)
    } @$betrokkene_nodes;

    # For each current_betrokkenen, check if they're in betrokkene_nodes
    for my $b (@$current_betrokkenen) {
        if (!first { $b->betrokkene_identifier eq $_ } @new_betrokkenen) {
            $self->log->trace("Removing ZaakBetrokkene " . $b->id);
            $b->delete();
        }
        else {
            $self->log->trace("ZaakBetrokkene " . $b->id . " does not need to be deleted.");
        }
    }

    # For each betrokkene_nodes, check if they're in current_betrokkenen
    for my $nb (@new_betrokkenen) {
        if ( !first { $nb eq $_->betrokkene_identifier } @$current_betrokkenen ) {
            $self->log->trace("Adding ZaakBetrokkene '$type' - '$nb'");
            $self->_add_case_relation($case, $type, $nb);
        }
        else {
            $self->log->trace("ZaakBetrokkene '$type' - '$nb' already exists");
        }
    }

    return;
}

sub _format_person {
    my $self = shift;
    my ($entiteittype, $betrokkene) = @_;

    return if not defined $betrokkene;

    my %rv = (
        entiteittype => $entiteittype,
    );

    if ($betrokkene->btype eq 'medewerker') {
        $rv{gerelateerde}{medewerker} = {
            "entiteittype"  => "MDW",
            "identificatie" => $betrokkene->username,
            "achternaam"    => $betrokkene->geslachtsnaam,
            "roepnaam"      => $betrokkene->voornamen,
            "voorletters"   => $betrokkene->voorletters,

            defined($betrokkene->telefoonnummer)
                ? ("telefoonnummer" => $betrokkene->telefoonnummer)
                : (),
            defined($betrokkene->email)
                ? ("emailadres" => $betrokkene->email)
                : (),
        };
    }
    elsif ($betrokkene->btype eq 'natuurlijk_persoon') {
        $rv{gerelateerde}{natuurlijkPersoon} = {
            "entiteittype" => 'NPS',
            "inp.bsn" => $betrokkene->bsn,
            "authentiek" => $self->_format_stuf_boolean($betrokkene->authenticated),
        };
    }
    elsif ($betrokkene->btype eq 'bedrijf') {
        $rv{gerelateerde}{vestiging} = {
            "entiteittype" => 'VES',
            "vestigingsNummer" => $betrokkene->vestigingsnummer,
            "authentiek"       => $self->_format_stuf_boolean($betrokkene->authenticated),
        };
    }
    else {
        throw(
            "stufzkn/unknown_betrokkene_type",
            sprintf("Unknown betrokkene type '%s'", $betrokkene->btype)
        );
    }

    return \%rv;
}

=head2 generate_document_id

Generates a new document id, and return it in a "Du02" wrapper.

This implements the "genereerZaakidentificatie" SOAP call.

=cut

sig generate_document_id => 'XML::LibXML::XPathContext => Str';

sub generate_document_id {
    my ($self, $xpc) = @_;

    my $generated_id = '0000' . $self->schema->resultset('File')->generate_file_id;

    $self->record->transaction_id->update({ external_transaction_id => "doc-" . $generated_id });

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->generate_document_id_return(
        'writer',
        {
            stuurgegevens => {
                berichtcode => "Du02",
                functie => "genereerDocumentidentificatie",
                $self->_build_stuurgegevens($xpc),
            },
            document => {
                identificatie => {
                    _ => $generated_id,
                },
                entiteittype => 'EDC',
            },
        },
    );

    return $xml;
}

=head2 write_case

Create or update a case, based on the received C<zakLk01> message.

If the message contains a "mutatiesoort" "T", it's an addition ("Toevoeging"),
if it has "W" it's a modification ("Wijzigen").

After processing, a "Bv03" return message is generated using the
"stuurgegevens" from the incoming message as a base.

=cut

sig write_case => 'XML::LibXML::XPathContext => Str';

sub write_case {
    my ($self, $xpc) = @_;

    my $mutatie = $xpc->findvalue('ZKN:parameters/StUF:mutatiesoort');
    my $interface = $self->record->transaction_id->interface_id;

    if ($mutatie eq 'T') {
        if (not $interface->jpath('$.allow_new_cases')) {
            throw(
                'stufzkn/case_creation_not_allowed',
                'The administrator has disabled case creation using StUF-ZKN',
            );
        }
        $self->_create_case($xpc);
    }
    elsif ($mutatie eq 'W') {
        $self->_update_case($xpc, $interface);
    }
    else {
        throw(
            'stufzkn/unknown_mutatiesoort',
            "Unknown 'mutatiesoort': '$mutatie'"
        );
    }

    my %remote = $self->_parse_stuurgegevens($xpc, $xpc->findnodes('ZKN:stuurgegevens'));
    return $self->generate_bv03($self->record->get_column('id'), \%remote);
}

=head2 unlock_case_document

Unlock a case document retrieved for editing by another application.

This is a no-op in Zaaksysteem: we don't support locking/unlocking. It always succeeds.

=cut

sub unlock_case_document {
    my ($self, $xpc) = @_;

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->bv02(
        'writer',
        {},
    );

    return $xml;
}

sub _update_case {
    my ($self, $xpc, $interface) = @_;

    # If the new object has a "heeft" block, the other side is trying to update the status.
    if ($xpc->findnodes('ZKN:object[2]/ZKN:heeft')) {
        if (not $interface->jpath('$.allow_phase_update')) {
            throw(
                'stufzkn/phase_update_not_allowed',
                'The administrator has disabled case status updates using StUF-ZKN',
            );
        }
        return $self->_actualiseer_zaak_status($xpc);
    }
    return $self->_update_zaak($xpc);
}

sub _find_case {
    my ($self, $xpc, $object) = @_;

    my $case_id = $xpc->findvalue('ZKN:identificatie', $object);

    if (!$case_id) {
        throw(
            'stufzkn/caseid_not_found',
            "Unable to find case_id in XML",
            $object,
        );
    }
    $self->log->trace("Found case ID: '$case_id'");

    my $case = $self->schema->resultset('Zaak')->find($case_id);

    if (!$case) {
        throw(
            'stufzkn/case_not_found',
            "Case with identifier $case_id was not found",
        );
    }

    $self->assert_casetype($case->get_column('zaaktype_id'));

    return ($case);
}

sub _actualiseer_zaak_status {
    my ($self, $xpc) = @_;

    # We don't care about the "old" object.
    my ($object) = ($xpc->findnodes('ZKN:object'))[-1];

    my $case = $self->_find_case($xpc, $object);

    my $new_phase = $xpc->findvalue('ZKN:heeft/ZKN:gerelateerde/ZKN:volgnummer', $object);
    if (!defined $new_phase) {
        throw(
            'stufzkn/actualiseer_zaak_status/no_volgnummer',
            "'volgnummer' is mandatory for updating the case status",
        );
    }

    if ($new_phase == 1 && $new_phase == $case->milestone) {
        # StUF-ZKN has a concept of a "case without status", i.e. "just a
        # completed form". Zaaksysteem does not.
        #
        # This lets callers think they set the status, without actually doing
        # anything.
        return;
    }

    if ($new_phase != ($case->milestone + 1)) {
        throw(
            'stufzkn/invalid_status_update',
            sprintf(
                "Current status is '%d', cannot set next status '%d'",
                $case->milestone,
                $new_phase,
            ),
        );
    }

    my $username = $xpc->findvalue('ZKN:heeft/ZKN:isGezetDoor/ZKN:medewerker/ZKN:identificatie', $object);
    # XML::Compile doesn't handle "gerelateerde" in StUF very well. So we default to admin.
    my $medewerker = $self->_find_medewerker_by_username($username || 'admin');

    # TODO Set schema->current_user, so deelzaken can be created
    $case->advance(
        object_model     => $self->object_model,
        betrokkene_model => $self->betrokkene_model,
        current_user     => $medewerker,
    );

    return;
}

sub _update_zaak {
    my ($self, $xpc) = @_;

    # We don't care about the "old" object.
    my ($object) = ($xpc->findnodes('ZKN:object'))[-1];

    my $case = $self->_find_case($xpc, $object);

    if (my $einddatum = $xpc->findvalue('ZKN:einddatum', $object)) {
        $case->afhandeldatum($self->_parse_stuf_date($einddatum));
    }
    if (my $einddatumGepland = $xpc->findvalue('ZKN:einddatumGepland', $object)) {
        $case->streefafhandeldatum($self->_parse_stuf_date($einddatumGepland));
    }
    if (my $uiterlijkeEinddatum = $xpc->findvalue('ZKN:uiterlijkeEinddatum', $object)) {
        $case->streefafhandeldatum($self->_parse_stuf_date($uiterlijkeEinddatum));
    }
    if (my $registratiedatum = $xpc->findvalue('ZKN:registratiedatum', $object)) {
        # We ignore ZKN:startdatum, because we don't have the 2 separate dates in ZS.
        $case->registratiedatum($self->_parse_stuf_date($registratiedatum));
    }
    if (my $registratiedatum = $xpc->findvalue('ZKN:registratiedatum', $object)) {
        # We ignore ZKN:startdatum, because we don't have the 2 separate dates in ZS.
        $case->registratiedatum($self->_parse_stuf_date($registratiedatum));
    }
    if (my $datumVernietigingDossier = $xpc->findvalue('ZKN:datumVernietigingDossier', $object)) {
        # If result is also set, actual vernietigingsdatum will be calculated.
        # If it's not set, this one stands.
        $case->vernietigingsdatum($self->_parse_stuf_date($datumVernietigingDossier));
    }
    if (my $archiefnominatie = $xpc->findvalue('ZKN:archiefnominatie', $object)) {
        # We ignore ZKN:startdatum, because we don't have the 2 separate dates in ZS.
        $case->archival_state(
            $self->_parse_stuf_boolean($archiefnominatie)
                ? 'overdragen'
                : 'vernietigen'
        );
    }
    # zaakniveau, deelzakenindicatie are implied in ZS, based on whether the
    # case *has* any deelzaken/moederzaak. They can't be set using STUF-ZKN.

    if (my $resultaat = $xpc->findvalue('ZKN:resultaat/ZKN:omschrijving', $object)) {
        my $result = $case->zaaktype_node_id->zaaktype_resultaten->search(
            {resultaat => lc($resultaat)}
        )->first;

        if (!$result) {
            throw(
                'stufzkn/invalid_result',
                "Resultaat '$resultaat' is niet toegestaan voor dit zaaktype."
            );
        }

        $case->set_resultaat(lc($resultaat));
    }

    if(my ($ini) = $xpc->findnodes('ZKN:heeftAlsInitiator', $object)) {
        my $betrokkene_id = $self->_parse_related($xpc, $ini);
        $case->set_aanvrager($betrokkene_id);
    }

    if (my $behandelaar = $self->_parse_related($xpc, $xpc->findnodes('ZKN:heeftAlsUitvoerende', $object))) {
        throw(
            "stufzkn/uitvoerende_type",
            sprintf("Uitvoerende should be an type 'medewerker' (got '%s')", $behandelaar)
        ) if $behandelaar !~ /^betrokkene-medewerker-/;

        $case->set_behandelaar($behandelaar);
    }
    if (my $coordinator_username = $xpc->findvalue('ZKN:heeftAlsVerantwoordelijke/ZKN:identificatie', $object)) {
        my $coordinator = $self->_find_medewerker_by_username($coordinator_username);
        $case->set_coordinator($coordinator);
    }

    $self->_synchronise_betrokkenen(
        $case,
        'belanghebbende',
        [ $case->get_zaak_betrokkenen({ rol => 'Belanghebbende' })->all ],
        [ $xpc->findnodes('ZKN:heeftAlsBelanghebbende', $object) ],
    );

    $self->_synchronise_betrokkenen(
        $case,
        'gemachtigde',
        [ $case->get_zaak_betrokkenen({ rol => 'Gemachtigde' })->all ],
        [ $xpc->findnodes('ZKN:heeftAlsGemachtigde', $object) ],
    );

    $self->_synchronise_betrokkenen(
        $case,
        'overig',
        [
            $case->get_zaak_betrokkenen({
                -and => [
                    { rol => { -not_in => ['Gemachtigde', 'Belanghebbende'] } },
                    { rol => { '!=' => undef } },
                ],
            })->all
        ],
        [ $xpc->findnodes('ZKN:heeftAlsOverigBetrokkene', $object) ],
    );

    $case->update();
    $case->touch();

    return;
}

sub _allowed_casetype_ids {
    my $self = shift;

    my $interface = $self->record->transaction_id->interface_id;
    my @allowed_ids = $interface->jpath_all('$.casetypes[*].casetype.object_id');

    return @allowed_ids;
}

=head2 assert_casetype

Throw if the specified casetype_id doesn't match the casetype configured in the
interface configuration.

=cut

sub assert_casetype {
    my $self = shift;
    my ($casetype_id) = @_;

    my @allowed_ids = $self->_allowed_casetype_ids();

    if (none { $casetype_id == $_ } @allowed_ids) {
        my $casetype = $self->schema->resultset('Zaaktype')->search(
            { 'me.id' => $casetype_id },
            { prefetch => 'zaaktype_node_id' },
        )->first;
        my @casetypes_allowed = $self->schema->resultset('Zaaktype')->search(
            { 'me.id' => \@allowed_ids },
            { prefetch => 'zaaktype_node_id' },
        )->all;
        throw(
            'stufzkn/casetype_mismatch',
            sprintf(
                "Cannot perform action on a case of casetype %s (%d). " .
                "Configured to only allow %s (%s)",
                $casetype ? $casetype->title : '(no name found)',
                $casetype_id // -1,
                @casetypes_allowed ? join(", ", map { $_->title } @casetypes_allowed) : '(no name found)',
                join(", ", @allowed_ids) // '(none)',
            ),
        );
    }

    return;
}

sub _create_case {
    my ($self, $xpc) = @_;

    my @objects = $xpc->findnodes('ZKN:object');
    if (@objects > 1) {
        throw(
            'stufzkn/multiple_objects_in_create',
            "Multiple objects found in 'creeerZaak', this is not supported."
        );
    }
    my $object = $objects[0];

    my $identifier = $xpc->findvalue('ZKN:identificatie', $object);
    $self->assert_generated_identifier("case-$identifier");

    # XXX This default is here because XML::Compile 1.40+patch can't handle
    # writing isVan in zakLk01 properly (so our own simulator sends
    # bad/incomplete XML our way).
    my $casetype_code = $xpc->findvalue('ZKN:isVan/ZKN:gerelateerde/ZKN:code', $object) || '1234';
    my $casetype_node = $self->_find_casetype_node_by_code($casetype_code);

    $self->assert_casetype($casetype_node->get_column('zaaktype_id'));

    my $requestor = $self->_parse_related($xpc, $xpc->findnodes('ZKN:heeftAlsInitiator', $object));

    if (!$requestor) {
        throw('stuf/zkn/missing_requestor', "Missing requestor in StUF zaken message: ZKN:heeftAlsInitiator missing");
    }
    my $reg_date = $self->_parse_stuf_date($xpc->findvalue('ZKN:registratiedatum', $object));

    my %create_args = (
        override_zaak_id => 1,
        id               => $identifier,
        zaaktype_node_id => $casetype_node->get_column('id'),
        aanvraag_trigger => 'extern',
        contactkanaal    => 'balie',
        registratiedatum => $reg_date,
        kenmerken => [],
        aanvragers => [
            {
                betrokkene  => $requestor,
                verificatie => 'stufzkn',
            }
        ],
    );

    if ($xpc->findnodes('ZKN:kenmerk', $object)) {
        my $interface = $self->record->transaction_id->interface_id;
        my $kenmerk_bron  = $xpc->findvalue('ZKN:kenmerk/ZKN:bron', $object);
        my $kenmerk_value = $xpc->findvalue('ZKN:kenmerk/ZKN:kenmerk', $object);

        # Hardcoded magic string: ztc_extern_kenmerk. Part of the base catalog.
        # If the kenmerk isn't found in this case type, the kenmerk is not saved.
        my $kenmerk = $casetype_node->zaaktype_kenmerken->search_by_magic_strings('ztc_extern_kenmerk')->search(
            { required_permissions => [ undef, '{}' ] },
        )->first;
        if ($kenmerk) {
            push @{ $create_args{kenmerken} }, {
                $kenmerk->get_column('bibliotheek_kenmerken_id') => "$kenmerk_bron|$kenmerk_value"
            };
        }

        $interface->create_related(
            'object_subscriptions',
            {
                external_id  => sprintf("%s|%s", $kenmerk_bron, $kenmerk_value),
                local_table  => 'zaak',
                local_id     => $identifier,
            }
        );
    }

    if (my $vd = $xpc->findvalue('ZKN:datumVernietigingDossier', $object)) {
        $create_args{vernietigingsdatum} = $self->_parse_stuf_date($vd);
    }
    if (my $an = $xpc->findvalue('ZKN:archiefnominatie', $object)) {
        $create_args{archival_state} = $self->_parse_stuf_boolean($an)
            ? 'overdragen'
            : 'vernietigen';
    }

    my $case = $self->schema->resultset('Zaak')->create_zaak(\%create_args);

    if (my $behandelaar = $self->_parse_related($xpc, $xpc->findnodes('ZKN:heeftAlsUitvoerende', $object))) {
        throw(
            "stufzkn/uitvoerende_type",
            sprintf("Uitvoerende should be an type 'medewerker' (got '%s')", $behandelaar)
        ) if $behandelaar !~ /^betrokkene-medewerker-/;

        $case->set_behandelaar($behandelaar);
    }
    if (my $coordinator_username = $xpc->findvalue('ZKN:heeftAlsVerantwoordelijke/ZKN:identificatie', $object)) {
        my $coordinator = $self->_find_medewerker_by_username($coordinator_username);
        $case->set_coordinator($coordinator);
    }

    for my $betrokkene ($xpc->findnodes('ZKN:heeftAlsGemachtigde', $object)) {
        my $betrokkene_id = $self->_parse_related($xpc, $betrokkene);
        $self->_add_case_relation($case, 'gemachtigde', $betrokkene_id);
    }
    for my $betrokkene ($xpc->findnodes('ZKN:heeftAlsBelanghebbende', $object)) {
        my $betrokkene_id = $self->_parse_related($xpc, $betrokkene);
        $self->_add_case_relation($case, 'belanghebbende', $betrokkene_id);
    }
    for my $betrokkene ($xpc->findnodes('ZKN:heeftAlsOverigBetrokkene', $object)) {
        my $betrokkene_id = $self->_parse_related($xpc, $betrokkene);
        $self->_add_case_relation($case, 'overig', $betrokkene_id);
    }

    $case->_touch();

    return;
}

sub _add_case_relation {
    my $self = shift;
    my ($case, $relation_type, $betrokkene_id) = @_;

    $case->betrokkene_relateren(
        {
            betrokkene_identifier => $betrokkene_id,
            rol                   => ucfirst($relation_type),
            magic_string_prefix   => $relation_type,
        }
    );

    return;
}

sub _find_medewerker_by_username {
    my $self = shift;
    my ($username) = @_;

    my $mdw = $self->schema->resultset('Subject')->search(
        {
            subject_type => 'employee',
            username     => $username,
        },
    )->first;

    if (!$mdw) {
        throw(
            'stufzkn/medewerker_not_found',
            "No employee found with username '$username'",
        );
    }

    return $mdw;
}

sub _parse_related {
    my ($self, $xpc, $relation) = @_;
    return if not defined $relation;

    my ($gerelateerde) = $xpc->findnodes('ZKN:gerelateerde', $relation);

    my $ident;
    if ($ident = $xpc->findvalue('ZKN:medewerker/ZKN:identificatie', $gerelateerde)) {
        my $mdw = $self->_find_medewerker_by_username($ident);
        return sprintf("betrokkene-medewerker-%d", $mdw->id);
    }
    elsif ($ident = $xpc->findvalue('ZKN:organisatorischeEenheid/ZKN:identificatie', $gerelateerde)) {
        throw(
            'stufzkn/unsupported_gerelateerde',
            'Unsupported gerelateerde type "organisatorische eenheid"',
        );
    }
    elsif ($ident = $xpc->findvalue('ZKN:natuurlijkPersoon/BG:inp.bsn', $gerelateerde)) {
        my $np = $self->schema->resultset('NatuurlijkPersoon')->get_by_bsn($ident);

        return sprintf("betrokkene-natuurlijk_persoon-%d", $np->id);
    }
    elsif ($ident = $xpc->findvalue('ZKN:nietNatuurlijkPersoon/BG:inn.nnpId', $gerelateerde)) {
        # This is a guess. According to documentation, this should be RSIN
        # which we don't store.
        my $bedrijf = $self->schema->resultset('Bedrijf')->search({
            dossiernummer => $ident
        })->first;

        if (!$bedrijf) {
            throw(
                "stufzkn/niet_natuurlijk_persoon_not_found",
                "No bedrijf found with dossiernummer '$ident'"
            );
        }

        return sprintf("betrokkene-bedrijf-%d", $bedrijf->id);
    }
    elsif ($ident = $xpc->findvalue('ZKN:vestiging/BG:vestigingsNummer', $gerelateerde)) {
        my $bedrijf = $self->schema->resultset('Bedrijf')->search({
            vestigingsnummer => $ident
        })->first;

        if (!$bedrijf) {
            throw(
                "stufzkn/niet_natuurlijk_persoon_not_found",
                "No bedrijf found with vestigingsnummer '$ident'"
            );
        }

        return sprintf("betrokkene-bedrijf-%d", $bedrijf->id);
    }

    throw(
        'stufzkn/unsupported_gerelateerde',
        "Unsupported gerelateerde type\n" . $gerelateerde->toString(1)
    );
}

sub _find_casetype_node_by_code {
    my $self = shift;
    my ($casetype_code) = @_;

    my @casetype_nodes = $self->schema->resultset('ZaaktypeNode')->search(
        {
            code => $casetype_code,
            deleted => undef,
        }
    )->all;
    throw(
        'stufzkn/no_nodes',
        "No casetype found with code '$casetype_code'"
    ) unless @casetype_nodes;
    throw(
        'stufzkn/too_many_nodes',
        "More than one case type found with code '$casetype_code'"
    ) if @casetype_nodes > 1;

    return $casetype_nodes[0];
}

=head2 write_case_document_lock

Process an incoming C<edcLk01> message, which adds a new document to a case.

Returns a C<Bv03> message to indicate success.

=cut

sig write_case_document_lock => 'XML::LibXML::XPathContext => Str';

sub write_case_document_lock {
    my ($self, $xpc) = @_;

    my @objects = $xpc->findnodes('ZKN:edcLk02/ZKN:object');
    my $object = $objects[-1];

    my $document_id = $xpc->findvalue('ZKN:identificatie', $object);
    my @casetype_ids = $self->_allowed_casetype_ids();

    my $existing = $self->schema->resultset('File')->active()->search(
        {
            'me.id'               => $document_id,
            'me.case_id'          => { '!=' => undef },
            'case_id.zaaktype_id' => \@casetype_ids,
        },
        {
            'join' => 'case_id',
        },
    )->first;

    if (!$existing) {
        throw(
            'stufzkn/update_to_nonexistant_file',
            sprintf("File with ID '%s' does not exist", $document_id),
        );
    }

    my $tmp = File::Temp->new(UNLINK => 1);
    print $tmp decode_base64($xpc->findvalue('ZKN:inhoud', $object));
    $tmp->close();

    my $new_file = $self->schema->resultset('File')->file_create({
        name       => $existing->filename,
        file_path  => $tmp->filename,
        db_params => {
            case_id    => $existing->get_column('case_id'),
            created_by => '', # TODO retrieve/require a 'nobody' user
        },
    });

    my $instance = $self->xml_backend->stuf0310;
    my $xml = $instance->bv02(
        'writer',
        {},
    );

    return $xml;
}

=head2 write_case_document

Process an incoming C<edcLk01> message, which adds a new document to a case.

Returns a C<Bv03> message to indicate success.

=cut

sig write_case_document => 'XML::LibXML::XPathContext => Str';

sub write_case_document {
    my ($self, $xpc) = @_;

    my @objects = $xpc->findnodes('ZKN:object');
    my $object = $objects[-1];

    my $document_id = $xpc->findvalue('ZKN:identificatie', $object);
    my ($case_node) = $xpc->findnodes('ZKN:isRelevantVoor/ZKN:gerelateerde', $object);
    my $filename = $xpc->findvalue('ZKN:inhoud/@StUF:bestandsnaam', $object);

    my ($case) = $self->_find_case($xpc, $case_node);
    my $case_id = $case->id;
    $self->assert_generated_identifier("doc-$document_id");

    my $tmp = File::Temp->new(UNLINK => 1);
    print $tmp decode_base64($xpc->findvalue('ZKN:inhoud', $object));
    $tmp->close();

    # TODO pass in file id
    my $file = $self->schema->resultset('File')->file_create({
        name       => $filename,
        file_path  => $tmp->filename,
        db_params => {
            id         => $document_id,
            case_id    => $case_id,
            created_by => '', # TODO retrieve/require a 'nobody' user
        },
    });

    my %metadata = (
        description => $xpc->findvalue('ZKN:titel', $object),

        # StUF uses ALL CAPS, we only capitalize the first letter
        trust_level => ucfirst(lc($xpc->findvalue('ZKN:vertrouwelijkAanduiding', $object))),
    );
    # Ignored: taal, auteur, titel
    if (my $vd = $xpc->findvalue('ZKN:verzenddatum', $object)) {
        $metadata{origin} = 'Uitgaand';
        $metadata{origin_date} = $self->_parse_stuf_date($vd)->strftime('%Y-%m-%d'),
    }
    elsif (my $od = $xpc->findvalue('ZKN:ontvangstdatum', $object)) {
        $metadata{origin} = 'Inkomend';
        $metadata{origin_date} = $self->_parse_stuf_date($od)->strftime('%Y-%m-%d'),
    }

    $file->update_metadata(\%metadata);

    my %remote = $self->_parse_stuurgegevens($xpc, $xpc->findnodes('ZKN:stuurgegevens'));
    return $self->generate_bv03($self->record->get_column('id'), \%remote);
}

=head2 generate_bv03

Generate a "Bv03" response message, which is used as a response to most "write"
actions in the STUF-ZKN API.

=cut

sig generate_bv03  => 'Str, HashRef => Str';

sub generate_bv03 {
    my $self = shift;
    my ($reference, $remote) = @_;

    my $instance = $self->xml_backend->stuf0310;

    my $xml = $instance->bv03(
        'writer',
        {
            stuurgegevens => {
                berichtcode => 'Bv03',
                # We just swap the sender/receiver from the incoming message around.
                zender    => { applicatie => $remote->{receiver} },
                ontvanger => { applicatie => $remote->{sender} },
                referentienummer => $reference,
                tijdstipBericht  => DateTime->now()->strftime('%Y%m%d%H%M%S%3N'),
                crossRefnummer   => $remote->{reference},
            },
        }
    );

    return $xml;
}

=head2 generate_fo03

Generate a "Fo03" response message, which the generic "error" response
in the STUF-ZKN API.

=cut

sig generate_fo03  => 'XML::LibXML::XPathContext, Any => Str';

sub generate_fo03 {
    my $self = shift;
    my ($xpc, $exception) = @_;

    my $instance = $self->xml_backend->stuf0310;

    my %remote = $self->_parse_stuurgegevens($xpc, $xpc->findnodes('ZKN:stuurgegevens'));

    my $description;
    if (blessed $exception) {
        if ($exception->can('type')) {
            $description = $exception->type;
        }
        else {
            $description = ref($exception);
        }
    }

    my $xml = $instance->fo03(
        'writer',
        {
            stuurgegevens => {
                berichtcode => 'Fo03',
                # We just swap the sender/receiver from the incoming message around.
                zender    => { applicatie => $remote{receiver} },
                ontvanger => { applicatie => $remote{sender} },
                referentienummer => $self->record->id,
                tijdstipBericht  => DateTime->now()->strftime('%Y%m%d%H%M%S%3N'),
                crossRefnummer   => $remote{reference},
            },
            body => {
                code => 'StUF058', # Proces voor afhandelen bericht geeft fout
                plek => 'server',
                omschrijving => substr($description, 0, 200),
                details      => substr("$exception", 0, 1000),
            },
        }
    );

    return $xml;
}

=head2 assert_generated_identifier

Assert that the given identifier was generated in an earlier transaction.

Throws C<stufzkn/unclaimed_id> if the identifier can't be found.

=cut

sig assert_generated_identifier  => 'Str';

sub assert_generated_identifier {
    my $self = shift;
    my ($id) = @_;

    my $transaction = $self->schema->resultset('Transaction')->search(
        {
            external_transaction_id => $id,

            interface_id  => $self->record->transaction_id->get_column('interface_id'),
            success_count => { '>' => 0 },
        }
    )->first;

    if(!$transaction) {
        throw(
            'stufzkn/unclaimed_id',
            sprintf("ID '%s' was not generated by an earlier call", $id),
        );
    }

    return;
}

sub _parse_stuurgegevens {
    my ($self, $xpc, $stuurgegevens) = @_;

    my %rv;

    $rv{reference} = $xpc->findvalue('StUF:referentienummer', $stuurgegevens);
    $rv{sender}    = $xpc->findvalue('StUF:zender/StUF:applicatie', $stuurgegevens);
    $rv{receiver}  = $xpc->findvalue('StUF:ontvanger/StUF:applicatie', $stuurgegevens);

    return %rv;
}

sub _build_stuurgegevens {
    my ($self, $xpc) = @_;

    my %remote = $self->_parse_stuurgegevens($xpc, $xpc->findnodes('ZKN:stuurgegevens'));

    return (
        zender          => { applicatie => $remote{receiver} },
        ontvanger       => { applicatie => $remote{sender} },
        tijdstipBericht => DateTime->now()->strftime('%Y%m%d%H%M%S%3N'),

        referentienummer => $self->record->id,
        defined($remote{reference})
            ? (crossRefnummer  => $remote{reference})
            : ()
    );
}


sub _format_stuf_date {
    my $self = shift;
    my $date = shift;

    if (not defined $date) {
        return {
            noValue => 'geenWaarde',
            _ => 'NIL',
        }
    }

    # StUF has its own special date format.
    return {
        _ => $date->strftime('%Y%m%d'),
    };
}

sub _parse_stuf_date {
    my $self = shift;
    my ($date) = @_;

    return unless length($date);

    $date =~ /
        ^
        (?<year>[0-9]{4})
        (?<month>[0-9]{2})
        (?<day>[0-9]{2})
    /x or throw(
        'stufzkn/malformed_date',
        "StUF date isn't in the correct format (yyyymmdd expected)"
    );

    return DateTime->new(%+);
}

sub _format_stuf_boolean {
    my $self = shift;
    my ($value) = @_;

    return $value ? 'J' : 'N';
}

sub _parse_stuf_boolean {
    my $self = shift;
    my ($value) = @_;

    if ($value eq 'J') {
        return 1;
    }
    elsif ($value eq 'N') {
        return 0;
    }
    else {
        throw(
            'stufzkn/unknown_boolean',
            "Unknown value for boolean: '$value'"
        );
    }
}

__PACKAGE__->meta->make_immutable();
1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
