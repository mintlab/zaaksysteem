package Zaaksysteem::XML::Zorginstituut::Converter;
use Moose;

extends 'Zaaksysteem::XML::Zorginstituut::Reader';

=head1 NAME

Zaaksysteem::XML::Zorginstituut::Converter - An Zorginstituut Converter reader for ZS

=head1 SYNOPSIS

    use Zaaksysteem::XML::Zorginistituut::Converter;

    my $reader = Zaaksysteem::XML::Zorginistituut::Converter->new(
        xml => $xml_string,
    );

    $reader->is_valid();
    my $datastructure = $reader->show_invalid();

=head1 DESCRIPTION

This module will read a Zorginstituut "convert" message, either XML to ASCII or ASCII to XML and check if there are errors
or returns the content of the message.

=cut

has '+ns' => (default => 'con');

around _xpath_builder => sub {
    my $orig = shift;
    my $self = shift;

    my $xp = $self->$orig(@_);
    $xp->registerNs('soap', 'http://schemas.xmlsoap.org/soap/envelope/');
    $xp->registerNs('con', 'http://www.istandaarden.nl/berichten/conversie/data');
    return $xp;
};

sub _build_message_mapping {
    return {};
}

=head2 is_valid

Tells you whether the answer from the converter service is valid.

=cut

sub is_valid {
    my $self = shift;

    my $bericht = $self->get_bericht;
    if ($bericht) {
        return 1;
    }
    return 0;
}

=head2 get_bericht

Get the converted message from the converter service XML.

=cut

sub get_bericht {
    my $self = shift;

    my $bericht = $self->find('//con:bericht');
    if ($bericht) {
        return $self->get_one_node_from_nodelist($bericht);
    }
    return undef;
}

=head2 show_errors

Returns all the errors in a array ref.

=cut

sub show_errors {
    my $self   = shift;
    my $errors = $self->find('//con:fouten/con:Fout');

    my @rv;
    while (my $e = $errors->shift) {
        my $foutcode     = $e->find('con:foutCode')->shift->textContent;
        my $beschrijving = $e->find('con:omschrijving')->shift->textContent;
        push(@rv, { foutCode => $foutcode, omschrijving => $beschrijving });
    }
    return \@rv;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
