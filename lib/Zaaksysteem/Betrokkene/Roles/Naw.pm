package Zaaksysteem::Betrokkene::Roles::Naw;
use Moose::Role;

=head1 NAME

Zaaksysteem::Betrokkene::Roles::Naw - A role for betrokkene address functions

=head1 METHODS

=head2 get_volledig_huisnummer_from_adres

Make the house number human readable

=cut


sub get_volledig_huisnummer_from_adres {
    my ($self, $adres) = @_;

    return '' if (!$adres);

    my $huisnummer = $adres->huisnummer // '';
    if ($adres->huisletter) {
        $huisnummer .= " " . $adres->huisletter;
    }
    if ($adres->huisnummertoevoeging) {
        $huisnummer .= " - " . $adres->huisnummertoevoeging;
    }
    return $huisnummer;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
