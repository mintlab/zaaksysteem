#!/usr/bin/perl

use strict;
use warnings;
use Data::UUID;

use Zaaksysteem::Constants qw/GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES/;

my $constant = GEGEVENSMAGAZIJN_KVK_RECHTSVORMCODES;

for my $key (sort keys %$constant) {
    my $label = $constant->{$key};

    print "    {\n";
    print "        id       => '" . Data::UUID->new->create_str() . "',\n";
    print "        code     => '" . $key . "',\n";
    print "        label    => '" . $label . "',\n";
    print "    },\n"
}

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
