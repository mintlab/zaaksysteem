#!/usr/bin/perl

use strict;
use warnings;

use Cwd 'realpath';
use FindBin;
use lib "$FindBin::Bin/../lib";

use Getopt::Long;
use Zaaksysteem::Config;
use Pod::Usage;
use File::Spec::Functions;

use Text::CSV;


use Time::HiRes qw(gettimeofday tv_interval);

my %opt = (
    help       => 0,
    config     => '/etc/zaaksysteem/zaaksysteem.conf',
    customer_d => '/etc/zaaksysteem/customer.d',
    n          => 0,
);

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(
                help
                n
                config=s
                customer_d=s
                hostname=s
                filename=s
                type=s
                interface_id=s
                )
        );
    };
    if (!$ok) {
        pod2usage(1) ;
    }
}

pod2usage(0) if ($opt{help});

foreach (qw(config customer_d hostname type filename interface_id)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        pod2usage(1) ;
    }
}

my $ZS = Zaaksysteem::Config->new(
    zs_customer_d => $opt{customer_d},
    zs_conf       => $opt{config},
);

my $schema = $ZS->get_customer_schema($opt{hostname}, 1);

my %typeconfig = (
    bedrijf     => {
        cols        => [qw/subscription_id kvknummer straatnaam huisnummer/],
        resultset   => 'Bedrijf',
    },
    natuurlijk_persoon => {
        cols        => [qw/subscription_id burgerservicenummer/],
        resultset   => 'NatuurlijkPersoon',
    }
);

# my $subscriptions = collect_subscriptions($schema);
load_subscriptions($schema, collect_subscriptions($schema));

sub load_subscriptions {
    my $dbic            = shift;
    my $subscriptions   = shift;

    my $count = 0;
    my $total = scalar(@$subscriptions);
    for my $subscription (@$subscriptions) {
        my $subject_entry   = get_subject_entry($dbic, $subscription, ++$count, $total) or next;

        my $db_subs         = $dbic->resultset('ObjectSubscription')->search(
            {
                'interface_id'  => $opt{interface_id},
                'local_table'   => $typeconfig{ $opt{type} }->{resultset},
                'local_id'      => $subject_entry->id,
            }
        );

        if ($db_subs->count) {
            my $entry = $db_subs->first;

            do_transaction(
                $dbic,
                sub {
                    $entry->update(
                        {
                            external_id     => $subscription->{subscription_id},
                            date_created    => DateTime->now,
                        }
                    );
                }
            );

            next;
        }

        do_transaction(
            $dbic,
            sub {
                $db_subs->create(
                    {
                        'interface_id'  => $opt{interface_id},
                        'local_table'   => $typeconfig{ $opt{type} }->{resultset},
                        'local_id'      => $subject_entry->id,
                        'external_id'   => $subscription->{subscription_id},
                        'date_created'  => DateTime->now(),
                    }
                )
            }
        );
    }
}

sub get_subject_entry {
    my $dbic            = shift;
    my $subscription    = shift;
    my $count           = shift;
    my $total           = shift;

    if ($opt{type} eq 'natuurlijk_persoon') {
        my $burgerservicenummer = sprintf("%09d", $subscription->{burgerservicenummer});
        
        my $entries         = $dbic->resultset('NatuurlijkPersoon')->search(
            {
                'burgerservicenummer'   => $burgerservicenummer,
                'authenticated'         => 1,
                'authenticatedby'       => 'gba',
            }
        );

        my $dbcount = $entries->count;
        if ($dbcount > 1) {
            print "  $count / $total Cannot set subscription because of multiple entries for burgerservicenummer: " . $burgerservicenummer . "\n";
            return;
        }
        
        if ($dbcount) {
            print "  " . $count . " / $total importing: " . $burgerservicenummer . "\n";
            return $entries->first;
        } else {
            print "  " . $count . " / $total skipping because not found: " . $burgerservicenummer . "\n";
            return;
        }
    } elsif ($opt{type} eq 'bedrijf') {
        my $entries         = $dbic->resultset('Bedrijf')->search(
            {
                'dossiernummer'         => $subscription->{kvknummer},
                'authenticated'         => 1,
                'authenticatedby'       => 'kvk',
            }
        );

        if ($entries->count > 1) {
            ### Lets check for street
            $entries = $entries->search(
                {
                    vestiging_straatnaam => $subscription->{straatnaam},
                }
            );

            my $dbcount = $entries->count;
            if ($dbcount > 1) {
                print "  $count / $total Cannot set subscription because of multiple entries for burgerservicenummer: " . $subscription->{kvknummer} . "\n";
                return;
            }

            if ($dbcount) {
                print "  " . $count . " / $total importing: " . $subscription->{kvknummer} . "\n";
                return $entries->first;
            } else {
                print "  " . $count . " / $total skipping because not found: " . $subscription->{kvknummer} . "\n";
                return;
            }
        }

        my $dbcount = $entries->count;

        if ($dbcount) {
            print "  " . $count . " / $total importing: " . $subscription->{kvknummer} . "\n";
            return $entries->first;
        } else {
            print "  " . $count . " / $total skipping because not found: " . $subscription->{kvknummer} . "\n";
            return;
        }
        
        return $entries->first;
    }

}


sub collect_subscriptions {
    my $dbic    = shift;


    die('Invalid type, need "bedrijf" or "natuurlijk_persoon"') unless $typeconfig{ $opt{type} };

    my $csv     = Text::CSV->new ( { binary => 1 } )  # should set binary attribute.
        or die "Cannot use CSV: ".Text::CSV->error_diag ();

    $csv->column_names(@{ $typeconfig{$opt{type}}->{cols}});

    open my $fh, "<:encoding(utf8)", $opt{filename} or die $opt{filename} . ": $!";

    ### Skip header
    my (@subscriptions);
    my $header = 0;
    my $count = 0;

    print STDERR "Loading subscriptions from CSV\n";
    while ( my $row = $csv->getline_hr( $fh ) ) {
        ++$count;
        if ($count % 100 == 0) {
            print STDERR "  " . $count . " processed from CSV\n";
        }
        if (!$header) {
            print STDERR "Skipping first line because of header \n";
            $header++;
            next;
        }
        # print Data::Dumper::Dumper($row);
        push(@subscriptions, $row);
    }

    $csv->eof or $csv->error_diag();
    close $fh;

    return \@subscriptions;
}

sub do_transaction {
    my ($dbic, $sub) = @_;

    $dbic->txn_do(
        sub {
            $sub->();
            if ($opt{n}) {
                $dbic->txn_rollback;
            }
        }
    );
}

1;

__END__

=head1 NAME

touch_casetype.pl - A casetype hstore fixer

=head1 SYNOPSIS

    touch_case.pl OPTIONS [ [ --casetypes 1234 ] [--casetypes 12345 ] ]

    # From vagrant:
    /dev-bin/mark_subscription_for_csv.pl --customer_d /vagrant/etc/customer.d --config /vagrant/etc/zaaksysteem.conf --hostname 10.440.0.11



=head1 OPTIONS

=over

=item * config

The Zaaksysteem configuration defaults to /etc/zaaksysteem/zaaksysteem.conf

=item * type [required]

    --type bedrijf
    --type natuurlijk_persoon

The type of StUF entity to set a subscription id for, one of: "natuurlijk_persoon"
or "bedrijf" supported

=item * interface_id [required]

    --interface_id 44

The interface ID to set this subscription on, see koppelingen > configuratiescherm

=item * filename [required]

    --filename bedrijven.csv

Filename of csv file to import from, format per type:

B<bedrijf>

    sleutelgegevensbeheer,kvknummer,straatnaam,huisnummer
    1234567890,37564589,de hoofdlaan,44
    1234567891,26564590,langestraat,33
    1234567892,16564595,laat,24

B<natuurlijk_persoon>

    sleutelgegevensbeheer,burgerservicenummer
    1234567890,564568656
    1224567891,325678456

=item * customer_d

The customer_d dir, defaults to /etc/zaaksysteem/customer.d

=item * hostname

The hostname you want to touch cases for

=item * casetypes

You can use this to update specific case types, not supplying this will touch all case types.

=item * n

Dry run, run it, but don't

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
