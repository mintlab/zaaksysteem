#
# Cookbook Name:: zaaksysteem
# Recipe:: clientutils
#
# Contains the different client utils, like the BBV app proxy
#

cookbook_file "/etc/init/zaaksysteem-bbvproxy.conf" do
    source "clientutils/init-zaaksysteem-bbvproxy.conf"
    mode 00600
    owner "root"
    group "root"
    notifies :reload, 'service[zaaksysteem-bbvproxy]', :delayed
end

service "zaaksysteem-bbvproxy" do
    provider Chef::Provider::Service::Upstart
    supports :status => true, :restart => true
    action [ :enable, :start ]
end
