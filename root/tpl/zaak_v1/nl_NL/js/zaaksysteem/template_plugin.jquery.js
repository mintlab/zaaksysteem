// From http://docs.jquery.com/Plugins/Authoring
// Replace ezra_map with ezra_[plugin name]

(function( $ ){
    var methods = {
        required: function(options) {
            var required = ['map_type'];

            if (!options && required && required.length) { 
                $(this).ezra_map(
                    'log',
                    'ezra_map error: No options given'
                );
                return false;
            }

            for(var i = 0; i < required.length; i++) {
                var param = required[i];

                if(!options[param]) {
                    $(this).ezra_map(
                        'log',
                        'ezra_map error: Missing required parameter ' + param
                    );
                    return false;
                }
            }

            return true;
        },
        init : function( options ) {
            return this.each(function(){
                var $this   = $(this),
                    data    = $this.data('ezra_map');

                // Check required parameters
                if (!$this.ezra_map('required')) {
                    return false;
                }

                // If the plugin hasn't been initialized yet
                if ( ! data ) {
                    /*
                    Do more setup stuff here
                    */
                    $(this).data('ezra_map', {
                        target : $this,
                    });
                }
            });
        },
        destroy : function( ) {
            return this.each(function(){
                var $this = $(this),
                    data = $this.data('ezra_map');

                // Namespacing FTW
                $(window).unbind('.ezra_map');
                data.ezra_map.remove();
                $this.removeData('ezra_map');
            });
        },
        //reposition : function( ) { // ... },
        //show : function( ) { // ... },
        //hide : function( ) { // ... },
        //update : function( content ) { // ...}
        log: function(message) {
            if (typeof console != 'undefined' && typeof console.log == 'function') {
                console.log(message);
            }
        }
    };

    $.fn.ezra_map = function( method ) {
        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.ezra_map' );
        }
    };
})( jQuery );
