BEGIN;

ALTER TABLE object_relation DROP CONSTRAINT object_relation_object_id_fkey;
ALTER TABLE object_relation DROP CONSTRAINT object_relation_object_uuid_fkey;

ALTER TABLE object_relation ADD CONSTRAINT object_relation_object_id_fkey FOREIGN KEY (object_id) REFERENCES object_data(uuid) ON DELETE CASCADE DEFERRABLE;
ALTER TABLE object_relation ADD CONSTRAINT object_relation_object_uuid_fkey FOREIGN KEY (object_uuid) REFERENCES object_data(uuid);

COMMIT;