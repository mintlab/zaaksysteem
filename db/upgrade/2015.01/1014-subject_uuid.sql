BEGIN;

ALTER TABLE zaak_betrokkenen ALTER COLUMN uuid SET DEFAULT uuid_generate_v4();

COMMIT;
