package TestFor::General::Object::Types::Instance;

# ./zs_prove -v t/lib/TestFor/General/Object/Types/Instance.pm
use base qw(ZSTest);

use TestSetup;
use Zaaksysteem::Object::Types::Instance;

sub zs_object_types_instance : Tests {
    my $model = $zs->object_model;

    $zs->zs_transaction_ok(
        sub {
            my %opts = (
                fqdn     => 'testsuite.zaaksysteem.nl',
                label    => 'Zaaksysteem testsuite omgeving',
                owner    => 'betrokkene-bedrijf-1',
                template => 'meuk',
                customer_type => 'government',
            );

            my %defaults = (
                disabled       => 'true',
                deleted        => 'false',
                fallback_url   => 'http://www.mintlab.nl'
            );

            my ($obj, $saved) = $zs->create_object_ok('Instance', \%opts, \%defaults);

            throws_ok(
                sub {
                    $model->save_object(object => $obj);
                },
                qr#object/unique/constraint: Object unique constraint violation: fqdn#,
                "FQDN is unique"
            );

            lives_ok(
                sub { $model->save_object(object => $saved) },
                "Saved object can be saved if it exists"
            );

            my $now = DateTime->now();

            %opts = (
                %opts,
                fqdn           => Zaaksysteem::TestUtils::generate_random_string . ".testsuite.zaaksysteem.nl",
                database       => 'zs_db_testsuite',
                filestore      => '/path/to/storage',
                otap           => 'production',
                protected      => '1',
                network_acl    => ['194.134.5.5 # Ye olde employee'],
            );

            ($obj) = $zs->create_object_ok('Instance', \%opts, { %defaults, protected => 'true' });

            my $object = $model->save(
                json => $zs->open_testsuite_json('instance.json'));

            my $cc = $zs->create_customer_config_ok;

            my $relation = $object->add_to_customer($cc);
            ok($relation, "Relationship has been created");

            lives_ok(
                sub { $object->add_to_customer($cc) },
                "Creating an existing relationship is allowed"
            );

            throws_ok(
                sub {
                    $object->add_to_customer(
                        $model->save_object(
                            object => $zs->create_customer_config_ok()
                        )
                    );
                },
                qr/Existing relationship, unable to set new relationship/,
                "Existing relationship, polygamy is not allowed here",
            );

        },
        'Instance object'
    );
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
