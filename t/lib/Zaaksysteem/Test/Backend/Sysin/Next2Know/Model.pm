package Zaaksysteem::Test::Backend::Sysin::Next2Know::Model;

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Next2Know::Model - Test Next2Know model

=head2 SYNOPSIS

    prove -l :: Zaaksysteem::Test::Backend::Sysin::Next2Know::Model

=cut

use Zaaksysteem::Test;
use Zaaksysteem::Backend::Sysin::Next2Know::Model;

sub test_setup {
    my $test        = shift;

    my $test_method = $test->test_report->current_method->name;

    if ('test_oauth_live' eq $test_method && !$ENV{ZS_NEXT2KNOW_OAUTH_URL}) {
        $test->test_skip("Skipping $test_method: set ZS_NEXT2KNOW_OAUTH_URL");
    }
}

sub _create_next2know_ok {
    my $options = {@_};
    my $n2k = Zaaksysteem::Backend::Sysin::Next2Know::Model->new(
        oauth_endpoint => $options->{oauth_endpoint}
            // 'https://foo.bar.nl/v1/oauth/access_token',
        grant_type => $options->{grant_type} // 'client_credentials',
        username => $options->{client_id}  // 'zs-testsuite',
        password => $options->{client_secret} // 'very-secret',
    );
    isa_ok($n2k, 'Zaaksysteem::Backend::Sysin::Next2Know::Model');
    return $n2k;
}

sub test_oauth {

    my $n2k = _create_next2know_ok();

    no warnings qw(redefine once);
    use HTTP::Response;
    my $answer = qq({
       "token_type" : "Bearer",
       "expires_in" : 3600,
       "access_token" : "qHV5LrNsaBXQx6loTHHaYyrro4iFKlfurXHLSzwI",
       "success" : true
    });
    local *LWP::UserAgent::request = sub {
        my $self = shift;
        return HTTP::Response->new(200, undef, undef,$answer);
    };
    use warnings;

    my $token = $n2k->get_oauth_token();

    is($token->{success}, 1, "Token call is successful");
    is($token->{token_type}, 'Bearer', "Has an 'Bearer' token type");

    is($token->{access_token}, 'qHV5LrNsaBXQx6loTHHaYyrro4iFKlfurXHLSzwI', "Has an access token");
    is($token->{expires_in}, 3600, "Token expires in 3600");

}

sub test_oauth_live {

    my $n2k = _create_next2know_ok(
        oauth_endpoint => $ENV{ZS_NEXT2KNOW_OAUTH_URL},
        grant_type     => 'client_credentials',
        client_id      => $ENV{ZS_NEXT2KNOW_OAUTH_CLIENT_ID},
        client_secret  => $ENV{ZS_NEXT2KNOW_OAUTH_CLIENT_SECRET}
    );

    my $token = $n2k->get_oauth_token();

    # Real live tests indicate success not in the JSON
    #is($token->{success}, 1, "Token call is successful");

    is($token->{token_type}, 'Bearer', "Has an 'Bearer' token type");

    isnt($token->{access_token}, undef, "Has an access token");
    isnt($token->{expires_in}, undef, "Token expires");

    note explain $token;
}



1;

__END__

=head1 NAME

Zaaksysteem::Test::Backend::Sysin::Next2Know::Model - A Next2Know test package

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
