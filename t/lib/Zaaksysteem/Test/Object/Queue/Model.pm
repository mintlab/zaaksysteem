package Zaaksysteem::Test::Object::Queue::Model;

use Zaaksysteem::Test;
use Zaaksysteem::StatsD;

use Zaaksysteem::Object::Queue::Model;

=head1 NAME

Zaaksysteem::Test::Object::Queue::Model - Test Queue model

=head1 DESCRIPTION

Test queue model

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::Object::Queue::Model

=head2 test_run

=cut

sub test_run {
    my $log = {};

    ### Mock
    my $role  = mock_moose_class(
        {
            superclasses => ['Zaaksysteem::Object::Queue::Model'],
            methods => {
                test_handler => sub {
                    $log->{test_handler} = 1;
                }
            },
        },
        {
            table => mock_dbix_resultset(resultset => 'Queue'),
        }
    );

    my $item = mock_dbix_component('Queue',
        {
            data => {
                natuurlijk_persoon_id => 44,
            },
            label => 'Update cases of deceased persons',
            type => 'test_handler',
            status => 'pending',
        }
    );

    $item = $role->run($item);
    is($item->status, 'finished', 'Succesfully finished handler');
    is(${ $item->date_started }, 'statement_timestamp() at time zone \'UTC\'', 'Correct date_started');
    is(${ $item->date_finished }, 'statement_timestamp() at time zone \'UTC\'', 'Correct date_finished');
    ok($log->{test_handler}, 'Succesfully ran "test_handler" queue item');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
