/*global angular,fetch*/
(function () {

    angular.module('Zaaksysteem.user')
        .controller('nl.mintlab.user.UserLegacyController', [ '$scope', 'smartHttp', 'translationService', 'systemMessageService', function ( $scope, smartHttp, translationService, systemMessageService ) {

            var form,
                labsState;

            function setFormValues ( ) {
                if (form && labsState) {
                    form.setValue('use_legacy', labsState.case_version === 1);
                }
            }

            smartHttp.connect({
                method: 'GET',
                url: '/api/user/settings/lab'
            })
                .success( function ( data ) {

                    labsState = data.result[0];

                    setFormValues();

                });
            
            $scope.$on('form.ready', function ( event ) {

                if(event.targetScope.getName() === 'use_legacy') {
                        
                    form = event.targetScope;

                    setFormValues();

                }
            });
            
            
            $scope.saveValues = function ( values ) {
                smartHttp.connect({
                    method: 'POST',
                    url: '/api/user/settings/lab',
                    data: _.omit(
                        _.assign({ desktop_version: 2 }, labsState, { case_version: values.use_legacy ? 1 : 2 }),
                        'object_search'
                    )
                })
                    .success(function ( data ) {
                        systemMessageService.emitSave();
                    })
                    .error(function ( ) {
                        systemMessageService.emitSaveError();  
                    });
            };

        }]);
}());
