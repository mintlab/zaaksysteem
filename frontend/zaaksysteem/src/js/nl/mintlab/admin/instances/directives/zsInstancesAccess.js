/*global angular,_,fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.admin.instances')
		.directive('zsInstancesAccess', [ '$parse', 'systemMessageService', 'instancesService', function ( $parse, systemMessageService, instancesService ) {			
			
			var cancelEvent = fetch('nl.mintlab.utils.events.cancelEvent'),
				generateUid = fetch('nl.mintlab.utils.generateUid');
			
			return {
				controller: [ '$scope', '$element', '$attrs', function ( $scope, $element, $attrs ) {
					
					var ctrl = this,
						formVisible = false,
						instance,
						items = [],
						startedWith,
						submitting = false,						
						config = {
							columns: [
								{
									id: 'title',
									label: 'Titel',
									resolve: 'title',
									locked: true,
									dynamic: true
								},
								{
									id: 'address',
									label: 'Adres',
									resolve: 'address',
									locked: true,
									dynamic: true
								},
								{
									id: 'remove',
									label: 'Acties',
									templateUrl: '/html/admin/instances/instances.html#access-remove',
									locked: true,
									dynamic: true
								}
							],
							options: {
								select: 'none',
								resolve: 'id'
							},
							actions: []
						};
						
					instance = $scope.$eval($attrs.instance);
					items = _.map(instance.values.network_acl, function ( entry ) {
						var spl = entry.split(' '),
							title = spl.splice(1).join(' '),
							address = spl[0];
						
						return {
							title: title,
							address: address,
							id: generateUid()
						};
					});
					
					startedWith = angular.copy(items);
					
					function getControlPanelId ( ) {
						return $scope.$eval($attrs.customerId);
					}
					
					ctrl.getCrudConfig = function ( ) {
						return config;
					};
						
					ctrl.getCrudItems = function ( ) {
						var crudItems = items;
						return crudItems;
					};
					
					ctrl.toggleForm = function ( ) {
						formVisible = !formVisible;
					};
					
					ctrl.openForm = function ( ) {
						formVisible = true;
					};
					
					ctrl.closeForm = function ( ) {
						formVisible = false;	
					};
					
					ctrl.isFormVisible = function ( ) {
						return formVisible;	
					};
					
					ctrl.addAddress = function ( title, address ) {
						items.push({
							title: title,
							address: address,
							id: generateUid()
						});
					};
					
					ctrl.removeAddress = function ( item ) {
						_.pull(items, item);	
					};
					
					ctrl.hasUnsavedChanges = function ( ) {
						return !angular.equals(items, startedWith);	
					};
					
					ctrl.saveChanges = function ( ) {
						var entries = _.map(items, function ( item ) {
								return item.address + ' ' + item.title;
							});
						
						submitting = true;
						
						instancesService.updateInstance(getControlPanelId(), instance, {
							network_acl: entries
						})
							['finally'](function ( ) {
								submitting = false;
							})
							.then(function ( ) {
								$parse($attrs.zsInstancesAccessSave)($scope, {
									$entries: entries
								});
								$scope.closePopup();
							})
							['catch'](function ( ) {
								systemMessageService.emitSaveError();
							});
						
					};
					
					ctrl.handleRemoveClick = function ( item, event ) {
						ctrl.removeAddress(item);
						cancelEvent(event);
					};
					
					return ctrl;
					
				}],
				controllerAs: 'instancesAccess'
			};
			
		}]);
	
})();
