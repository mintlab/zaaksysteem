/*global angular, fetch*/
(function ( ) {
	
	angular.module('Zaaksysteem.kcc')
		.controller('nl.mintlab.kcc.CallEnabledController', [ '$scope', 'callService', function ( $scope, callService ) {
			
			var safeApply = fetch('nl.mintlab.utils.safeApply');
			
			$scope.toggleEnabled = function ( ) {
				if(callService.isEnabled()) {
					callService.disable();
				} else {
					callService.enable();				
				}
			};
			
			$scope.isEnabled = function ( ) {
				return callService.isEnabled();
			};
			
			$scope.isAvailable = function ( ) {
				return callService.isAvailable();	
			};
			
			$scope.$on('call.enabled.change', function ( ) {
				safeApply($scope);
			});
			
			$scope.$on('call.available.change', function ( ) {
				safeApply($scope);
			});
			 
		}]);
	
})();