/*global angular,_,fetch*/
(function ( ) {

	angular.module('Zaaksysteem.case')
		.directive('zsCaseTemplateForm', [ '$rootScope', '$injector', '$http', '$q', '$timeout', 'caseDocumentService', 'systemMessageService', 'translationService', function ( $rootScope, $injector, $http, $q, $timeout, caseDocumentService, systemMessageService, translationService ) {

			var File = fetch('nl.mintlab.docs.File');

			return {
				restrict: 'E',
				templateUrl: '/html/case/case.html#template-form',
				scope: {
					caseId: '&',
					values: '&',
					templates: '&',
					showTemplateSelect: '&',
					caseDocs: '&',
					actions: '&',
					onDone: '&'
				},
				controller: [ '$scope', function ( $scope ) {

					var ctrl = this,
						submitting = false,
						redirectUrl;

					function done ( ) {
						$scope.onDone();
					}

					function createFileFromData ( data ) {
						var file = new File(),
							fileData = data.data;

						file.updating = true;
						
						file.updateWith(fileData);
						
						file.case_documents = _.map(fileData.case_documents, function ( caseDocId ) {
							return caseDocumentService.getCaseDocById(caseDocId);
						});

						$timeout(function ( ) {
							file.updating = false;
						});

						return file;
					}

					function createTemplate ( ) {

						var params = {
							'case_id': Number($scope.caseId()),
							name: ctrl.values.name,
							'zaaktype_sjabloon_id': ctrl.values.template.id,
							'target_format': ctrl.values.targetFormat
						};
						
						if(ctrl.values.caseDocument) {
							params.case_document_ids = ctrl.values.caseDocument.id;
						}
						
						return $http( {
							url: 'file/file_create_sjabloon',
							method: 'POST',
							data: params
						});
					}

					function handleFormSubmit ( ) {

						ctrl.formPhase = 'submitting';

						createTemplate()
							.then(function ( response ) {

								var data = response.data;

								ctrl.formPhase = data.type;

								switch(data.type) {
									case 'pending':
									
									break;

									case 'redirect':
									redirectUrl = data.data.document.resume_url;
									break;

									case 'created':
									systemMessageService.emit('info', 'Sjabloon "' + ctrl.values.name + '" toegevoegd');
									$rootScope.$emit('fileFromTemplate', createFileFromData(data));
									done();
									break;

									default:
									return $q.reject(data);
								}

							})
							.catch(function ( ) {
								systemMessageService.emitSaveError();
								ctrl.formPhase = 'creating';
							});
					}

					ctrl.getRedirectUrl = function ( ) {
						return redirectUrl;
					};

					ctrl.isDisabled = function ( ) {
						var form = $scope['template-form'];
						return (form ? form.$invalid : false) || submitting;
					};

					ctrl.handleActionClick = function ( action ) {

						switch(action.name) {
							case 'submit':
							handleFormSubmit();
							break;

							default:
							submitting = true;
							$injector.invoke(action.click, null, { $values: angular.copy(ctrl.values) })
								.then(done)
								.finally(function ( ) {
									submitting = false;
								});
							break;
						}

					};

					ctrl.handleCloseClick = function ( ) {
						done();
					};

					ctrl.handleTemplateChange = function ( ) {
						var template = ctrl.values.template,
							name = ctrl.values.name;
							
						if(!name
							|| _.filter($scope.templates() || [], function ( template ) {
								return template.bibliotheek_sjablonen_id.naam === name;
							}).length
						) {

							if(!template) {
								name = '';
							} else {
								name = template.bibliotheek_sjablonen_id.naam;
							}
							ctrl.values.name = name;
						}
						
						ctrl.values.targetFormat = ctrl.values.template && ctrl.values.target_format ? ctrl.values.target_format : 'odt';
					};

					ctrl.formPhase = 'creating';

					ctrl.values = _.assign(
						{
							template: null,
							targetFormat: null,
							caseDoc: null
						},
						$scope.values()
					);


				}],
				controllerAs: 'caseTemplateForm'

			};

		}]);

})();
