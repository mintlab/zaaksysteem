/*global define*/
(function ( ) {
	
window.define('nl.mintlab.utils.object.extendProto', function ( ) {
		return function ( D, S ) {
			var protoSource = S.prototype,
				protoDest = D.prototype,
				key;
			
			for(key in protoSource) {
				protoDest[key]= protoSource[key];
			}
		};
	});
	
})();
