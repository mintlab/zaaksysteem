/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('zsAnchorLink', [ '$window', function ( $window ) {

		return {
			link: function ( scope, element, attrs ) {

				var href = attrs.href,
					anchor = $window.location.pathname + $window.location.search + href;
				
				element.attr('href', anchor);

			}
		};

	}]);

})();
