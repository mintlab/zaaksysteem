/*global angular*/
(function ( ) {

	angular.module('Zaaksysteem')
		.directive('ngEnter', function ( ) {
			return function ( scope, element, attrs ) {
				element.bind('keyup', function ( event ) {
					if(event.keyCode === 13) {
						scope.$eval(attrs.ngEnter);
					}
				});
				
			};
		});
})();