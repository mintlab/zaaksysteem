import e2epasswords from '../../e2epasswords.json';

export default ( username ) => e2epasswords[username];
