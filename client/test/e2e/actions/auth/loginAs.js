import attemptLogin from './attemptLogin';
import getPassword from './getPassword';

export default ( username ) => {
	return attemptLogin(username, getPassword(username));
};
