import openAction from './../../actions/common/create/openAction';
import selectFromSuggest from './../../actions/common/object/selectFromSuggest';
import getFieldSelector from './../../actions/common/form/getFieldSelector';
import first from 'lodash/first';

describe('when starting a case registration', ( ) => {

	beforeAll(( ) => {

		let form = $('zs-contextual-action-menu form');

		openAction('zaak');

		selectFromSuggest(
			form.$(getFieldSelector('casetype')),
			'basic casetype',
			first
		);

		form.$(getFieldSelector('requestor_type'))
			.$(`input[type=radio][value=natuurlijk_persoon`)
			.click();

		form.submit();

	});

	it('should redirect to zaak/create', ( ) => {

		expect(browser.getCurrentUrl()).toContain('/zaak/create/balie');

	});

	describe('and completing the steps', ( ) => {

		beforeAll(( ) => {

			$('[name=submit_to_next]').click();

			browser.driver.wait(( ) => {
				return browser.driver.getCurrentUrl().then((url) => {
					return url.indexOf('submit_to_next=1') !== -1;
				});
			}, 5000);

		});

		it('should move to the next phase', ( ) => {

			expect(browser.getCurrentUrl()).toContain('submit_to_next=1');

		});

		describe('and submitting the form', ( ) => {

			beforeAll(( ) => {

				$('[name=submit_to_next]').click();

			});

			it('should redirect to the case', ( ) => {

				browser.driver.wait(( ) => {
					return browser.driver.getCurrentUrl().then((url) => {
						return /intern\/zaak/.test(url);
					});
				}, 5000);

				expect(browser.getCurrentUrl()).toContain('/intern/zaak');

			});

		});

	});

	afterAll(( ) => {

		$('.contextual-action-form-close').click();

	});

});
