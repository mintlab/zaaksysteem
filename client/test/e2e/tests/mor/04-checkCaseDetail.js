describe('when clicking on a case in the caseList', ( ) => {

	beforeAll(( ) => {

		browser.get('/mor');

	});

	it('should navigate to the detailView', ( ) => {

		let caseItem = element.all(by.css('case-list-item-list-item')).first();
		
		caseItem.click();

		expect(browser.getCurrentUrl()).toContain('!melding/');

	});

	it('should contain a detailView', ( ) => {

		let caseDetailView = element.all(by.css('case-detail-view'));
		
		expect(caseDetailView.count()).toBeGreaterThan(0);

	});

	it('should have a detailView that contains a handle button', ( ) => {

		let button = element.all(by.css('.case-detail-view__button-container button'));
		
		expect(button.count()).toBeGreaterThan(0);

	});

	it('should have a detailView that contains a handle button that after a click takes a case into behandeling', ( ) => {

		let button = element.all(by.css('.case-detail-view__button-container button'));
		
		button.click();

		expect(browser.getCurrentUrl()).toContain('/afhandelen');

	});

	afterAll(( ) => {

		browser.get('/intern');

	});

});
