import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import angularUiRouterModule from 'angular-ui-router';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import getBase64fromArrayBuffer from '../../shared/getBase64fromArrayBuffer';
import displayContentsInWord from '../../shared/displayContentsInWord';
import onGotAllSlices from '../../shared/onGotAllSlices';
import './styles.scss';

export default
		angular.module('Zaaksysteem.officeaddins.word.caseDocumentView', [
			composedReducerModule,
			angularUiRouterModule,
			snackbarServiceModule
		])
		.directive('caseDocumentView', [ '$window', '$http', '$timeout', '$state', '$stateParams', 'composedReducer', 'snackbarService', ( $window, $http, $timeout, $state, $stateParams, composedReducer, snackbarService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					caseDocument: '&',
					fileId: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						titleReducer,
						editMode = 'open',
						fileUuidReducer,
						fileUuid;

					titleReducer = composedReducer( { scope: $scope }, ctrl.caseDocument() )
						.reduce( caseDoc => caseDoc.instance.name);

					fileUuidReducer = composedReducer( { scope: $scope }, ctrl.caseDocument() )
						.reduce( caseDoc => {
							return caseDoc.instance.id;
						});

					fileUuid = fileUuidReducer.data();


					ctrl.getTitle = titleReducer.data;

					ctrl.getMode = ( ) => editMode;

					// Downloading doc from ZS and inserting contents into open Word document
					ctrl.insertIntoDocument = ( ) => {

						$http( {
							method: 'GET',
							url: `/api/v1/case/${$stateParams.caseId}/document/${$stateParams.fileId}/download`,
							responseType: 'arraybuffer'
						}).then( ( response ) => {
							displayContentsInWord( getBase64fromArrayBuffer(response.data) );
							editMode = 'edit';
						});

					};

					// Getting file from Word and sending it to ZS
					let sendFile = ( file ) => {

						let fd = new FormData();

						fd.append('file', file, titleReducer.data() );
						fd.append('file_uuid', fileUuid );

						let promise =
							$http({
								method: 'POST',
								url: '/file/update_file',
								transformRequest: angular.identity,
								headers: {
									'Content-Type': undefined
								},
								data: fd
							})
							.then( response => {

								fileUuid = response.data.result[0].filestore_id.uuid;

							});

						snackbarService.wait('Nieuwe versie van bestand wordt opgeslagen op de server', {
							promise,
							then: ( ) => 'Nieuwe versie van bestand is opgeslagen op de server',
							catch: ( ) => {
								return 'Niet gelukt om nieuwe versie van bestand op te slaan. Neem contact op met uw beheerder voor meer informatie.';
							}
						});

					};

					function getSliceAsync(file, nextSlice, sliceCount, gotAllSlices, docdataSlices, slicesReceived) {
						file.getSliceAsync(nextSlice, (sliceResult) => {
	
							if (sliceResult.status === 'succeeded') {
								if (!gotAllSlices) {
									return;
								}

								docdataSlices[sliceResult.value.index] = sliceResult.value.data;

								if (++slicesReceived === sliceCount) { //eslint-disable-line
									file.closeAsync();

									sendFile(onGotAllSlices(docdataSlices));

								} else {
									getSliceAsync(file, ++nextSlice, sliceCount, gotAllSlices, docdataSlices, slicesReceived); //eslint-disable-line
								}
							} else {
								gotAllSlices = false; //eslint-disable-line
								file.closeAsync();
							}
						});
					}
					ctrl.handleSaveClick = ( ) => {

						Office.context.document.getFileAsync(Office.FileType.Compressed, { sliceSize: 655360 /*64 KB*/ }, //eslint-disable-line
							(result) => {
								if (result.status === 'succeeded') {

									let myFile = result.value,
										sliceCount = myFile.sliceCount,
										slicesReceived = 0, gotAllSlices = true, docdataSlices = [];
									
									getSliceAsync(myFile, 0, sliceCount, gotAllSlices, docdataSlices, slicesReceived);

								} else {

									snackbarService.error('Er is iets mis gegaan bij het verwerken van het document. Neem contact op met uw beheeder voor meer informatie.');

								}
							}
						);
					};

					ctrl.goBack = ( ) => {
						$state.go('caseDetail', { caseId: $stateParams.caseId });
					};

				}],
				controllerAs: 'caseDocumentView'

			};
		}
		])
		.name;
