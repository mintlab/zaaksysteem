import angular from 'angular';
import caseDocumentViewModule from './caseDocumentView';
import resourceModule from '../../shared/api/resource';
import composedReducerModule from '../../shared/api/resource/composedReducer';
import snackbarServiceModule from '../../shared/ui/zsSnackbar/snackbarService';
// import auxiliaryRouteModule from '../../shared/util/route/auxiliaryRoute';
import onRouteActivateModule from '../../shared/util/route/onRouteActivate';
import zsModalModule from '../../shared/ui/zsModal';
import viewTitleModule from '../../shared/util/route/viewTitle';
import template from './index.html';
import first from 'lodash/first';
import seamlessImmutable from 'seamless-immutable';
import './styles.scss';

export default {
	moduleName:
		angular.module('Zaaksysteem.officeaddins.word.caseDocument', [
			resourceModule,
			caseDocumentViewModule,
			snackbarServiceModule,
			// auxiliaryRouteModule,
			onRouteActivateModule,
			zsModalModule,
			viewTitleModule,
			composedReducerModule
		])
		.name,
	config: [
		{
			route: {
				url: '/zaak/:caseId/document/:fileId',
				resolve: {
					caseDocument: [ '$rootScope', '$stateParams', '$q', 'resource', 'snackbarService', ( $rootScope, $stateParams, $q, resource, snackbarService ) => {

						let caseResource = resource(
							{ url: `/api/v1/case/${$stateParams.caseId}/document/${$stateParams.fileId}` },
							{ scope: $rootScope }
						)
							.reduce( ( requestOptions, data ) => {
								return ( first(data) || seamlessImmutable({}) );
							});

						return caseResource
							.asPromise()
							.then( ( ) => caseResource
							)
							.catch( ( err ) => {

								snackbarService.error('Het document kon niet worden geladen. Neem contact op met uw beheerder voor meer informatie.');

								return $q.reject(err);

							});

					}]
				},
				auxiliary: true,
				onActivate: [ '$state', '$timeout', '$rootScope', '$window', '$document', '$compile', 'viewTitle', '$stateParams', 'zsModal', 'caseDocument', ( $state, $timeout, $rootScope, $window, $document, $compile, viewTitle, $stateParams, zsModal, caseDocument ) => {

					let scrollEl = angular.element($document[0].querySelector('.body-scroll-container'));

					scrollEl.css({ overflow: 'hidden' });

					let openModal = ( ) => {

						let modal,
							unregister,
							scope = $rootScope.$new(true);

						scope.caseDocument = caseDocument;

						modal = zsModal({
							el: $compile(angular.element(template))(scope),
							title: viewTitle.get(),
							classes: 'document-modal'
						});

						modal.open();

						modal.onClose( ( ) => {
							$window.history.back();
							return true;
						});

						unregister = $rootScope.$on('$stateChangeStart', ( ) => {

							$window.requestAnimationFrame( ( ) => {

								$rootScope.$evalAsync(( ) => {

									modal.close()
										.then(( ) => {

											scope.$destroy();

											scrollEl.css({
												overflow: '',
												height: ''
											});

										});

									unregister();

								});

							});
							
						});

					};

					$window.requestAnimationFrame( ( ) => {
						$rootScope.$evalAsync(openModal);
					});

				}]

			},
			state: 'caseDocument'
		}
	]
};
