import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import angularUiRouterModule from 'angular-ui-router';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import onGotAllSlices from '../../shared/onGotAllSlices';
import zsModalModule from '../../../shared/ui/zsModal';
import './styles.scss';

export default
		angular.module('Zaaksysteem.officeaddins.word.caseDetailView', [
			composedReducerModule,
			angularUiRouterModule,
			snackbarServiceModule,
			zsModalModule
		])
		.directive('caseDetailView', [ '$sce', '$compile', '$http', '$state', 'composedReducer', 'snackbarService', 'zsModal', ( $sce, $compile, $http, $state, composedReducer, snackbarService, zsModal ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					caseItem: '&',
					caseDocuments: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						titleReducer,
						caseIdReducer,
						caseUuidReducer;

					titleReducer = composedReducer( { scope: $scope }, ctrl.caseItem() )
						.reduce( caseItem => `${caseItem.instance.number}: ${caseItem.instance.subject_external || caseItem.instance.casetype.instance.name || '-'}`);

					caseIdReducer = composedReducer( { scope: $scope }, ctrl.caseItem() )
						.reduce( caseItem => caseItem.instance.number );

					caseUuidReducer = composedReducer( { scope: $scope }, ctrl.caseItem() )
						.reduce( caseItem => caseItem.reference );

					ctrl.getTitle = titleReducer.data;

					ctrl.getDocuments = ctrl.caseDocuments;

					ctrl.getCaseUuid = caseUuidReducer.data;

					let sendFile = ( file ) => {

						let fd = new FormData();

						fd.append('file', file, `${ctrl.filename}.docx` );
						fd.append('case_id', caseIdReducer.data());

						let promise =
							$http({
								method: 'POST',
								url: '/file/create',
								transformRequest: angular.identity,
								headers: {
									'Content-Type': undefined
								},
								data: fd
							});

						snackbarService.wait(`${ctrl.filename}.docx wordt opgeslagen op de server`, {
							promise,
							then: ( ) => `${ctrl.filename}.docx is opgeslagen op de server`,
							catch: ( ) => {
								return `Niet gelukt om ${ctrl.filename}.docx op te slaan. Neem contact op met uw beheerder voor meer informatie.`;
							}
						});

					};

					function getSliceAsync(file, nextSlice, sliceCount, gotAllSlices, docdataSlices, slicesReceived) {
						file.getSliceAsync(nextSlice, (sliceResult) => {
	
							if (sliceResult.status === 'succeeded') {
								if (!gotAllSlices) {
									return;
								}

								docdataSlices[sliceResult.value.index] = sliceResult.value.data;

								if (++slicesReceived === sliceCount) { //eslint-disable-line
									file.closeAsync();

									sendFile(onGotAllSlices(docdataSlices));

								} else {
									getSliceAsync(file, ++nextSlice, sliceCount, gotAllSlices, docdataSlices, slicesReceived); //eslint-disable-line
								}
							} else {
								gotAllSlices = false; //eslint-disable-line
								file.closeAsync();
							}
						});
					}

					let filenameTemplate = '<div class="input_container"><input ng-model="caseDetailView.filename" /><span>.docx</span></div><button ng-disabled="!caseDetailView.filename.length" ng-click="caseDetailView.confirmFilename()" class="btn btn-secondary">Opslaan</button>';

					ctrl.handleSaveClick = ( ) => {

						//Get the URL of the current file.
						Office.context.document.getFilePropertiesAsync( asyncResult => { //eslint-disable-line

							let fileUrl = asyncResult.value.url,
								file = String(fileUrl.match(/[^/]+$/g)),
								filename = file.replace(/\.[^/.]+$/, '');

							if (fileUrl !== '') {
								ctrl.filename = filename;
							}

						});

						let modal = zsModal({
								el: $compile(angular.element(filenameTemplate))($scope),
								title: 'Geef bestandsnaam op',
								classes: 'filename-modal'
							});

							modal.onClose( ( ) => {
								modal.close();
								return true;
							});

							modal.open();

					};

					ctrl.confirmFilename = ( ) => {

						Office.context.document.getFileAsync(Office.FileType.Compressed, { sliceSize: 655360 /*64 KB*/ }, //eslint-disable-line
							(result) => {
								if (result.status === 'succeeded') {

									let myFile = result.value,
										sliceCount = myFile.sliceCount,
										slicesReceived = 0, gotAllSlices = true, docdataSlices = [];
									
									getSliceAsync(myFile, 0, sliceCount, gotAllSlices, docdataSlices, slicesReceived);

								} else {

									snackbarService.error('Er is iets mis gegaan bij het verwerken van het document. Neem contact op met uw beheeder voor meer informatie.');

								}
							}
						);
					};

					ctrl.goBack = ( ) => {
						$state.go('caseSearch');
					};

				}],
				controllerAs: 'caseDetailView'

			};
		}
		])
		.name;
