import angular from 'angular';
import template from './template.html';

export default
	angular.module('zsRolePicker', [
	])
		.directive('zsRolePicker', [ ( ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					onRoleSelect: '&',
					role: '&'
				},
				bindToController: true,
				controller: [ function ( ) {

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
