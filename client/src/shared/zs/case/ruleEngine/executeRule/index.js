import matchRule from './../matchRule';
import evaluateFormula from './evaluateFormula';
import defaults from 'lodash/defaults';
import propCheck from './../../../../util/propCheck';

export default
	( rule, current, options = {} ) => {

		propCheck.throw(
			propCheck.shape({
				debug: propCheck.bool.optional
			}),
			options
		);

		let matches = matchRule(rule, current.values, current.hidden),
			actions = matches ? rule.then : rule.else,
			debug = !!options.debug,
			debugInfo = current.debug;

		let addDebug = ( attrName, type ) => {

			if (!debugInfo[attrName]) {
				debugInfo[attrName] = [];
			}

			debugInfo[attrName].push({
				rule: {
					label: rule.label,
					matches
				},
				type
			});

		};

		return actions.reduce(( result, action ) => {

			let attrName = action.data.attribute_name;

			switch (action.type) {
				case 'set_value':
				if (!action.data.can_change) {
					result.values[attrName] = action.data.value;
					result.disabled[attrName] = true;
					if (debug) {
						addDebug(attrName, 'set_value');
					}
				} else {
					result.prefill[attrName] = action.data.value;
					if (debug) {
						addDebug(attrName, 'set_value_editable');
					}
				}
				
				break;

				case 'hide_attribute':
				case 'show_attribute':
				// only unhide attribute if it isn't hidden by group
				
				result.hidden[attrName] = action.type === 'hide_attribute' || result.hiddenByGroup[attrName];
				result.hiddenByAttribute[attrName] = action.type === 'hide_attribute';

				if (debug) {
					addDebug(attrName, action.type);
				}

				break;

				case 'hide_group':
				case 'show_group':

				if (debug) {
					addDebug(attrName, action.type);
				}

				action.data.related_attributes.forEach(( attr ) => {
					result.hiddenByGroup[attr] = (action.type === 'hide_group');

					// only unhide attribute if it wasn't hidden individually
					if (action.type === 'hide_group' || result.hiddenByAttribute[attr] !== true) {
						result.hidden[attr] = (action.type === 'hide_group');
					}

					if (debug) {
						addDebug(attr, action.type);
					}

				});
				break;

				case 'set_value_formula':
				result.values[attrName] = evaluateFormula(action.data.formula, result.values);
				result.disabled[attrName] = true;

				if (debug) {
					addDebug(attrName, action.type);
				}

				break;

				case 'pause_application':
				console.log('error: pause_application rule not implemented yet');
				break;
			}

			return result;

		}, defaults(current, { hidden: {}, hiddenByGroup: {}, hiddenByAttribute: {}, values: {}, disabled: { }, prefill: {}, debug: { } }));

	};
