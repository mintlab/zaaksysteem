import isEmpty from 'lodash/isEmpty';
import pick from 'lodash/pick';
import pickBy from 'lodash/pickBy';
import identity from 'lodash/identity';
import assign from 'lodash/assign';
import mapKeys from 'lodash/mapKeys';
import omit from 'lodash/omit';

let remoteFields =
	[ 'address_residence.zipcode', 'address_residence.street_number' ];

let isBsnRequired = ( vals ) => {
	return vals.remote && isEmpty(
		pickBy(
			pick(vals, ...remoteFields),
			identity
		)
	);
};

export default ( locals = { modules: [ ] } ) => {

	let modules = locals.modules,
		remoteField;

	if (modules.length === 1) {
		remoteField = {
			name: 'remote',
			template: 'checkbox',
			label: '',
			data: {
				checkboxLabel: 'Zoeken in BRP'
			}
		};
	} else if (modules.length > 1) {
		remoteField = {
			name: 'remote',
			template: 'select',
			label: 'Bron',
			data: {
				notSelectedLabel: 'Intern',
				options: modules.asMutable().map(
					module => {

						return {
							value: module.instance.id,
							label: module.instance.interface_config.search_form_title || module.instance.name
						};
					}
				)
			}
		};
	}

	return {
		format: ( values ) => {

			if (modules.length === 1) {
				return values.merge({
					remote: values.remote ?
						modules[0].instance.id
						: null
				});
			}

			return values;

		},
		parse: ( values ) => {

			if (modules.length === 1) {
				return values.merge({ remote: !!values.remote });
			}

			return values;

		},
		request: ( values ) => {

			return {
				method: 'POST',
				url: !values.remote ?
					'/api/v1/subject'
					: `/api/v1/subject/remote_search/${values.remote}`,
				data:
					{
						query: {
							match: assign(
								{
									subject_type: 'person'
								},
								mapKeys(
									omit(
										pickBy(values, identity),
										'remote'
									),
									( value, key ) => `subject.${key}`
								)
							)
						}
					}
			};
		},
		fields:
			[
				{
					name: 'personal_number',
					label: 'BSN',
					template: 'text',
					required: [ '$values', isBsnRequired ]
				},
				{
					name: 'prefix',
					label: 'Voorvoegsel',
					template: 'text'
				},
				{
					name: 'family_name',
					label: 'Achternaam',
					template: 'text'
				},
				{
					name: 'gender',
					label: 'Geslacht',
					template: 'radio',
					data: {
						options: [
							{
								value: 'M',
								label: 'Man'
							},
							{
								value: 'V',
								label: 'Vrouw'
							}
						]
					}
				},
				{
					name: 'date_of_birth',
					label: 'Geboortedatum',
					template: 'date'
				},
				{
					name: 'address_residence.street',
					label: 'Straat',
					template: 'text'
				},
				{
					name: 'address_residence.zipcode',
					label: 'Postcode',
					template: 'text',
					required: [ '$values', ( vals ) => {

						return vals.remote && !vals.personal_number;

					}]
				},
				{
					name: 'address_residence.street_number',
					label: 'Huisnummer',
					template: 'text',
					required: [ '$values', ( vals ) => {

						return vals.remote && !vals.personal_number;

					}]
				},
				{
					name: 'address_residence.city',
					label: 'Woonplaats',
					template: 'text'
				}
			].concat( remoteField ? remoteField : []),
		columns:
			[
				{
					id: 'gender',
					template:
						`<zs-icon
							icon-type="alert-circle"
							ng-show="item.messages"
							zs-tooltip="{{::item.messages || ''}}"
						></zs-icon>
						<zs-icon icon-type="account"></zs-icon>`,
					label: ''
				},
				{
					id: 'personal_number',
					label: 'BSN'
				},
				{
					id: 'first_names',
					label: 'Voornamen'
				},
				{
					id: 'family_name',
					label: 'Achternaam'
				},
				{
					id: 'address',
					label: 'Adres',
					template:
						'<span>{{::item.address}}</span>'
				}
			]
	};
};
