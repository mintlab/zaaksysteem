import angular from 'angular';
import propCheck from './../../../util/propCheck';
import merge from 'lodash/merge';
import assign from 'lodash/assign';
import result from 'lodash/result';
import mapValues from 'lodash/mapValues';
import pickBy from 'lodash/pickBy';
import identity from 'lodash/identity';

let templates = {
	'natuurlijk_persoon_description': '{{object.adres_id.straatnaam}} {{object.adres_id.huisnummer || \'\'}} {{object.adres_id.huisletter || \'\'}} {{object.adres_id.huisnummertoevoeging || \'\'}}, {{object.adres_id.postcode}} {{object.adres_id.woonplaats}}',
	'bedrijf_description': '{{object.vestiging_adres}}, {{object.vestiging_postcodewoonplaats}}'
};

export default
	angular.module('shared.object.zsObjectSuggest.objectSuggestionService', [
	])
		.factory('objectSuggestionService', [ '$interpolate', ( $interpolate ) => {

			templates = mapValues(templates, ( tpl ) => {
				return tpl
					.replace(/{{/g, $interpolate.startSymbol())
					.replace(/}}/g, $interpolate.endSymbol());
			});

			let transformers =
				{
					natuurlijk_persoon: ( item ) => {

						let birthDate = item.object.geboortedatum ? ` (${item.object.geboortedatum})` : '',
							description = $interpolate(templates.natuurlijk_persoon_description, false, null, true)(pickBy(item, identity));

						return {
							label: item.object.decorated_name + birthDate,
							description,
							link: `/betrokkene/${item.object.id}/?gm=1&type=natuurlijk_persoon`
						};
					},
					bedrijf: ( item ) => {

						let description = $interpolate(templates.bedrijf_description, false, null, true)(pickBy(item, identity));

						return {
							label: item.object.handelsnaam,
							description,
							link: `/betrokkene/${item.object.id}/?gm=1&type=bedrijf`
						};

					},
					case: ( item ) => {

						let obj = 'zs_object' in item.object ? item.object.zs_object : item.object,
							label = `${obj.values['case.number']}: ${obj.values['case.casetype.name']}`,
							description = item.object.description || '';

						return {
							label,
							description,
							id: obj.id,
							link: `/intern/zaak/${obj.values['case.number']}`,
							type: 'case'
						};
					},
					zaak: ( item ) => transformers.case(item),
					casetype: ( item ) => {

						return {
							id: item.object.zs_object.id,
							label: item.object.zs_object.label,
							description: item.object.zs_object.values.description,
							data: merge({}, item.object.zs_object, {
								values: {
									trigger: result(item, 'object.zaaktype_node_id.trigger')
								}
							}),
							type: 'casetype'
						};
					},
					zaaktype: ( item ) => transformers.casetype(item),
					file: ( item ) => {
						return {
							link: `/intern/zaak/${item.object.case_id}/documenten/`
						};
					},
					attribute: ( item ) => {

						return {
							id: item.id,
							label: item.label,
							data: item,
							type: 'attribute'
						};

					},
					defaults: ( item ) => {

						return {
							id: item.id,
							name: item.id,
							label: item.label,
							type: item.object_type,
							description: '',
							data: item.object,
							link: `/object/${item.id}`
						};
					}
				};

			let handlers = {
				casetype: {
					request: ( query/*, type*/ ) => {
						
						if (query.length < 3) {
							return null;
						}

						return {
							url: '/objectsearch/',
							params: {
								query,
								object_type: 'zaaktype'
							}
						};
					}
				},
				case: {
					request: ( query/*, type*/ ) => {

						if (!query) {
							return null;
						}

						return {
							url: '/objectsearch/',
							params: {
								query,
								object_type: 'zaak'
							}
						};

					}
				},
				objecttypes: {
					request: ( query/*, type*/ ) => {
						let params = {};

						if (query) {
							params.query = query;
						} else {
							params.all = 1;
						}

						return {
							url: '/objectsearch/objecttypes',
							params
						};
					}
				},
				object: {
					request: ( query ) => {

						return query ?
							{
								url: '/objectsearch/objects',
								params: {
									query
								}
							}
							: null;

					}
				},
				medewerker: {
					request: ( query/*type*/ ) => {

						if (query.length < 3) {
							return null;
						}

						return {
							url: '/objectsearch/contact/medewerker',
							params: {
								query
							}
						};
					}
				},
				bag: {
					request: ( query ) => {

						if (!query) {
							return null;
						}

						return {
							url: '/objectsearch/bag',
							params: {
								query
							}
						};

					}
				},
				'bag-street': {
					request: ( query ) => {

						if (!query) {
							return null;
						}

						return {
							url: '/objectsearch/bag-street',
							params: {
								query
							}
						};

					}
				},
				attribute: {
					request: ( query ) => {

						if (!query) {
							return null;
						}

						return {
							url: '/objectsearch/attributes',
							params: {
								query
							}
						};

					}
				},
				defaults: {
					request: ( query, type ) => {

						if (query.length < 3) {
							return null;
						}

						return {
							url: '/objectsearch',
							params: {
								query,
								object_type: type
							}
						};
					},
					reduce: ( data, type ) => {

						return (data || [])
							.map(( item ) => {
								let transformedItem,
									transformer = transformers[item.object_type || type];

								transformedItem = transformers.defaults(item);

								if (transformer) {
									transformedItem = assign(transformedItem, transformer(item));
								}

								return transformedItem;
							});

					}
				}
			};

			let getHandler = ( type ) => {
				return handlers[type] || handlers.defaults;
			};

			return {
				getRequestOptions: ( preferredQuery, type ) => {

					let handler,
						opts,
						query = preferredQuery;

					propCheck.throw(
						propCheck.shape({
							query: propCheck.string.optional,
							type: propCheck.string.optional
						}),
						{ query, type }
					);

					handler = getHandler(type);

					if (!query) {
						query = '';
					}

					opts = (typeof handler.request === 'function') ?
						handler.request(query, type)
						: handlers.defaults.request(query, type);

					return opts;

				},
				reduce: ( data, type ) => {

					let handler,
						res;

					propCheck.throw(
						propCheck.shape({
							type: propCheck.string.optional,
							data: propCheck.any.optional
						}),
						{ data, type }
					);

					handler = getHandler(type);

					res = (typeof handler.reduce === 'function') ?
						handler.reduce(data, type)
						: handlers.defaults.reduce(data, type);

					return res;
				}
			};

		}])
		.name;
