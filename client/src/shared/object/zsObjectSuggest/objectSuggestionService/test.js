import angular from 'angular';
import 'angular-mocks';
import objectSuggestionServiceModule from '.';

describe('objectSuggestionService', ( ) => {

	let objectSuggestionService;

	beforeEach(angular.mock.module(objectSuggestionServiceModule));

	beforeEach(angular.mock.inject([ 'objectSuggestionService', ( ...rest ) => {

		[ objectSuggestionService ] = rest;

	}]));

	describe('when type is casetype', ( ) => {

		let type = 'casetype';

		it('should return a request options object', ( ) => {

			let opts = objectSuggestionService.getRequestOptions('foo', type);

			expect(opts).toBeDefined();

			expect(opts.url).toBeDefined();

		});

		it('should reduce a data set to [ { id, label, description } ]', ( ) => {

			let data = [
					{
						object: {
							zs_object: {
								id: 1,
								label: 'label'
							},
							zaaktype_node_id: {
								trigger: 'internextern'
							}
						}
					}
				],
				result;

			result = objectSuggestionService.reduce(data, type);

			expect(result[0]).toBeDefined();

			expect(result[0].id).toBe(data[0].object.zs_object.id);
			expect(result[0].label).toBe(data[0].object.zs_object.label);
			expect(result[0].description).toBe('');
			expect(result[0].type).toBe('casetype');


		});

	});

	describe('when type is not given', ( ) => {

		it('should return a request options object', ( ) => {

			let opts = objectSuggestionService.getRequestOptions('foo');

			expect(opts).toBeDefined();

			expect(opts.url).toBeDefined();

		});

		it('should reduce a data set to [ { id, label, description } ]', ( ) => {

			let data = [
					{
						object: { },
						id: 1,
						label: 'foo',
						object_type: 'bar'
					}
				],
				result;

			result = objectSuggestionService.reduce(data);

			expect(result[0]).toBeDefined();

			expect(result[0].id).toBe(data[0].id);
			expect(result[0].label).toBe(data[0].label);
			expect(result[0].description).toBe('');
			expect(result[0].type).toBe(data[0].object_type);


		});

	});

});
