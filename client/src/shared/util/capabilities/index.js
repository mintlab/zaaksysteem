import angular from 'angular';

export default
	angular.module('capabilities', [
	])
		.factory('capabilities', [ '$window', ( $window ) => {

			let capabilities = {
				touch: !!('ontouchstart' in $window)
			};

			return ( ) => capabilities;

		}])
		.name;
