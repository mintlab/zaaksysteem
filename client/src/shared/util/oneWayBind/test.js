import createBinding from '.';

describe('oneWayBind', ( ) => {

	let func = createBinding();

	it('should return undefined when not configured', ( ) => {

		expect(func()).toBeUndefined();

	});

	it('should return the result of the getter when configured', ( ) => {

		let result = true;

		func(( ) => result);

		expect(func()).toBe(true);
		
		result = false;

		expect(func()).toBe(false);

	});

	it('should return an unregister function when configuring', ( ) => {

		let unregister,
			result = true;

		unregister = func(( ) => result);

		expect(func()).toBe(result);

		unregister();

		expect(func()).toBeUndefined();

	});

	it('should throw when anything but a function is passed', ( ) => {

		expect(( ) => {
			func('foo');
		}).toThrow();

	});

});
