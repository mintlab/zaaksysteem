import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import zsDropdownMenu from '../../ui/zsDropdownMenu';
import zsSpotEnlighter from '../../ui/zsSpotEnlighter';
import zsSideMenuModule from './../zsSideMenu';
import oneWayBind from './../../util/oneWayBind';
import zsUiViewProgressModule from './../../ui/zsNProgress/zsUiViewProgress';
import viewTitleModule from './../../util/route/viewTitle';
import zsContextualSettingMenuModule from './zsContextualSettingMenu';
import rwdServiceModule from './../../util/rwdService';
import template from './template.html';
import './topbar.scss';

export default
	angular.module('shared.navigation.zsTopBar', [
		zsDropdownMenu,
		zsSpotEnlighter,
		zsUiViewProgressModule,
		viewTitleModule,
		zsContextualSettingMenuModule,
		zsSideMenuModule,
		angularUiRouterModule,
		rwdServiceModule
	])
	.directive('zsTopBar', [
		'HOME', '$state', '$sce', '$compile', 'viewTitle', 'rwdService',
		( HOME, $state, $sce, $compile, viewTitle, rwdService ) => {

		return {
			restrict: 'E',
			template,
			bindToController: true,
			scope: {
				user: '&',
				company: '&',
				useLocation: '&',
				development: '&',
				instanceId: '&',
				onOpen: '&',
				onClose: '&'
			},
			controller: [ '$scope', function ( scope ) {

				let ctrl = this,
					pageTitle = '';

				ctrl.isSpotEnlighterActive = oneWayBind();

				ctrl.isExpanded = ctrl.isSpotEnlighterActive;

				ctrl.getPageTitle = ( ) => pageTitle;

				ctrl.getHome = ( ) => HOME;

				ctrl.isSideMenuActive = ( ) => {
					oneWayBind();
				};

				ctrl.isHome = ( ) => {
					return ctrl.useLocation() ? (!$state.current.name || $state.current.name === 'home') : false;
				};

				ctrl.isDashboardLinkVisible = ( ) => ctrl.isHome() || !rwdService.isActive('small-and-down');

				scope.$watch(viewTitle.get, ( title ) => {

					if (title) {
						$compile(`<span>${title}</span>`)(scope, ( clonedElement ) => {
							scope.$$postDigest(( ) => {
								pageTitle = $sce.trustAsHtml(clonedElement[0].outerHTML);
							});
						});
					} else {
						pageTitle = title;
					}

				});

			}],
			controllerAs: 'vm'
		};
	}]).name;

