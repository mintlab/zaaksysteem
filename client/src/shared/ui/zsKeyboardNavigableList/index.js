import angular from 'angular';

export default
	angular.module('shared.ui.zsKeyboardNavigableList', [ ])
		.directive('zsKeyboardNavigableList', [ '$document', ( $document ) => {

			return {
				restrict: 'A',
				scope: {
					keyInputDelegate: '&',
					highlightableItems: '&',
					onKeyCommit: '&',
					onKeyHighlight: '&',
					commitOnTab: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( $scope, $element ) {

					let ctrl = this,
						highlighted,
						input;

					let highlight = ( item, event ) => {
						highlighted = item;
						ctrl.onKeyHighlight({ $item: item, $event: event });
					};

					let commit = ( event ) => {
						if (highlighted) {
							ctrl.onKeyCommit({ $item: highlighted, $event: event });
						}
						event.preventDefault();
						event.stopPropagation();
					};

					let moveUp = ( event ) => {
						let items = ctrl.highlightableItems(),
							index;

						if (!items || !items.length) {
							return;
						}

						index = items.indexOf(highlighted);

						if (index === 0 || index === -1) {
							index = items.length - 1;
						} else {
							index -= 1;
						}

						highlight(items[index], event);

					};

					let moveDown = ( event ) => {
						let items = ctrl.highlightableItems(),
							index;

						if (!items || !items.length) {
							return;
						}

						index = items.indexOf(highlighted);

						if (index === items.length - 1 || index === -1) {
							index = 0;
						} else {
							index += 1;
						}

						highlight(items[index], event);
					};

					let onKeyDown = ( event ) => {
						switch (event.keyCode) {
							case 13:
							$scope.$apply(( ) => {
								commit(event);
							});
							break;

							case 9:
							if (ctrl.commitOnTab()) {
								$scope.$apply(( ) => {
									commit(event);
								});
							}
							break;

							case 38:
							$scope.$apply(( ) => {
								moveUp(event);
							});
							break;

							case 40:
							$scope.$apply(( ) => {
								moveDown(event);
							});
							break;
						}

					};

					let enableKeyboardNavigation = function ( ) {
						input.on('keydown', onKeyDown);
					};

					let disableKeyboardNavigation = function ( ) {
						input.off('keydown', onKeyDown);
					};

					if (ctrl.keyInputDelegate()) {
						input = ctrl.keyInputDelegate().input;
					} else {
						input = $element;
					}

					let onFocus = ( ) => {
						$scope.$evalAsync(( ) => {
							enableKeyboardNavigation();
						});
					};

					let onBlur = ( ) => {
						$scope.$evalAsync(( ) => {
							disableKeyboardNavigation();
						});
					};

					input.on('focus', onFocus);

					input.on('blur', onBlur);

					$scope.$watch(ctrl.highlightableItems, ( ) => {
						highlight(undefined);
					});

					if ($document[0].activeElement === input[0]) {
						enableKeyboardNavigation();
					}

					$scope.$on('$destroy', ( ) => {

						input.off('focus', onFocus);
						input.off('blur', onBlur);

					});

				}],
				controllerAs: 'zsKeyboardNavigableList'
			};

		}])
		.name;
