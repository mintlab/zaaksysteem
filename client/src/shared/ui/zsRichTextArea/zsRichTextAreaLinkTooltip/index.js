import template from '!html-loader?minimize=false!./template.html';

export default ( editor ) => {
	let LinkTooltip = editor.constructor.import('ui/link-tooltip'),
		tooltip,
		open;

	tooltip = new LinkTooltip(editor, { template, offset: 0 });
	tooltip.isOpen = ( ) => open;

	tooltip.show = function ( ...rest ) {
		open = true;
		LinkTooltip.prototype.show.call(tooltip, ...rest);
	};

	tooltip.hide = function ( ...rest ) {
		open = false;
		LinkTooltip.prototype.hide.call(tooltip, ...rest);
	};

	return tooltip;

};
