import angular from 'angular';
import composedReducerModule from './../../api/resource/composedReducer';
import shortid from 'shortid';
import seamlessImmutable from 'seamless-immutable';
import merge from 'lodash/merge';
import includes from 'lodash/includes';
import without from 'lodash/without';
import template from './template.html';
import ZsRichTextAreaLinkTooltip from './zsRichTextAreaLinkTooltip';
import './styles.scss';

export default
	angular.module('zsRichTextArea', [
		composedReducerModule
	])
		.directive('zsRichTextArea', [ '$rootScope', '$sce', '$q', '$document', 'composedReducer', ( $rootScope, $sce, $q, $document, composedReducer ) => {

			let interactedPromise = $q.defer(),
				hasInteracted = false,
				load = ( ) => {

					return interactedPromise.promise.then(( ) => {

						hasInteracted = true;

						return $q(( resolve/*, reject*/ ) => {

							require([ 'quill' ], ( ...rest ) => {

								$rootScope.$evalAsync(( ) => resolve(rest));

							});

						});

					});

				};

			return {
				restrict: 'E',
				template,
				scope: {
					formats: '&',
					onChange: '&',
					html: '&'
				},
				bindToController: true,
				controller: [ '$scope', '$element', function ( scope, element ) {

					let ctrl = this,
						editor,
						id = shortid().replace(/^\d+/, ''),
						optionReducer,
						formats = 'bold italic strike underline header link bullet list paste fullscreen'.split(' '),
						settings = seamlessImmutable({ pastePlain: true }),
						updateScheduled,
						loaded = false,
						loading = false,
						sanitizedHtml,
						settingHtml,
						tooltip,
						tempWatcher;

					let scheduleUpdateOnBlur = ( ) => {

						if (!updateScheduled) {

							updateScheduled = true;

							$document[0].activeElement.addEventListener('blur', ( ) => {
								updateScheduled = false;
								ctrl.setEditorHtml(ctrl.html());
							});
						}

					};

					optionReducer = composedReducer({ scope }, ctrl.formats, ( ) => settings)
						.reduce( ( availableFormats = formats, currentSettings ) => {

							let opts =
								[
									{
										name: 'bold',
										type: 'toggle',
										label: 'Dikgedrukt',
										icon: 'format-bold'
									},
									{
										name: 'italic',
										type: 'toggle',
										label: 'Cursief',
										icon: 'format-italic'
									},
									{
										name: 'strike',
										type: 'toggle',
										label: 'Doorgestreept',
										icon: 'format-strikethrough'
									},
									{
										name: 'underline',
										type: 'toggle',
										label: 'Onderstreept',
										icon: 'format-underline'
									},
									{
										name: 'bullet',
										qlFormat: {
											name: 'list',
											value: 'bullet'
										},
										type: 'toggle',
										label: 'Lijst',
										icon: 'format-list-bulleted'
									},
									{
										name: 'list',
										qlFormat: {
											name: 'list',
											value: 'ordered'
										},
										type: 'toggle',
										label: 'Genummerde lijst',
										icon: 'format-list-numbers'
									},
									{
										name: 'align',
										type: 'dropdown',
										label: 'Uitlijning',
										data: {
											options: [ 'left', 'right', 'center', 'justify' ]
												.map(align => {

													let label = {
														left: 'Links',
														right: 'Rechts',
														center: 'Centreren',
														justify: 'Uitvullen'
													}[align];


													return {
														name: align,
														iconClass: `format-align-${align}`,
														label,
														click: ( ) => {

															editor.focus();

															editor.format('align', align, 'user');

														}
													};
												})
										},
										icon: 'format-align-left'
									},
									{
										name: 'header',
										type: 'dropdown',
										label: 'Grootte',
										data: {
											options: [ 0, 2, 1 ]
												.map( ( heading, index ) => {

													let label =
														[ 'Normaal', 'Kop 2', 'Kop 1' ][index];

													return {
														name: heading,
														label,
														click: ( ) => {

															// editor needs focus, else range is null

															editor.focus();
															
															editor.format('header', heading, 'user');

														}
													};
												})
										},
										icon: 'format-size'
									},
									{
										name: 'link',
										type: 'click',
										label: 'Link',
										icon: 'link',
										click: ( ) => {

											if (tooltip.isOpen()) {
												tooltip.hide();
											} else {
												editor.focus();
												tooltip.open();
											}

										}
									},
									{
										name: 'paste',
										type: 'click',
										qlFormat: null,
										label: 'Behoud opmaak bij plakken',
										icon: 'clipboard-text',
										click: ( ) => {

											ctrl.handlePasteToggle();

										},
										format: false,
										buttonClasses: {
											'ql-active': !currentSettings.pastePlain
										}
									},
									{
										name: 'fullscreen',
										type: 'click',
										qlFormat: null,
										label: settings.fullscreen ?
											'Normale weergave'
											: 'Volledig scherm',
										click: ( ) => {

											settings = settings.merge({
												fullscreen: !settings.fullscreen
											});
										},
										icon: settings.fullscreen ?
											'fullscreen-exit'
											: 'fullscreen'
									}
								];

							return opts.filter(option => !availableFormats || includes(availableFormats, option.name))
								.concat({
									name: 'help',
									qlFormat: null,
									label: 'Let op: functionaliteit met een ster wordt niet ondersteund in sjablonen',
									icon: 'help-circle'
								})
								.map(
									option => {

										let opt,
											buttonClasses,
											value,
											classes = {};

										if (option.qlFormat !== undefined) {
											if (option.qlFormat) {
												buttonClasses = {
													[`ql-${option.qlFormat.name}`]: true
												};
												value = option.qlFormat.value;
											}
										} else {
											buttonClasses = {
												[`ql-${option.name}`]: true
											};
										}

										if (!includes([ 'link', 'bullet', 'list', 'fullscreen', 'help' ], option.name)) {
											classes = merge({}, classes, { unsupported: true });
										}

										opt =
											merge(
												{ id: shortid(), buttonClasses, value, classes },
												option
											);

										return opt;

									}
								);

						});

					ctrl.getEditor = ( ) => editor;

					ctrl.getEditorHtml = ( ) => {
						return editor.container.children[0].innerHTML;
					};

					ctrl.setEditorHtml = ( html ) => {
						// html is pasted via the clipboard,
						// so we need to differentiate between user paste
						// and model updates
						settingHtml = true;
						editor.pasteHTML(html);
						settingHtml = false;
					};

					ctrl.triggerChange = ( ) => {
						ctrl.onChange({ $html: ctrl.getEditorHtml() });
					};

					ctrl.getUuid = ( ) => id;

					ctrl.getToolbarOptions = optionReducer.data;

					ctrl.handlePasteToggle = ( ) => {
						settings = settings.merge({ pastePlain: !settings.pastePlain });
					};

					ctrl.isFullscreen = ( ) => !!settings.fullscreen;

					ctrl.getSanitizedHtml = ( ) => sanitizedHtml;

					ctrl.isLoaded = ( ) => loaded;

					ctrl.isLoading = ( ) => loading;

					let loadEditor = ( ) => {

						loading = true;

						load().then(( libs ) => {

							let [ Quill ] = libs;

							let Parchment = Quill.import('parchment');

							Quill.register(
								new Parchment.Attributor.Style('align', 'text-align', { scope: Parchment.Scope.BLOCK }),
								true
							);

							editor = new Quill(`#${id}-editor`, {
								bounds: element[0],
								formats:
									without(
										ctrl.formats() ?
											ctrl.formats()
											: formats,
										'paste',
										'fullscreen'
									),
								modules: {
									toolbar: {
										container: `#${id}-toolbar`
									},
									clipboard: {
										matchers: [
											[ '*', ( node, delta ) => {

												if (settings.pastePlain && !settingHtml) {
													delta.ops = delta.ops.map(
														op => {

															delete op.attributes;

															return op;
														}
													);
												}

												// remove extra newlines from delta

												if (String(node.tagName).toLowerCase().match(/^h\d$/)) {
													delta.ops = delta.ops.filter(
														op => op.insert !== '\n'
													);
												}

												delta.ops = delta.ops.map(
													op => {

														// remove unnecessary newlines
														// https://github.com/quilljs/quill/issues/745

														if (op.insert) {
															op.insert = op.insert.replace(/\n{2,}$/, '\n');
														}

														return op;
													}
												);

												return delta;

											}]
										]
									}
								}
							});

							tooltip = new ZsRichTextAreaLinkTooltip(editor);

							if (tempWatcher) {
								tempWatcher();
							}

							scope.$watch(ctrl.html, ( html ) => {
								
								if (ctrl.getEditorHtml() !== html) {

									// prevent field from being updated when focused

									if (element[0].contains($document[0].activeElement)) {
										scheduleUpdateOnBlur();
									} else {
										ctrl.setEditorHtml(html);
									}
									
								}
							});

							editor.on('text-change', ( delta, oldDelta, source ) => {

								if (source === 'user') {
									ctrl.triggerChange();
								}

							});

							let onKeyUp = ( event ) => {

								if (event.keyCode === 27 && settings.fullscreen) {
									scope.$evalAsync( ( ) => {
										settings = settings.merge({ fullscreen: false });
									});
								}


							};

							$document.bind('keyup', onKeyUp);

						})
							.catch( ( err ) => {

								console.error(err);

							})
							.finally(( ) => {

								loading = false;

								loaded = true;

							});

					};

					let onMouseOver = ( ) => {

						element.unbind('mouseover', onMouseOver);

						if (!loaded) {
							scope.$evalAsync(( ) => {
								interactedPromise.resolve();
								loadEditor();
							});
						}

					};

					if (!hasInteracted) {

						tempWatcher = scope.$watch(( ) => ctrl.html(), val => {
							sanitizedHtml = $sce.trustAsHtml(val);
						});

						element.bind('mouseover', onMouseOver);

					} else {
						loadEditor();
					}

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
