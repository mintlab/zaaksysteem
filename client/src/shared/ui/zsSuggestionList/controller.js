import isPromise from 'is-promise';

export default function ( $scope ) {

	let ctrl = this,
		loading = false,
		suggestions = [],
		highlighted;

	let select = ( suggestion, event ) => {
		ctrl.onSelect({ $suggestion: suggestion, $event: event });
	};

	ctrl.isLoading = ( ) => loading;

	ctrl.getSuggestions = ( ) => suggestions;

	ctrl.getKeyInputDelegate = ctrl.keyInputDelegate;

	ctrl.handleKeyCommit = ( suggestion, event ) => {
		select(suggestion, event);
	};

	ctrl.handleHighlight = ( suggestion ) => {
		highlighted = suggestion;
	};

	ctrl.handleSuggestionClick = ( suggestion, event ) => {
		select(suggestion, event);
	};

	ctrl.isHighlighted = ( suggestion ) => suggestion === highlighted;

	$scope.$watch(ctrl.suggestions, ( result ) => {

		loading = false;

		if (isPromise(result)) {
			loading = true;

			result
				.then(( data ) => {
					suggestions = data;
				})
				.catch(( ) => {
					suggestions = [];
				})
				.finally( ( ) => {
					loading = false;
				});
		} else {
			suggestions = result;
		}
	});

}
