import angular from 'angular';
import oneWayBind from '../../../shared/util/oneWayBind';


export default
	angular.module('Zaaksysteem.bbv.proposalListService', [
	])
		.factory('proposalListService', ( ) => {

			return {
				areItemsGrouped: oneWayBind()
			};
			
		})
		.name;
