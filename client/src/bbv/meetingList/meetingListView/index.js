import angular from 'angular';
import template from './template.html';
import resourceModule from '../../../shared/api/resource';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import configServiceModule from '../../shared/configService';
import filterServiceModule from '../../shared/filterService';
import angularUiRouterModule from 'angular-ui-router';
import appServiceModule from './../../shared/appService';
import keyBy from 'lodash/keyBy';
import every from 'lodash/every';
import find from 'lodash/find';
import assign from 'lodash/assign';
import mapValues from 'lodash/mapValues';
import get from 'lodash/get';
import sortBy from 'lodash/sortBy';
import shortid from 'shortid';
import seamlessImmutable from 'seamless-immutable';
import includes from 'lodash/includes';

import './styles.scss';

export default
		angular.module('Zaaksysteem.bbv.meetingListView', [
			resourceModule,
			composedReducerModule,
			configServiceModule,
			filterServiceModule,
			appServiceModule,
			angularUiRouterModule
		])
		.directive('meetingListView', [ '$state', '$rootScope', '$window', 'resource', 'composedReducer', 'configService', 'filterService', 'appService', ( $state, $rootScope, $window, resource, composedReducer, configService, filterService, appService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					proposalResource: '&',
					meetingResource: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						filteredProposalReducer,
						meetingReducer,
						sortedMeetingReducer,
						itemsCollapsedState = {},
						noteProposalReducer,
						noteStorage = seamlessImmutable( JSON.parse( $window.localStorage.getItem('bbvAppNotes') ) );

					// Builds the filter suggestion index
					ctrl.proposalResource().onUpdate(( data ) => {
						filterService.addToIndex( data || [] );
					});

					let mappings = {
						date: 'vergaderingdatum',
						time: 'vergaderingtijd',
						label: 'vergaderingtitel',
						location: 'vergaderinglocatie',
						chairman: 'vergaderingvoorzitter'
					};

					sortedMeetingReducer = composedReducer({ scope: $scope }, ctrl.meetingResource(), configService.getMeetingAttributes, ( ) => $state.params.meetingType)
						.reduce(( meetings, attributes, meetingType ) => {

							let sortedAttributes,
								sortAttributeDate,
								sortAttributeTime;
							
							sortAttributeDate = find(attributes, ( n ) => {
								return n.external_name === 'vergaderingdatum';
							});

							sortAttributeTime = find(attributes, ( n ) => {
								return n.external_name === 'vergaderingtijd';
							});

							if (!sortAttributeDate) {
								return meetings;
							}

							sortedAttributes = sortBy(meetings, ( meeting ) => {
								let date = meeting.instance.attributes[sortAttributeDate.internal_name.searchable_object_id],
									time,
									match,
									hours;

									if (sortAttributeTime) {
										time = meeting.instance.attributes[sortAttributeTime.internal_name.searchable_object_id] || '00:00';
									} else {
										time = '00:00';
									}

									match = time.match(/(\d{1,2})(:|\.)?(\d{2})/);
									hours = match ? Number(match[1]) + (Number(match[3]) / 60) : 0;

								return new Date(date).getTime() + (hours * 60 * 60 * 1000);
							});

							if (meetingType === 'archief') {
								sortedAttributes = sortedAttributes.reverse();
							}

							return sortedAttributes;

						});

					filteredProposalReducer = composedReducer({ scope: $scope }, ctrl.proposalResource(), ( ) => appService.state().filters, filterService.getAttributesToIndex)
						.reduce( ( proposals, filters, attributesToIndex ) => {

							return filters.length ?
								proposals.filter(
									proposal => {
										return filters.every( filter => {
											return attributesToIndex.map( attr => String(proposal.instance.attributes[attr] || proposal.instance.casetype.instance.name || '').toLowerCase())
												.some(val => val.indexOf(filter) !== -1);
										});
									}
								)
								: proposals;
						});


					let getStorage = ( ) => {
						noteStorage = seamlessImmutable( JSON.parse( $window.localStorage.getItem('bbvAppNotes') ) );
					};

					if (!noteStorage) {
						$window.localStorage.setItem('bbvAppNotes', JSON.stringify([]));
						getStorage();
					}

					appService.on('proposal_note_save', ( ) => {
						getStorage();
					});

					noteProposalReducer = composedReducer({ scope: $scope }, filteredProposalReducer, ( ) => noteStorage)
						.reduce( ( proposals, notes ) => {

							let noteIds = notes.map( ( note ) => note.id);

							return proposals.map( ( proposal ) => {

								if ( includes( noteIds, proposal.instance.number ) ) {

									return proposal.merge( { notes: true } );
								}

								return proposal;
							});

						});

					meetingReducer = composedReducer({ scope: $scope }, sortedMeetingReducer, noteProposalReducer, ( ) => appService.state().grouped)
						.reduce(( meetings, filteredProposals, isGrouped ) => {

							let proposalsById =
								keyBy(
									filteredProposals,
									'reference'
								),
								groups;

							if (isGrouped) {
								groups =
									meetings.map( ( meeting ) => {

										let meetingObj = assign(
											{
												$id: shortid(),
												id: meeting.reference,
												casenumber: meeting.instance.number,
												children:
													meeting.instance.relations.instance.rows
														.filter(proposal => proposalsById[proposal.reference])
														.map( ( proposal, index ) => {
															return proposalsById[proposal.reference].merge({ instance: { attributes: { $index: index + 1 } } }, { deep: true });
														})
											},
											meeting,
											mapValues(mappings, ( value ) => {

												let attributeName =
													get(
														find(configService.getAttributes(), { external_name: value }),
														'internal_name.searchable_object_id'
													);

												return meeting.instance.attributes[attributeName];
													
											})
										);

										return meetingObj;

									})
									.filter( meeting => meeting.children.length);

							} else {
								groups = [
									{
										id: null,
										label: 'Alle voorstellen',
										children: filteredProposals
									}
								];
							}

							return groups;

						});

					ctrl.getMeetings = meetingReducer.data;

					ctrl.handleToggle = ( id ) => {

						let isStateExpanded = appService.state().expanded,
							allCollapsed;

						itemsCollapsedState[id] = ctrl.isExpanded(id);

						allCollapsed = every(meetingReducer.data(), ( meeting ) => !ctrl.isExpanded(meeting.id));

						if (isStateExpanded && allCollapsed) {
							appService.dispatch('toggle_expand');
						}
					};

					ctrl.isExpanded = ( id ) => {
						return itemsCollapsedState.hasOwnProperty(id) ? !itemsCollapsedState[id] : !!appService.state().expanded;
					};

					ctrl.isGrouped = ( ) => !!appService.state().grouped;

					ctrl.isLoading = ( ) => meetingReducer.state() === 'pending';

					$scope.$on(
						'$destroy',
						appService.on('toggle_expand', ( ) => {

							itemsCollapsedState = {};

						}, 'expanded')
					);

				}],
				controllerAs: 'meetingList'
			};

		}
		])
		.name;
