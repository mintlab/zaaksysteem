import angular from 'angular';
import 'angular-i18n/angular-locale_nl';
import ngAnimate from 'angular-animate';
import url from 'url';
import routing from './routing';
import resourceModule from './../shared/api/resource';
import zsResourceConfiguration from './../shared/api/resource/resourceReducer/zsResourceConfiguration';
import bbvAppModule from './shared/bbvApp';
import configServiceModule from './shared/configService';
import appServiceModule from './shared/appService';
import serviceworkerModule from './../shared/util/serviceworker';
import assign from 'lodash/assign';
import get from 'lodash/get';
import without from 'lodash/without';
import uniq from 'lodash/uniq';
import createManifest from './shared/manifest';
import sessionServiceModule from '../shared/user/sessionService';

export default
	angular.module('Zaaksysteem.bbv', [
		routing,
		resourceModule,
		ngAnimate,
		configServiceModule,
		appServiceModule,
		serviceworkerModule,
		bbvAppModule,
		sessionServiceModule
	])
	.config(['$provide', ( $provide ) => {
		
		// make sure sourcemaps work
		// from https://github.com/angular/angular.js/issues/5217

		$provide.decorator('$exceptionHandler', [ '$delegate', ( $delegate ) => {

			return ( exception, cause ) => {
				$delegate(exception, cause);
				setTimeout(( ) => {
					console.error(exception.stack);
				});
			};

		}]);

	}])
	.config([ 'appServiceProvider', ( appServiceProvider ) => {

		appServiceProvider.setDefaultState({ expanded: true, filters: [ ], grouped: true })
			.reduce('filter_add', 'filters', ( filters, filterToAdd ) => uniq(filters.concat(filterToAdd)))
			.reduce('filter_remove', 'filters', ( filters, filterToRemove ) => without(filters, filterToRemove))
			.reduce('filter_clear', 'filters', ( ) => [ ])
			.reduce('toggle_grouped', 'grouped', ( isGrouped ) => !isGrouped)
			.reduce('toggle_expand', 'expanded', ( isExpanded ) => !isExpanded)
			.reduce('disable_grouped', 'grouped', ( ) => false);
			
	}])
	.config([ '$httpProvider', ( $httpProvider ) => {
				
		$httpProvider.interceptors.push([ '$location', '$q', '$injector', '$rootScope', ( $location, $q, $injector, $rootScope ) => {

			return {
				request: ( config ) => {

					let configService = $injector.get('configService'),
						appConfig,
						credentials,
						fullUrl,
						parts,
						reqConfig,
						loginReqConfig,
						sessionService = $injector.get('sessionService'),
						userResource = sessionService.createResource($rootScope);


					// Disable proxy for /bbvapp calls, when using a post, or when logged in.
					if (config.url.indexOf('bbvapp') !== -1 || config.method === 'POST' || userResource.state() === 'resolved') {
						return config;
					}

					if ( config.url.indexOf('session/current') !== -1 ) {

						loginReqConfig =

							assign(
								{ },
								config,
								{
									headers: assign({}, config.headers, {
										'API-Interface-Id': configService.getInterfaceId(),
										'Disable-Digest-Authentication': 1
									})
								}
							);

						return loginReqConfig;
					}

					appConfig = configService.getConfig();

					credentials = {
						user: get(appConfig, 'medewerker.username'),
						password: get(appConfig, 'api_key')
					};

					parts = url.parse(config.url, true);

					fullUrl = url.format({ protocol: 'https', host: $location.$$host, pathname: `/clientutil/proxy${parts.pathname}`, query: assign({}, config.params, parts.query) });

					reqConfig =
						assign(
							{ },
							config,
							{
								url: fullUrl,
								params: {},
								headers: assign({}, config.headers, {
									'API-Interface-Id': configService.getInterfaceId(),
									'ZS-User-Name': credentials.user,
									'ZS-User-Password': credentials.password
								})
							}
						);

					return reqConfig;

				}
			};

		}]);

	}])
	.config([ 'resourceProvider', ( resourceProvider ) => {

		zsResourceConfiguration(resourceProvider.configure);

	}])
	.run([ '$document', 'configService', ( $document, configService ) => {

		let styleEl;

		configService.getResource().onUpdate( data => {

			createManifest( get(data, 'instance.interface_config'), $document );

			let color = get(data, 'instance.interface_config.accent_color', '#FFF'),
				rules = [
					`#nprogress .bar {
					  background: ${color};
					  height: 100%;
					}`,
					`#nprogress .peg {
					  box-shadow: 0 0 20px ${color}, 0 0 10px #FFF;
					}`,
					`#nprogress .spinner-icon {
					  border-top-color: ${color};
					  border-left-color: ${color};
					}`,
					`.proposal-item-table tr:hover .proposal__number {
					  background-color: ${color} !important;
					}`
				];

			if (styleEl) {
				styleEl.remove();
			}

			styleEl = $document[0].createElement('style');

			$document[0].head.appendChild(styleEl);

			rules.forEach(( rule, index ) => {
				styleEl.sheet.insertRule(rule, index);
			});

		});

	}])
	.run([ '$window', 'serviceWorker', '$animate', ( $window, serviceWorker, $animate ) => {

		let enable = PROD, //eslint-disable-line
			worker = serviceWorker();

		$animate.enabled(true);

		if (worker.isSupported()) {
			worker.isEnabled()
				.then( ( enabled ) => {
					if (enabled !== enable) {
						return (enable ?
							worker.enable()
							: worker.disable()
								.then(( ) => {
									if (enabled) {
										$window.location.reload();
									}
								})
						);
					}
				});
		}

	}])
		.name;
