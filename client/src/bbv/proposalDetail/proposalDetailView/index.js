import angular from 'angular';
import template from './template.html';
import composedReducerModule from '../../../shared/api/resource/composedReducer';
import angularUiRouterModule from 'angular-ui-router';
import configServiceModule from '../../shared/configService';
import proposalNoteBarModule from './proposalNoteBar';
import proposalVoteBarModule from './proposalVoteBar';
import actionsModule from './actions';
import shortid from 'shortid';
import capitalize from 'lodash/capitalize';
import get from 'lodash/get';
import includes from 'lodash/includes';
import rwdServiceModule from '../../../shared/util/rwdService';
import auxiliaryRouteModule from '../../../shared/util/route/auxiliaryRoute';
import find from 'lodash/find';
import isArray from 'lodash/isArray';
import sessionServiceModule from '../../../shared/user/sessionService';
import uniqBy from 'lodash/uniqBy';
import first from 'lodash/head';
import snackbarServiceModule from '../../../shared/ui/zsSnackbar/snackbarService';
import appServiceModule from '../../shared/appService';

import './styles.scss';

export default
		angular.module('Zaaksysteem.bbv.proposalDetailView', [
			composedReducerModule,
			configServiceModule,
			angularUiRouterModule,
			proposalNoteBarModule,
			rwdServiceModule,
			auxiliaryRouteModule,
			sessionServiceModule,
			proposalVoteBarModule,
			actionsModule,
			snackbarServiceModule,
			appServiceModule
		])
		.directive('proposalDetailView', [ '$sce', '$window', '$document', '$http', '$compile', '$state', 'dateFilter', 'composedReducer', 'configService', 'auxiliaryRouteService', 'rwdService', 'sessionService', 'snackbarService', 'appService', ( $sce, $window, $document, $http, $compile, $state, dateFilter, composedReducer, configService, auxiliaryRouteService, rwdService, sessionService, snackbarService, appService ) => {

			return {
				restrict: 'E',
				template,
				scope: {
					proposal: '&',
					documents: '&',
					casetype: '&',
					onSaveNote: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						fieldReducer,
						styleReducer,
						titleReducer,
						userResource = sessionService.createResource($scope),
						casetypeResource = ctrl.casetype(),
						voteAttributesReducer,
						voteBarConfigReducer,
						voteBarDataReducer,
						proposalResultsReducer;

					voteAttributesReducer = composedReducer({ scope: $scope }, casetypeResource, ( ) => ctrl.proposal().data().instance.casetype.reference, configService.getProposalVoteAcceptAttributes, configService.getProposalVoteCommentAttributes )
						.reduce( ( caseType, currentCasetypeReference, acceptAttributes, commentAttributes ) => {

							if (!caseType) {
								return [];
							}

							let allConfigAttributes = acceptAttributes.concat(commentAttributes),
								allCaseAttributes = uniqBy(caseType.instance.phases.flatMap(phase => phase.fields), 'magic_string');

							return allConfigAttributes.map(
								attribute => {
									return attribute.merge({
										permissions: get(find(allCaseAttributes, { magic_string: attribute.object.column_name.replace('attribute.', '') }), 'permissions')
									});
								}
							);

						});

					voteBarConfigReducer = composedReducer({ scope: $scope }, voteAttributesReducer, userResource)
						.reduce( ( attributes, user ) => {

							let orgUnits = get(user, 'instance.logged_in_user.legacy.parent_ou_ids').concat( get(user, 'instance.logged_in_user.legacy.ou_id')),
								roles = get(user, 'instance.logged_in_user.legacy.role_ids');

							let findAttr = ( type ) => {
								return find(attributes, ( attribute ) => {

									if (attribute.object.value_type === type && get(attribute, 'permissions') ) {
										return includes( orgUnits, first(attribute.permissions).group.instance.id)
											&& includes(roles, first(attribute.permissions).role.instance.id);
									}

									return false;

								});
							};

							return {
								choice: findAttr('select') || findAttr('option') || null,
								comment: findAttr('textarea')
							};

						});

					voteBarDataReducer = composedReducer({ scope: $scope }, voteBarConfigReducer, ctrl.proposal() )
						.reduce( ( attributes, proposal ) => {

							return attributes.choice !== null ? {
								choice: get(proposal, `instance.attributes.${attributes.choice.object.column_name.replace('attribute.', '')}`) || '',
								comment: get(proposal, `instance.attributes.${attributes.comment.object.column_name.replace('attribute.', '')}` || '')
							} : null;
						});

					fieldReducer = composedReducer({ scope: $scope }, ctrl.proposal(), configService.getProposalAttributes)
						.reduce( ( proposal, fieldConfig ) => {

							let caseData = ctrl.proposal(),
								values =
								fieldConfig.filter( ( field ) => field.external_name.indexOf('voorstelbijlage') === -1)
								.asMutable().map( ( field ) => {

									let label = field.internal_name.searchable_object_label_public || field.internal_name.searchable_object_label,
										value = caseData.data().instance.attributes[field.internal_name.searchable_object_id] || '',
										tpl = String(value) || '-';

									if ( isArray(value) ) {

										if (value.length > 1) {
											tpl = '<ul>';

											value.map( ( el ) => {
												tpl += `<li>${el}</li>`;
											});

											tpl += '</ul>';
										} else {
											tpl = `${value.join()}`;
										}
									}

									if (label === 'zaaknummer' ) {
										label = capitalize(label);
										value = caseData.data().instance.number;
										tpl = String(value);
									}

									if (field.external_name.includes('datum')) {
										tpl = dateFilter( value, 'dd MMMM yyyy');
									}

									if (field.internal_name.searchable_object_id === 'resultaat') {
										tpl = String( caseData.data().instance.result || '-');
									}

									if (field.internal_name.searchable_object_id === 'zaaktype') {
										tpl = String(caseData.data().instance.casetype.instance.name || '-');
									}

									return {
										id: shortid(),
										label,
										value,
										template: $sce.trustAsHtml(tpl)
									};
								});

							return values;

						});

					styleReducer = composedReducer( { scope: $scope }, configService.getConfig)
						.reduce( config => {

							return {
								'background-color': get(config, 'header_bgcolor', '#FFF')
							};

						});

					titleReducer = composedReducer({ scope: $scope }, ctrl.proposal(), configService.getProposalAttributes)
						.reduce( ( proposal, columns ) => {

							let descriptionCol = find(columns, ( col ) => col.external_name === 'voorstelonderwerp' ),
								value = proposal.instance.attributes[descriptionCol.internal_name.searchable_object_id] || proposal.instance.casetype.instance.name || '-';

							return `${proposal.instance.number}: ${value}`;
						});

					proposalResultsReducer = composedReducer({ scope: $scope }, ctrl.proposal(), configService.getProposalVoteAcceptAttributes, configService.getProposalVoteCommentAttributes)
						.reduce( ( proposal, acceptAttributes, commentAttributes ) => {

							return acceptAttributes.concat(commentAttributes).asMutable().map( ( attribute ) => {

								let attributeName = attribute.object.column_name.replace('attribute.', ''),
									value = proposal.instance.attributes[attributeName] || '',
									tpl = String(value) || '-';

									if ( isArray(value) ) {

										if (value.length > 1) {
											tpl = '<ul>';

											value.map( ( el ) => {
												tpl += `<li>${el}</li>`;
											});

											tpl += '</ul>';
										} else {
											tpl = `${value.join()}`;
										}
									}
								return {
									id: shortid(),
									label: attribute.public_label || attribute.label,
									value,
									template: $sce.trustAsHtml(tpl)
								};

							});

						});

					ctrl.proposalResults = proposalResultsReducer.data;

					ctrl.voteBarData = voteBarDataReducer.data;

					ctrl.voteBarConfig = voteBarConfigReducer.data;

					ctrl.getFields = fieldReducer.data;

					ctrl.getAttachments = ctrl.documents;

					ctrl.getStyle = styleReducer.data;

					ctrl.getTitle = titleReducer.data;

					ctrl.proposalId = ctrl.proposal().data().instance.number;

					ctrl.proposalRef = ctrl.proposal().data().reference;

					ctrl.isWriteModeEnabled = ( ) => configService.getConfig().access === 'rw' && userResource.state() === 'resolved';

					ctrl.isVotingAllowed = ( ) => configService.getConfig().access === 'rw' && userResource.state() === 'resolved' && voteBarConfigReducer.data();

					ctrl.getFileUrl = ( documentId ) => `/api/v1/case/${ctrl.proposal().data().reference}/document/${documentId}/download`;

					ctrl.handleFileClick = ( file, event ) => {

						event.preventDefault();

						snackbarService.wait(
							'Het bestand wordt gedownload',
							{
								promise:
									$http({
										url: ctrl.getFileUrl(file.reference),
										responseType: 'blob'
									})
									.then( ( response ) => {

										let URL = 'URL' in $window ? $window.URL : $window.webkitURL,
											blobUrl = URL.createObjectURL(response.data, { type: 'text/bin' }),
											anchor = angular.element('<a></a>');

										anchor.attr('href', blobUrl);
										anchor.attr('download', file.filename);

										$document.find('body').append(anchor);

										anchor[0].click();

										anchor.remove();

										URL.revokeObjectURL(blobUrl);

									}),
								then: ( ) => '',
								catch: ( ) => 'Het bestand kon niet worden gedownload. Neem contact op met uw beheerder voor meer informatie.'
							}
						);
					};

					ctrl.handleVote = ( proposalId, voteData ) => {

						return ctrl.proposal().mutate('VOTE_FOR_PROPOSAL', {
							proposalId,
							voteData
						})
						.asPromise()
						.then( ( ) => {
							$http({
								method: 'GET',
								url: '/clientutil/proxy/flush'
							});
						});

					};

					ctrl.handleSaveNote = ( proposalId ) => {
						appService.dispatch('proposal_note_save', proposalId);
					};

					ctrl.handleFileClick = ( file, event ) => {

						event.preventDefault();

						snackbarService.wait(
							'Het bestand wordt gedownload',
							{
								promise:
									$http({
										url: ctrl.getFileUrl(file.reference),
										responseType: 'blob'
									})
										.then( ( response ) => {

											let URL = 'URL' in $window ? $window.URL : $window.webkitURL,
												blobUrl = URL.createObjectURL(response.data, { type: 'text/bin' }),
												anchor = angular.element('<a></a>');

											anchor.attr('href', blobUrl);
											anchor.attr('download', file.instance.name);

											$document.find('body').append(anchor);

											anchor[0].click();

											anchor.remove();

											URL.revokeObjectURL(blobUrl);

										}),
								then: ( ) => '',
								catch: ( ) => 'Het bestand kon niet worden gedownload. Neem contact op met uw beheerder voor meer informatie.'
							}
						);
						

					};

					ctrl.getViewSize = ( ) => {
						if ( includes( rwdService.getActiveViews(), 'small-and-down') ) {
							return 'small';
						}
						return 'wide';
					};

					ctrl.goBack = ( ) => {
						$state.go('^');
					};

				}],
				controllerAs: 'proposalDetailView'

			};
		}
		])
		.name;
