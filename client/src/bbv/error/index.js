import angular from 'angular';
import template from './index.html';

export default {
	moduleName:
		angular.module('Zaaksysteem.bbv.error', [
		])
		.name,
		
	config: [
		{
			route: {
				template,
				url: '/404',
				title: 'Foutmelding'
			},
			state: 'error'
		}
	]
};
