import angular from 'angular';
import configServiceModule from '../configService';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import fuzzy from 'fuzzy';
import wordNgrams from 'word-ngrams';
import seamlessImmutable from 'seamless-immutable';
import uniq from 'lodash/uniq';
import flattenDeep from 'lodash/flattenDeep';
import identity from 'lodash/identity';
import take from 'lodash/take';
import filter from 'lodash/filter';
import isArray from 'lodash/isArray';
import get from 'lodash/get';

export default
	angular.module('Zaaksysteem.bbv.filterService', [
		configServiceModule,
		composedReducerModule
	])
		.factory('filterService', [ '$rootScope', 'composedReducer', 'configService', ( $rootScope, composedReducer, configService ) => {

			const NGRAM_RANGE = [ 1, 2, 3 ];

			let index = seamlessImmutable([]),
				results,
				matches,
				indexAttrReducer;

			let getAttributesToIndex = ( ) => {
				return filter(
					configService.getProposalAttributes(),
					attr => attr.external_name !== 'voorstelzaaknummer' && attr.external_name !== 'voorsteldocumenten'
				)
					.map( attr => get(attr, 'internal_name.searchable_object_id'));
			};

			indexAttrReducer = composedReducer( { scope: $rootScope }, ( ) => configService.getProposalAttributes())
				.reduce( ( attributes ) => {

					if (attributes) {
						return attributes.filter(attr => attr.external_name !== 'voorstelzaaknummer' && attr.external_name !== 'voorsteldocumenten' )
							.map( attr => get(attr, 'internal_name.searchable_object_id'));
					}
				});

			return {
				getAttributesToIndex: indexAttrReducer.data,
				addToIndex: ( elements ) => {

					let attrsToIndex = getAttributesToIndex();

					let getNGramsFromElement = ( el ) => {

						return attrsToIndex.map(attrName => el.instance.attributes[attrName] || el.instance.casetype.instance.name)
							.filter(identity)
							.map(elValue => {

								let stringValue = isArray(elValue) ? elValue.join() : elValue;

								return NGRAM_RANGE.map(nGramLength => {
									return wordNgrams.listAllNGrams(
										wordNgrams.buildNGrams( stringValue, nGramLength, { caseSensitive: false, includePunctuation: false })
									);
								});
							});
					};

					index = seamlessImmutable(
						uniq(flattenDeep(elements.map(getNGramsFromElement)))
					);
					
				},
				getFromIndex: ( query ) => {
					results = take(fuzzy.filter(query, index.asMutable()), 15);
					matches = results.map( (el) => el.string );
					return matches;
					
				}
			};

		}])
		.name;
