import angular from 'angular';
import 'angular-mocks';
import filterServiceModule from '.';
import configData from '../mock/config.json';
import proposalData from '../mock/data.json';

import cacherModule from '../../../shared/api/cacher';

describe('filterService', ( ) => {

	let filterService,
		$httpBackend;


	beforeEach(angular.mock.module(filterServiceModule, cacherModule));

	beforeEach(angular.mock.inject( [ '$httpBackend', 'apiCacher', ( ...rest ) => {

		let apiCacher;

		[ $httpBackend, apiCacher ] = rest;

		apiCacher.clear();

		$httpBackend.expectGET(/\/api\/v1\/app\/bbvapp/)
			.respond([ configData ]);

	}]));

	beforeEach(angular.mock.inject([ 'filterService', ( ...rest ) => {

		[ filterService ] = rest;

		$httpBackend.flush();

		filterService.addToIndex( proposalData );

	}]));


	it('should have an addToIndex method', ( ) => {

		expect(typeof filterService.addToIndex).toBe('function');

	});

	describe('when given a query', ( ) => {

		it('should return results for x', ( ) => {

			expect(filterService.getFromIndex('diemerbos') ).toContain('diemerbos');

		});

		it('should not return results for y', ( ) => {

			expect(filterService.getFromIndex('diemerbos') ).not.toContain('boer');

		});

		it('should return results as { x, y, z }', ( ) => {

			expect( typeof filterService.getFromIndex('diemerbos') ).toBe('object');

		});

		it('should wipe the index when a new one is given', ( ) => {

			let resultBefore = filterService.getFromIndex('diemerbos'),
				resultAfter;

			filterService.addToIndex([]);
			
			resultAfter = filterService.getFromIndex('diemerbos');

			expect( resultBefore ).not.toEqual( resultAfter );

		});

	});

});
