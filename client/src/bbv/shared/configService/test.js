import angular from 'angular';
import 'angular-mocks';
import configServiceModule from '.';
import cacherModule from '../../../shared/api/cacher';
import data from '../mock/config.json';
import filter from 'lodash/filter';
import map from 'lodash/map';

describe('configService', ( ) => {

	let $httpBackend,
		configService;

	beforeEach(angular.mock.module(configServiceModule, cacherModule));

	beforeEach(angular.mock.inject( [ '$httpBackend', 'apiCacher', ( ...rest ) => {

		let apiCacher;

		[ $httpBackend, apiCacher ] = rest;

		apiCacher.clear();

		$httpBackend.expectGET(/.*/)
			.respond([ data ]);

	}]));

	beforeEach(angular.mock.inject([ 'configService', ( ...rest ) => {

		[ configService ] = rest;

		$httpBackend.flush();

	}]));

	it('should have a config resource', ( ) => {

		expect( configService.getResource() ).toBeDefined();

	});

	describe('when receiving data from api', ( ) => {

		let resource;

		beforeEach( ( ) => {

			resource = configService.getResource();

		});

		it('should return a resource object with an interface_config key', ( ) => {

			expect(resource.data().instance.interface_config).toBeDefined();

		});

		it('should return a config object', ( ) => {

			expect( configService.getConfig() ).toBeDefined();

		});

		it('should return the attributes defined in instance.interface_config.attribute_mapping', ( ) => {

			expect(configService.getAttributes().length).toBe(data.instance.interface_config.attribute_mapping.length);

		});


		it('should return the attributes defined in instance.interface_config.attribute_mapping that prefix with "voorstel"', ( ) => {

			let proposalAttributes = filter(data.instance.interface_config.attribute_mapping, ( n ) => {
				return n.external_name.indexOf('voorstel') === 0;
			});

			expect( map(configService.getProposalAttributes(), 'external_name') ).toEqual( map(proposalAttributes, 'external_name') );

		});

		it('should return the attributes defined in instance.interface_config.attribute_mapping that prefix with "vergadering"', ( ) => {

			let proposalAttributes = filter(data.instance.interface_config.attribute_mapping, ( n ) => {
				return n.external_name.indexOf('vergadering') === 0;
			});

			expect( map(configService.getMeetingAttributes(), 'external_name') ).toEqual( map(proposalAttributes, 'external_name') );

		});

		it('should contain an interface ID', ( ) => {
			
			expect( configService.getInterfaceId() ).toBeDefined();

		});

	});

});
