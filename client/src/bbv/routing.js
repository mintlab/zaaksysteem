import angular from 'angular';
import uiRouter from 'angular-ui-router';
import meetingList from './meetingList';
import proposalDetail from './proposalDetail';
import err from './error';
import flatten from 'lodash/flatten';
import configServiceModule from './shared/configService';
import snackbarServiceModule from './../shared/ui/zsSnackbar/snackbarService';
import merge from 'lodash/merge';

export default angular.module('Zaaksysteem.bbv.routing', [
	uiRouter,
	configServiceModule,
	meetingList.moduleName,
	proposalDetail.moduleName,
	err.moduleName,
	snackbarServiceModule
])
	.config([ '$stateProvider', '$urlRouterProvider', '$locationProvider', ( $stateProvider, $urlRouterProvider, $locationProvider ) => {

		$locationProvider.html5Mode(true);
		$urlRouterProvider.otherwise(( $injector ) => {

			let $state = $injector.get('$state');

			$state.go('meetingList', { meetingType: 'open' }, { location: false });

		});

		flatten(
			[meetingList, proposalDetail, err]
				.map(routeConfig => routeConfig.config)
			)
			.forEach(route => {

				let mergedState =
					merge(route.route, {
						resolve: {
							config: [ '$q', 'configService', 'snackbarService', ( $q, configService, snackbarService ) => {

								return configService.getResource().asPromise()
									.catch( ( error ) => {

										snackbarService.error('De configuratie van de BBV app kon niet geladen worden. Neem contact op met uw beheerder voor meer informatie.');

										return $q.reject(error);
									});
							}]
						}
					});

				$stateProvider.state(route.state, mergedState);

			});


	}])
	.run([ '$rootScope', ( $rootScope ) => {

		$rootScope.$on('$stateChangeError', ( ...rest ) => {
			console.log(...rest);
		});

	}])
	.name;
