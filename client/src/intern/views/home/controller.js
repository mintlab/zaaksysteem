import angular from 'angular';
import 'angular-gridster';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';
import zsDashboardWidgetModule from './zsDashboard/zsDashboardWidget';
import zsDashboardWidgetCreateModule from './zsDashboard/zsDashboardWidget/types/zsDashboardWidgetCreate';
import zsDashboardSettingsModule from './zsDashboard/zsDashboardSettings';
import zsDashboardGridsterModule from './zsDashboard/zsDashboardGridster';
import composedReducerModule from './../../../shared/api/resource/composedReducer';
import rwdServiceModule from './../../../shared/util/rwdService';
import actionsModule from './actions';
import find from 'lodash/find';
import assign from 'lodash/assign';
import pick from 'lodash/pick';
import map from 'lodash/map';
import invoke from 'lodash/invokeMap';
import get from 'lodash/get';
import once from 'lodash/once';
import shortid from 'shortid';
import './dashboard.scss';

export default
	angular.module('DashboardController', [
		snackbarServiceModule,
		actionsModule,
		zsDashboardWidgetModule,
		zsDashboardWidgetCreateModule,
		zsDashboardSettingsModule,
		zsDashboardGridsterModule,
		rwdServiceModule,
		composedReducerModule,
		'gridster'
	])
	.controller('DashboardController',
		[ '$window', '$timeout', '$element', '$scope', '$http', '$stateParams', 'widgetResource', 'settingsResource', 'snackbarService', 'rwdService', 'composedReducer',
		function ( $window, $timeout, $element, $scope, $http, $stateParams, widgetResource, settingsResource, snackbarService, rwdService, composedReducer ) {

			const COLUMNS = 10;

			const COMPACT_AT = 600;

			let ctrl = this,
				defaultWidgetData = {
					'size_x': 5,
					'size_y': 10,
					row: null,
					column: null,
					widget: 'create',
					data: { 'json_data': 'valid' }
				},
				widgetReducer;

				widgetResource.reduce( ( requestOptions, data ) => {
					return map(data || [], ( widget, index ) => {
						return assign({ id: widget.reference || index }, widget.instance);
					});
				});

				widgetReducer = composedReducer( { scope: $scope }, widgetResource, ( ) => rwdService.getActiveViews())
					.reduce( ( widgets/*, views*/ ) => {

						let isMobile = rwdService.isActive('small-and-down'),
							width = $window.innerWidth,
							compactTreshold = isMobile ?
								COLUMNS
								: Math.ceil(COMPACT_AT / width * COLUMNS);

						return widgets.map(
							widget => {

								let compact = widget.size_x <= compactTreshold;

								// don't create new object or the widget library will
								// think it's a new one and animate it
								return assign(widget, { compact });
							}
						);

					});

			ctrl.isDisabled = ( ) => get(find( settingsResource.data(), { 'reference' : 'disable_dashboard_customization' }), 'instance.value') === 'on';

			ctrl.onItemAdd = [];

			ctrl.isEmpty = ( ) => {
				return widgetResource.state() === 'resolved' && widgetResource.data().length === 0;
			};

			ctrl.createWidget = ( data ) => {
				widgetResource.mutate('create_widget', data);

				$timeout(( ) => {

					$element[0].scrollIntoView(false);

				}, 0, false);
			};
			
			ctrl.deleteWidget = ( uuid ) => {

				widgetResource.mutate('delete_widget', { uuid });
			};

			ctrl.updateWidgets = ( ) => {

				let updates =
					widgetResource.data()
						.map( ( item ) => {
							return assign({ uuid: item.id }, pick(item, 'row', 'column', 'size_x', 'size_y', 'data'));
						});

				widgetResource.mutate('bulk_update', { updates });
			};

			ctrl.getWidgets = widgetReducer.data;

			ctrl.handleWidgetDataChange = ( id, data ) => {
				
				let item = find(widgetResource.data(), { id });

				widgetResource.mutate('update_widget',
					assign({ uuid: item.id }, pick(item, 'row', 'column', 'size_x', 'size_y', 'data'), { data })
				);
				
			};

			ctrl.handleWidgetCreate = ( widgetType ) => {

				let limit = widgetResource.limit() || 10;

				if (widgetResource.data().length >= limit) {
					snackbarService.error(`U heeft al het maximum van ${widgetResource.limit()} bereikt. Verwijder een widget en probeer het dan opnieuw.`);
					return;
				}

				let data =
					assign(
						{
							size_x: defaultWidgetData.size_x,
							size_y: defaultWidgetData.size_y,
							data: {},
							widget: widgetType
						}
					);

				invoke(ctrl.onItemAdd, 'call', null, data);

				ctrl.createWidget(data);

			};

			ctrl.remove = ( widget ) => {
				ctrl.deleteWidget(widget.id);
			};

			ctrl.resetWidgets = ( ) => {
				return snackbarService.wait(
					'Het dashboard wordt terug gezet naar de standaardinstelling', {
						promise: $http({
							url: '/api/v1/dashboard/widget/set_default',
							method: 'POST'
						})
						.then( ( ) => {

							return widgetResource.reload();

						}),
						then: ( ) => 'Het dashboard is terug gezet naar de standaardinstelling',
						catch: ( ) => 'Het is niet gelukt om het dashboard terug te zetten naar de standaardinstelling'
					}
				);
			};

			ctrl.customItemMap = {
				sizeX: 'widget.size_x',
				sizeY: 'widget.size_y',
				row: 'widget.row',
				col: 'widget.column',
				minSizeX: 'widget.minSizeX',
				maxSizeX: 'widget.maxSizeX',
				minSizeY: 'widget.minSizeY',
				maxSizeY: 'widget.maxSizeY'
			};

			ctrl.gridsterOpts = {
				columns: COLUMNS,
				margins: [ 20, 20 ],
				swapping: false,
				rowHeight: 60,
				mobileBreakPoint: 768,
				mobileModeEnabled: rwdService.isActive('small-and-down'),
				resizable: {
					enabled: !ctrl.isDisabled(),
					handles: ['se', 'sw', 'ne', 'nw' ],
					stop: ctrl.updateWidgets
				},
				draggable: {
					handle: '.widget-header',
					enabled: !ctrl.isDisabled(),
					stop: ctrl.updateWidgets
				}
			};

			$scope.$on('$destroy', ( ) => {
				widgetResource.destroy();
				settingsResource.destroy();
			});

			$scope.$watch(( ) => rwdService.isActive('medium-small-and-down'), ( isMobile ) => {

				ctrl.gridsterOpts = assign({}, ctrl.gridsterOpts, { mobileModeEnabled: isMobile });

			});

			$scope.$on('gridster-resized', once(( ) => {
				// because angular-gridster uses a timeout, sometimes
				// the colWidth is calculated before the scrollbars appear
				// so we have to trigger an update by invalidating the options
				// object
				assign(ctrl.gridsterOpts, { zsUpdate: shortid() });
			}));

		}])
		.name;
