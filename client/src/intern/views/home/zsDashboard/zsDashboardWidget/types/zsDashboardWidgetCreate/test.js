import zsDashboardWidgetCreate from './index.js';
import angular from 'angular';
import 'angular-mocks';

describe('zsDashboardWidgetCreate', ( ) => {

	let $rootScope,
		$compile,
		scope,
		dashboardWidgetCreateEl,
		ctrl;


	beforeEach(angular.mock.module(zsDashboardWidgetCreate));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', ( ...rest ) => {

		[ $rootScope, $compile ] = rest;

		dashboardWidgetCreateEl = angular.element('<zs-dashboard-widget-create on-widget-create="handleCreate($widgetType)"/>');
		
		scope = $rootScope.$new();

		scope.handleCreate = jasmine.createSpy('create');

		$compile(dashboardWidgetCreateEl)(scope);

		ctrl = dashboardWidgetCreateEl.controller('zsDashboardWidgetCreate');

	}]));
	
	it('should have a controller', ( ) => {

		expect(ctrl).toBeDefined();

	});

	it('should return the widget types', ( ) => {

		expect(ctrl.getWidgetTypes().length).toBeGreaterThan(0);

	});

	it('should trigger the function', ( ) => {

		let option = ctrl.getWidgetTypes()[0];

		ctrl.selectType(option);

		expect(scope.handleCreate).toHaveBeenCalled();
		expect(scope.handleCreate).toHaveBeenCalledWith(option.value);

	});
});
