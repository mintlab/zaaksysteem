import angular from 'angular';
import 'angular-mocks';
import zsDashboardWidgetSearchResult from '.';
import zsResourceConfiguration from './../../../../../../../../shared/api/resource/resourceReducer/zsResourceConfiguration';
import resourceModule from './../../../../../../../../shared/api/resource';
import uniq from 'lodash/uniq';
import map from 'lodash/map';
import shortid from 'shortid';

describe('zsDashboardWidgetSearchResult', ( ) => {

	let $rootScope,
		$compile,
		$httpBackend,
		apiCacher,
		mutationService;

	let getSearch = ( id ) => {

		return {
			actions: [],
			id,
			label: 'Foo',
			type: 'saved_search',
			values: {
				public: false,
				query: JSON.stringify(
					{
						values: {},
						predefined: false,
						columns: [
							{
								id: 'case.status',
								resolve: 'values[\'case.status\']',
								locked: true,
								sort: true,
								template: '<i class="mdi icon-status icon-status-<[item.values[\'case.archival_state\'] === \'overdragen\' ? \'overdragen\' : item.values[\'case.destructable\'] && item.values[\'case.status\'] === \'resolved\' && item.values[\'case.archival_state\'] === \'vernietigen\' ? \'deleted\' : item.values[\'case.status\']]>\"></i>'
							},
							{
								id: 'case.number',
								resolve: 'values[\'case.number\']',
								label: '#',
								locked: true,
								sort: {
									type: 'numeric'
								},
								template: '<a href=\'/zaak/<[item.values["case.number"]]>\' target="_blank"><[item.values[\'case.number\']]></a>'
							}
						],
						public: false,
						options: {
							sort: {
								by: 'case.number',
								order: 'desc'
							},
							showLocation: false
						},
						objectType: {
							object_type: 'case',
							label: 'Zaak',
							id: 1
						},
						label: 'Foo',
						zql: 'SELECT case.status, case.number, case.progress_status, case.casetype.name, case.subject, case.requestor.name, case.days_left, case.num_unaccepted_updates, case.num_unaccepted_files, case.number, case.archival_state, case.destructable FROM case WHERE (case.status IN (\"open\",\"new\")) AND (case.casetype.id = 7) NUMERIC ORDER BY case.number DESC'
					}
				),
				title: 'Foo'
			}
		};
	};

	let getUser = ( ) => {

		return {
			logged_in_user: {
				betrokkene_identifier: 'betrokkene-medewerker-1',
				id: 1
			}
		};

	};

	let getConfig = ( ) => {

		return {
			actions: [
				{
					id: 'actions',
					data: {
						children: [
							{ id: 'allocate' }
						]
					}
				}
			],
			columns: {
				always: [ 'case.number' ],
				default: [ 'case.number', 'case.status', 'actions' ],
				extra: [ 'case.destruction_blocked' ],
				templates: {
					actions: {
						data: {
							columns: []
						},
						sort: false,
						label: '',
						template: '<zs-dropdown-menu options=\'item.itemActions\' button-icon=\'dots-vertical\' is-fixed="true" button-style=\'{ btn: true, "btn-small": true, "btn-secondary": true, "btn-flat": true, "btn-open": zsDropdownMenu.isMenuOpen() }\'></zs-dropdown-menu>'
					},
					'case.number': {
						label: '#',
						sort: {
							type: 'numeric'
						},
						resolve: 'values[\'case.status\']'
					},
					'case.status': {
						sort: true
					}
				}
			},
			resolve: 'id',
			sort: {
				by: 'case.number',
				order: 'desc'
			},
			style: {
				classes: {
					'crud-table-item-row-restricted-search': 'actions.length===1&&actions[0]==="search"'
				}
			}
		};
	};

	beforeEach(angular.mock.module(resourceModule, [ 'resourceProvider', ( resourceProvider ) => {

		zsResourceConfiguration(resourceProvider.configure);

	}]));

	beforeEach(angular.mock.module(zsDashboardWidgetSearchResult));

	beforeEach(angular.mock.inject([ '$rootScope', '$compile', '$httpBackend', 'apiCacher', 'mutationService', ( ...rest ) => {

		[ $rootScope, $compile, $httpBackend, apiCacher, mutationService ] = rest;
		

	}]));

	describe('with a predefined search', ( ) => {

		let scope,
			ctrl;

		beforeEach( ( ) => {

			let el = angular.element('<zs-dashboard-widget-search-result widget-data="widgetData"></zs-dashboard-widget-search-result>');

			scope = $rootScope.$new();

			scope.widgetData = { search_id: 'all', limit: 10 };

			$httpBackend.whenGET(/\/api\/v1\/session\/current/)
				.respond([ getUser() ]);

			$httpBackend.whenGET(/\/api\/search\/config\/case/)
				.respond([ getConfig() ]);

			$httpBackend.whenGET(/\/api\/object\/search/)
				.respond([]);

			$compile(el)(scope);

			ctrl = el.controller('zsDashboardWidgetSearchResult');

		});

		it('should be loading before it\'s flushed', ( ) => {

			expect(ctrl.isLoading()).toBe(true);

			$httpBackend.flush();

		});

		describe('after flushing', ( ) => {

			beforeEach( ( ) => {

				$httpBackend.flush();

			});


			it('should return the columns from the config', ( ) => {

				let columns = getConfig().columns,
					expected = uniq(columns.default.concat(columns.always));

				expect(map(ctrl.getColumns(), 'id')).toEqual(expected);

			});

			it('should not be loading', ( ) => {

				expect(ctrl.isLoading()).toBe(false);

			});

			describe('without items', ( ) => {

				it('should return an empty array when getting the items', ( ) => {

					expect(ctrl.getRows().length).toBe(0);

				});

			});


		});
		

	});

	describe('with a user defined search', ( ) => {

		let scope;

		beforeEach( ( ) => {

			let el = angular.element('<zs-dashboard-widget-search-result widget-data="widgetData"></zs-dashboard-widget-search-result>'),
				id = shortid();

			scope = $rootScope.$new();

			scope.widgetData = { search_id: id, limit: 10 };

			$compile(el)(scope);

			$httpBackend.whenGET(/\/api\/v1\/session\/current/)
				.respond([ getUser() ]);

			$httpBackend.expectGET(/\/api\/object\/search/)
				.respond([ getSearch(id) ]);

			$httpBackend.expectGET(/\/api\/search\/config\/case/)
				.respond([ getConfig() ]);

			$httpBackend.expectGET(/\/api\/object\/search/)
				.respond([]);

		});

		it('should fulfill all expectations', ( ) => {

			$httpBackend.flush();

			$httpBackend.verifyNoOutstandingRequest();
			$httpBackend.verifyNoOutstandingExpectation();

		});

	});
	
	afterEach( ( ) => {

		apiCacher.clear();
		mutationService.clear();

	});
});
