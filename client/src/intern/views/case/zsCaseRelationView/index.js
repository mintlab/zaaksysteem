import angular from 'angular';
import auxiliaryRouteModule from './../../../../shared/util/route/auxiliaryRoute';
import angularUiRouterModule from 'angular-ui-router';
import resourceModule from './../../../../shared/api/resource';
import composedReducerModule from './../../../../shared/api/resource/composedReducer';
import zsTableModule from './../../../../shared/ui/zsTable';
import zsProgressBarModule from './../../../../shared/ui/zsProgressBar';
import actionsModule from './actions';
import zsConfirmModule from './../../../../shared/ui/zsConfirm';
import zsModalModule from './../../../../shared/ui/zsModal';
import vormFormModule from './../../../../shared/vorm/vormForm';
import contextualActionServiceModule from './../../../../shared/ui/zsContextualActionMenu/contextualActionService';
import angularDragulaModule from 'angular-dragula';
import shortid from 'shortid';
import each from 'lodash/each';
import flatten from 'lodash/flatten';
import assign from 'lodash/assign';
import uniqBy from 'lodash/uniqBy';
import get from 'lodash/get';
import identity from 'lodash/identity';
import capitalize from 'lodash/capitalize';
import seamlessImmutable from 'seamless-immutable';
import plannedCaseForm from './../zsCasePlan/form';
import relatedSubjectFormCtrl from './../zsCaseAddSubject/form';
import createMagicStringResource from './../zsCaseAddSubject/createMagicStringResource';
import template from './template.html';
import './styles.scss';

export default
	angular.module('zsCaseRelationView', [
		angularDragulaModule(angular),
		angularUiRouterModule,
		auxiliaryRouteModule,
		composedReducerModule,
		zsTableModule,
		zsProgressBarModule,
		actionsModule,
		zsConfirmModule,
		zsModalModule,
		vormFormModule,
		contextualActionServiceModule,
		resourceModule
	])
		.directive(
			'zsCaseRelationView',
			[ '$q', '$compile', '$state', '$timeout', 'resource', 'composedReducer', 'dateFilter', 'zsConfirm', 'zsModal', 'contextualActionService', 'auxiliaryRouteService',
			( $q, $compile, $state, $timeout, resource, composedReducer, dateFilter, zsConfirm, zsModal, contextualActionService, auxiliaryRouteService ) => {

			let caseColumns =
					[
						{
							id: 'number',
							label: 'Nr',
							template: '<span>{{::item.number}}</span>'
						},
						{
							id: 'progress',
							label: 'Voortgang',
							template: '<zs-progress-bar progress="{{::item.progress}}"></zs-progress-bar>'
						},
						{
							id: 'casetype',
							label: 'Zaaktype',
							template: '<span>{{::item.casetype}}</span>'
						},
						{
							id: 'subject',
							label: 'Extra informatie',
							template: '<span>{{::item.subject}}</span>'
						},
						{
							id: 'assignee',
							label: 'Behandelaar',
							template: '<a href="/betrokkene/get/{{::item.assigneeId}}">{{::item.assigneeName}}</a>'
						},
						{
							id: 'result',
							label: 'Resultaat',
							template: '<span>{{::item.result}}</span>'
						}

					];

			return {
				restrict: 'E',
				template,
				scope: {
					relatedCases: '&',
					childCases: '&',
					parentCase: '&',
					currentCase: '&',
					siblingCases: '&',
					relatedObjects: '&',
					plannedCases: '&',
					relatedSubjects: '&',
					plannedEmails: '&',
					canRelate: '&',
					onCaseRelate: '&',
					onCaseUnrelate: '&',
					onCaseReorder: '&',
					onPlannedCaseUpdate: '&',
					onPlannedCaseRemove: '&',
					onObjectRelate: '&',
					onRelatedSubjectUpdate: '&',
					onRelatedSubjectRemove: '&',
					onEmailReschedule: '&',
					supportsPlannedEmails: '&',
					hasRelatableObjectTypes: '&',
					disabled: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						tableReducer,
						listeners = [];

					let mapCase = ( caseObj ) => {

						return caseObj ?
							{
								id: shortid(),
								number: caseObj.instance.number,
								progress: caseObj.instance.progress_status,
								casetype: caseObj.instance.casetype.instance.name,
								subject: caseObj.instance.subject || '',
								assigneeId: get(caseObj.instance.assignee, 'instance.id'),
								assigneeName: get(caseObj.instance.assignee, 'instance.assignee'),
								result: capitalize(caseObj.instance.result) || 'Onbekend',
								reference: caseObj.reference,
								href: $state.href('case', { caseId: caseObj.instance.number }),
								relation_type_label: caseObj.relation_type === 'initiator' ?
									'Initiator'
									: (caseObj.relation_type === 'continuation' ? 'Vervolgzaak' : 'Gerelateerd')
							}
							: null;

					};

					let openForm = ( options ) => {

						return $q( ( resolve, reject ) => {

							let modalScope = scope.$new(true),
								modal;

							modalScope.vm = {
								fields: options.form.fields,
								defaults: seamlessImmutable(options.form.defaults || {}).merge(options.defaults),
								actions: [
									{
										type: 'submit',
										label: 'Opslaan',
										click: [ '$values', ( values ) => {

											modal.hide();

											$q.when(resolve(values))
												.then(( ) => {
													modalScope.$destroy();
												})
												.catch( ( err ) => {

													modalScope.show();

													return $q.reject(err);
												});

										} ]
									}
								],
								handleChange: ( name, value, values ) => {

									return options.form.processChange ?
										options.form.processChange(name, value, values)
										: values;

								}
							};

							modal = zsModal({
								title: options.title,
								el: $compile(
									`<vorm-form
										data-fields="vm.fields"
										data-defaults="vm.defaults"
										data-actions="vm.actions"
										on-change="vm.handleChange($name, $value, $values)"
									>
									</vorm-form>`
								)(modalScope)
							});

							modal.open();

							modal.onClose( ( ) => {

								modalScope.$destroy();

								return false;

							});

							modalScope.$on('$destroy', ( ) => {

								modal.close();

								reject();

							});

						});

					};

					tableReducer = composedReducer( { scope }, ctrl.currentCase, ctrl.siblingCases, ctrl.relatedCases, ctrl.childCases, ctrl.parentCase, ctrl.relatedObjects, ctrl.relatedSubjects, ctrl.plannedCases, ctrl.plannedEmails, ctrl.disabled, ctrl.supportsPlannedEmails, ctrl.hasRelatableObjectTypes, ctrl.canRelate)
						.reduce( ( currentCase, siblingCases = [], relatedCases = [], childCases = [], parentCase = null, relatedObjects = [], relatedSubjects = [], plannedCases = [], plannedEmails = [], isDisabled, supportsPlannedEmails, hasRelatableObjectTypes, canRelate ) => {

							let tables =
								[
									{
										name: 'sub-cases',
										label: 'Hoofd- en deelzaken',
										columns:
											[
												{
													id: 'level',
													label: 'Niveau',
													template: '<span>{{::item.level}}</span>'
												}
											].concat(caseColumns),
										items:
											uniqBy(
												flatten(
													[ parentCase ? [ parentCase ] : null, [ currentCase, ...siblingCases ], childCases ].filter(identity)
														.map(( cases, level ) => {

															return cases.filter(identity).map(
																caseObj => {

																	return assign({
																		level: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('')[level]
																	}, mapCase(caseObj));
																}
															);
														})
												),
												'reference'
											),
										actions: [],
										href: true
									},
									{
										name: 'related-cases',
										label: 'Gerelateerde zaken',
										columns:
											[
												{
													id: 'drag',
													label: '',
													template:
														`<div class="widget-favorite-drag list-item-drag">
															<zs-icon icon-type="drag-vertical"></zs-icon>
														</div>`
												},
												{
													id: 'type',
													label: 'Type',
													template: '<span>{{::item.relation_type_label}}</span>'
												}
											].concat(caseColumns),
										items: relatedCases.map(mapCase),
										actions:
											canRelate ?
												[
													{
														name: 'remove',
														type: 'click',
														label: 'Verbreek relatie',
														icon: 'close',
														click: ( item, event ) => {

															event.stopPropagation();
															event.preventDefault();

															zsConfirm('Weet u zeker dat u de relatie met deze zaak wilt verbreken?', 'Verwijderen')
																.then( ( ) => {
																	ctrl.onCaseUnrelate( { $reference: item.reference });
																});
														}
													}
												] : [ ],
										add: {
											type: 'suggest',
											data: {
												type: 'case',
												placeholder: 'Begin te typen',
												onSelect: ( item ) => {

													ctrl.onCaseRelate({ $reference: item.id });

												}
											}
										},
										href: true
									},
									{
										name: 'planned-cases',
										label: 'Geplande zaken',
										columns:
											[
												{
													id: 'label',
													label: 'Zaaktype'
												},
												{
													id: 'pattern',
													label: 'Patroon'
												},
												{
													id: 'reach',
													label: 'Bereik'
												}
											],
										items: (plannedCases || []).map(
											scheduledJob => {

												let pattern,
													moreThanOnce = scheduledJob.instance.interval_value > 1,
													val = scheduledJob.instance.interval_value,
													reach;
												
												switch (scheduledJob.instance.interval_period) {
													case 'once':
													pattern = 'Eenmalig';
													break;

													case 'days':
													pattern = `Elke ${moreThanOnce ? `${val} dagen` : 'dag'}`;
													break;

													case 'weeks':
													pattern = `Elke ${moreThanOnce ? `${val} weken` : 'week'}`;
													break;

													case 'months':
													pattern = `Elke ${moreThanOnce ? `${val} maanden` : 'maand'}`;
													break;

													case 'years':
													pattern = `Elk${moreThanOnce ? 'e' : ''} ${moreThanOnce ? `${val} jaren` : 'jaar'}`;
													break;
												}
												
												reach = dateFilter(scheduledJob.instance.next_run, 'dd-MM-yyyy');
												
												if (scheduledJob.instance.runs_left > 1) {
													reach = `Start op ${reach} (${scheduledJob.instance.runs_left} herhalingen)`;
												}
												
												return {
													id: shortid(),
													label: scheduledJob.label,
													pattern,
													reach,
													source: scheduledJob
												};

											}
										),
										actions: [
											{
												name: 'edit',
												label: 'Bewerken',
												type: 'click',
												icon: 'pencil',
												click: ( item, event ) => {

													event.stopPropagation();
													event.preventDefault();

													openForm(
														{
															form:
																assign(
																	{},
																	plannedCaseForm,
																	{
																		fields: plannedCaseForm.fields.filter(
																			field => [ 'from', 'pattern', 'has_pattern' ].indexOf(field.name) !== -1
																		)
																	}
																),
															title: 'Geplande zaak bewerken',
															defaults:
																{
																	from: new Date(item.source.instance.next_run),
																	has_pattern: item.source.instance.interval_period !== 'once',
																	pattern: {
																		every: {
																			count: Number(item.source.instance.interval_value) || 1,
																			type: item.source.instance.interval_period || 'days'
																		},
																		repeat: Number(item.source.instance.runs_left)
																	}
																}
														}
													)
														.then(( values ) => {

															return ctrl.onPlannedCaseUpdate(
																{
																	$id: item.source.reference,
																	$values: assign({}, item.source.instance, {
																		// incorrectly required by API, waiting on a fix
																		type: 'scheduled_job',
																		copy_relations: true,
																		//
																		next_run: values.from.toISOString(),
																		interval_period: values.has_pattern ?
																			get(values, 'pattern.every.type')
																			: 'once',
																		interval_value: values.has_pattern ?
																			Number(get(values, 'pattern.every.count'))
																			: 1,
																		runs_left: values.has_pattern ?
																			Number(get(values, 'pattern.repeat'))
																			: 1
																	})
																}
															);

														});

												}
											},
											{
												name: 'remove',
												label: 'Verwijderen',
												type: 'click',
												icon: 'close',
												click: ( item, event ) => {

													event.stopPropagation();
													event.preventDefault();

													zsConfirm('Weet u zeker dat u de geplande zaak wilt verwijderen?', 'Verwijderen')
														.then( ( ) => {
															ctrl.onPlannedCaseRemove({ $id: item.source.reference });
														});

												}
											}
										],
										collectionActions: [
											{
												type: 'button',
												data: {
													label: 'Voeg toe',
													click: ( ) => {

														contextualActionService.openAction(
															contextualActionService.findActionByName('geplande-zaak')
														);

													}
												},
												visible: !isDisabled
											},
											{
												type: 'link',
												data: {
													href: `/api/object/search/?zapi_no_pager=1&zapi_format=csv&zql=SELECT {} FROM scheduled_job WHERE object.uuid in ("${plannedCases.map(p => p.reference).join('","')}")`,
													label: 'Download als .csv'
												},
												visible: plannedCases.length
											}
										].filter(action => action.visible === undefined || action.visible)
									},
									{
										name: 'related_subjects',
										label: 'Betrokkenen',
										columns: [
											{
												id: 'type',
												label: 'Type'
											},
											{
												id: 'name',
												label: 'Naam'
											},
											{
												id: 'role',
												label: 'Rol'
											},
											{
												id: 'pip_authorized',
												label: 'Gemachtigd',
												template: '<zs-icon icon-type="{{::item.icon}}"></zs-icon>'
											}
										],
										items: relatedSubjects.map(relatedSubject => {

											return {
												id: shortid(),
												type: capitalize(relatedSubject.betrokkene_type.replace(/_/g, ' ')),
												name: relatedSubject.name,
												role: relatedSubject.role,
												icon: relatedSubject.pip_authorized ?
													'check'
													: '',
												source: relatedSubject
											};

										}),
										actions: [
											{
												name: 'update',
												label: 'Bewerken',
												type: 'click',
												icon: 'pencil',
												click: ( item, event ) => {

													event.stopPropagation();
													event.preventDefault();

													let defaults = seamlessImmutable({
															relation_type: item.source.betrokkene_type,
															related_subject: {
																data: {
																	id: item.source.betrokkene_id,
																	type: item.source.betrokkene_type
																},
																label: item.source.name
															},
															related_subject_role: item.source.role,
															related_subject_role_freeform: item.source.role,
															magic_string_prefix: item.source.magic_string_prefix,
															pip_authorized: item.source.pip_authorized,
															notify_subject: false
														}),
														values = defaults,
														magicStringResource =
															createMagicStringResource(
																resource,
																scope,
																( ) => {
																	return {
																		role:
																			values.related_subject_role_freeform === item.source.role ?
																				null
																				: values.related_subject_role_freeform,
																		caseId: ctrl.currentCase().instance.number
																	};
																}
														),
														formCtrl = relatedSubjectFormCtrl(magicStringResource),
														promise;

													openForm(
														{
															form:
																assign(
																	{},
																	formCtrl,
																	{
																		processChange: ( name, value, vals ) => {

																			values = vals;

																			if (name === 'related_subject_role' && value !== 'Anders') {
																				values = values.merge({ related_subject_role_freeform: value });
																			}

																			if ((name === 'related_subject_role' || name === 'related_subject_role_freeform') && value === item.source.role) {
																				values = values.merge({ magic_string_prefix: item.source.magic_string_prefix });
																			}

																			// todo: clean this up

																			promise = (promise || $timeout(angular.noop, 0))
																				.then(( ) => {
																					return magicStringResource.reload();
																				})
																				.then(( ) => {
																					return magicStringResource.data() ? values.merge( { magic_string_prefix: magicStringResource.data() }) : values;
																				})
																				.catch( ( ) => {
																					return values.merge( { magic_string_prefix: null });
																				})
																				.finally( ( ) => {
																					promise = null;
																				});

																			return promise;

																		},
																		fields: formCtrl.fields.filter(
																			field => field.name !== 'notify_subject'
																				&& field.name !== 'relation_type'
																		)
																			.map(field => {

																				return field.name === 'related_subject' || field.name === 'related_subject_type' ?
																					assign({}, field, { disabled: true })
																					: field;
																			})
																	}
																),
															title: 'Betrokkene bewerken',
															defaults
														}
													)
														.then( vals => {

															ctrl.onRelatedSubjectUpdate({
																$id: item.source.id,
																$values: assign(
																	{},
																	item.source,
																	{
																		magic_string_prefix: vals.magic_string_prefix,
																		role:
																			vals.related_subject_role !== 'Anders' ?
																				vals.related_subject_role
																				: vals.related_subject_role_freeform,
																		notify_subject: !!vals.notify_subject,
																		pip_authorized: !!vals.pip_authorized
																	}
																)
															});

														})
														.finally( ( ) => {

															magicStringResource.destroy();

														});

												}
											},
											{
												name: 'remove',
												label: 'Verwijderen',
												type: 'click',
												icon: 'close',
												click: ( item, event ) => {

													event.stopPropagation();
													event.preventDefault();

													zsConfirm('Weet u zeker dat u deze betrokkene wilt verwijderen?', 'Verwijderen')
														.then(( ) => {
															ctrl.onRelatedSubjectRemove({ $id: item.source.id });
														});

												}
											}
										],
										collectionActions:
											isDisabled ?
												[]
												: [
													{
														type: 'button',
														data: {
															label: 'Voeg toe',
															click: ( ) => {

																contextualActionService.openAction(
																	contextualActionService.findActionByName('betrokkene')
																);

															}
														}
													}
												]
									},
									{
										name: 'related_objects',
										label: 'Gerelateerde objecten',
										href: true,
										columns: [
											{
												id: 'type',
												label: 'Type'
											},
											{
												id: 'label',
												label: 'Naam'
											}
										],
										items: relatedObjects.map(object => {

											return {
												id: shortid(),
												type: object.related_object_type_label,
												label: object.label,
												href: `/object/${object.reference}`
											};

										}),
										collectionActions: hasRelatableObjectTypes ?
											[
												{
													type: 'link',
													data: {
														label: 'Relateer object',
														href: $state.href(
															auxiliaryRouteService.append($state.current, 'admin'),
															{ action: 'object-relateren' },
															{ inherit: true }
														)
													}
												}
											]
											: []
									},
									{
										name: 'planned-emails',
										label: 'Geplande e-mails',
										columns: [
											{
												id: 'date',
												label: 'Ingepland voor',
												template:
													'<span>{{::item.instance.date | date:\'dd-MM-yyyy HH:mm\'}}</span>'
											},
											{
												id: 'label',
												label: 'Sjabloon',
												resolve: 'instance.label'
											},
											{
												id: 'recipient',
												label: 'Ontvanger',
												resolve: 'instance.recipient'
											}
										],
										items: (plannedEmails || []).map(
											email => email.merge({ id: shortid() })
										),
										collectionActions:
											isDisabled ?
												[ ]
												: [
													{
														type: 'button',
														data: {
															label: 'Herplannen',
															click: ( ) => {

																ctrl.onEmailReschedule();

															}
														}
													}
												],
										visible: !!supportsPlannedEmails
									}
								];

							tables = tables.filter(table => table.visible || table.visible === undefined)
								.map(table => {
									let config;

									config = assign(
										{},
										table,
										{
											columns: table.columns.map(
												col => {

													let resolve = col.resolve || col.id;

													return assign(
														{
															template: `<span>{{::item.${resolve}}}</span>`
														},
														col
													);

												}
											)
										}
									);

									if (config.actions && !isDisabled || canRelate) {

										config = assign(
											{},
											config,
											{
												columns: config.columns.concat(
													{
														id: 'actions',
														label: '',
														template:
															`<button
																ng-repeat="action in item.actions track by action.name"
																type="button"
																class="button-reset btn-icon"
																ng-click="action.click(item, $event)"
																zs-tooltip="{{::action.label}}"
																zs-tooltip-options="{ attachment: 'right middle', target: 'left middle', flip: true, offset: { x: -10, y: 0 } }"
															>
																<zs-icon icon-type="{{::action.icon}}"></zs-icon>
															</button>`
													}
												),
												items: config.items.map(
													item => assign({ actions: config.actions }, item)
												)
											}
										);
									}

									if ( (table.name === 'related-cases' && !canRelate) || (table.name !== 'related-cases' && isDisabled)) {
										config = assign({}, config, { add: null });
									}

									return config;
								}
							);

							return tables;

						});

					ctrl.getTables = tableReducer.data;

					tableReducer.subscribe( ( ) => {

						each(listeners, fn => {
							fn();
						});

						listeners = tableReducer.data()
							.map( ( table ) => {

								return scope.$on(`${table.name}.drop`, ( event, draggedElement ) => {

									let after = draggedElement[0].previousElementSibling,
										elScope = angular.element(draggedElement).scope(),
										currentCase,
										prevCaseId,
										caseId;

									if (!elScope) {
										return;
									}

									currentCase = elScope.item;

									if (!currentCase) {
										return;
									}

									if (after && after.nodeName !== '#comment') {
										prevCaseId = angular.element(after).scope().item.reference;
									}

									caseId = currentCase.reference;

									ctrl.onCaseReorder( { $reference: caseId, $after: prevCaseId });

								});

							});
					});

				}],
				controllerAs: 'vm'
			};

		}])
		.directive('zsCaseRelationViewTable', [ 'dragulaService', ( dragulaService ) => {

			return {
				restrict: 'A',
				link: ( scope, element, attrs ) => {

					let bagId = attrs.zsCaseRelationViewTable;

					dragulaService.options(scope, bagId, {
						moves: ( el, container, handle ) => {
							return handle.classList.contains('mdi-drag-vertical');
						},
						containers: [ element.find('zs-table-body')[0] ],
						isContainer: ( el ) => {
							let isContainer = element[0].contains(el) && el.classList.contains('table-body');

							return isContainer;
						}
					});

					scope.$on('$destroy', ( ) => {
						dragulaService.destroy(scope, bagId);
					});

				}
			};

		}])
		.name;
