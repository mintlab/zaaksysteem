import angular from 'angular';
import template from './template.html';
import composedReducerModule from './../../../../../shared/api/resource/composedReducer';
import vormFieldsetModule from './../../../../../shared/vorm/vormFieldset';
import vormInvokeModule from './../../../../../shared/vorm/vormInvoke';
import inputModule from './../../../../../shared/vorm/types/input';
import radioModule from './../../../../../shared/vorm/types/radio';
import formModule from './../../../../../shared/vorm/types/form';
import selectModule from './../../../../../shared/vorm/types/select';
import vormRolePickerModule from './../../../../../shared/zs/vorm/vormRolePicker';
import vormObjectSuggestModule from './../../../../../shared/object/vormObjectSuggest';
import vormValidatorModule from './../../../../../shared/vorm/util/vormValidator';
import caseAttrTemplateCompilerModule from './../../../../../shared/case/caseAttrTemplateCompiler';
import zsCaseAdminAttributeListModule from './zsCaseAdminAttributeList';
import caseActions from './../../../../../shared/case/caseActions';
import actionsModule from './actions';
import mapValues from 'lodash/mapValues';
import keyBy from 'lodash/keyBy';
import get from 'lodash/get';
import find from 'lodash/find';
import includes from 'lodash/includes';
import seamlessImmutable from 'seamless-immutable';
import './styles.scss';

export default
	angular.module('zsCaseAdminView', [
		composedReducerModule,
		vormFieldsetModule,
		vormInvokeModule,
		actionsModule,
		inputModule,
		radioModule,
		formModule,
		selectModule,
		vormRolePickerModule,
		vormObjectSuggestModule,
		vormValidatorModule,
		caseAttrTemplateCompilerModule,
		zsCaseAdminAttributeListModule
	])
		.directive('zsCaseAdminView', [
			'$state', '$animate', 'composedReducer', 'vormValidator', 'caseAttrTemplateCompiler', 'vormInvoke',
			( $state, $animate, composedReducer, vormValidator, caseAttrTemplateCompiler, vormInvoke ) => {

			let compiler = caseAttrTemplateCompiler.clone();

			compiler.registerType('case-admin-attribute-list', {
				control:
					angular.element(
						`<zs-case-admin-attribute-list
							ng-model
							compiler="vm.compiler()"
						>
						</zs-case-admin-attribute-list>`
					)
			});

			return {
				restrict: 'E',
				template,
				scope: {
					action: '&',
					caseResource: '&',
					casetypeResource: '&',
					user: '&',
					onSubmit: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( scope ) {

					let ctrl = this,
						submitting = false,
						values = seamlessImmutable({}).merge(
							mapValues(
								keyBy(ctrl.action().fields, 'name'),
								'defaults'
							)
						),
						needsCasetype = includes([ 'fase', 'afhandelen', 'kenmerken', 'object-relateren', 'resultaat' ], ctrl.action().name),
						actionReducer,
						fieldReducer,
						validityReducer,
						messageReducer,
						args;

					args = [ ctrl.action(), ctrl.caseResource(), ctrl.user() ];

					if (needsCasetype) {
						args = args.concat(ctrl.casetypeResource());
					}

					actionReducer = composedReducer({ scope }, ...args)
						.reduce( ( action, caseObj, user, casetype ) => {

							let updatedAction = find(caseActions({ caseObj, casetype, user, $state }), { name: action.name });

							return updatedAction;

						});

					fieldReducer = composedReducer({ scope }, actionReducer)
						.reduce( ( action ) => {

							let fields = get(action, 'fields', []);

							return seamlessImmutable(fields).asMutable( { deep: true });
						});

					validityReducer = composedReducer({ scope }, ( ) => values, fieldReducer)
						.reduce( ( vals, fields ) => {
							let validation = vormValidator(fields, vals);

							return validation;
						});

					messageReducer = composedReducer( { scope }, actionReducer, ( ) => values)
						.reduce(( action, vals ) => {

							let messages = [];

							if (!action) {
								messages = messages.concat({
									name: 'error',
									icon: 'alert-circle',
									classes: {
										error: true
									},
									label: 'De actie kon niet worden geladen. Mogelijk heeft u niet voldoende rechten om deze uit te voeren.'
								});
							} else {
								messages = messages.concat(
									vormInvoke(action.messages, { $values: vals }) || []
								);
							}

							return messages;

						});

					ctrl.getFields = fieldReducer.data;

					ctrl.isFormValid = ( ) => get(validityReducer.data(), 'valid', false);

					ctrl.getValidity = validityReducer.data;

					ctrl.getSubmitLabel = composedReducer({ scope }, ctrl.action)
						.reduce( ( action ) => action.verb || action.label)
						.data;

					ctrl.handleSubmit = ( ) => {

						let action = actionReducer.data(),
							mutation = action.mutate(values),
							promise,
							opts = typeof action.options === 'function' ? action.options(values) : action.options;

						if (!opts) {
							opts = {};
						}

						submitting = true;

						promise = ctrl.caseResource().mutate(mutation.type, mutation.data).asPromise()
							.finally(( ) => {
								submitting = false;
							});

						ctrl.onSubmit({ $promise: promise, $reload: !!opts.reloadRoute, $willRedirect: !!opts.willRedirect });

					};

					ctrl.getValues = ( ) => values;

					ctrl.handleChange = ( name, value ) => {

						let action = actionReducer.data();

						values = values.merge({ [name]: value });

						if (action.processChange) {
							values = action.processChange(name, value, values);
						}


					};

					ctrl.getAction = actionReducer.data;

					ctrl.hasWarning = ( ) => actionReducer.state() === 'resolved' && !ctrl.getAction();

					ctrl.isSubmitting = ( ) => submitting;

					ctrl.isLoading = ( ) => {
						let isCaseResolved = ctrl.caseResource().state() === 'resolved',
							isCasetypeResolved = ctrl.casetypeResource().state() === 'resolved';

						return needsCasetype ?
							!(isCaseResolved && isCasetypeResolved)
							: !isCaseResolved;
					};

					ctrl.getCompiler = ( ) => compiler;

					ctrl.getMessages = messageReducer.data;

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
