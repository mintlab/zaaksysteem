import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import composedReducerModule from './../../../../../../../shared/api/resource/composedReducer';
import zsTooltipModule from './../../../../../../../shared/ui/zsTooltip';
import zsModalModule from './../../../../../../../shared/ui/zsModal';
import zsCaseActionFormModule from './zsCaseActionForm';
import snackbarServiceModule from './../../../../../../../shared/ui/zsSnackbar/snackbarService';
import find from 'lodash/find';
import keyBy from 'lodash/keyBy';
import each from 'lodash/each';
import get from 'lodash/get';
import assign from 'lodash/assign';
import sortBy from 'lodash/sortBy';
import _keys from 'lodash/keys';
import pickBy from 'lodash/pickBy';
import first from 'lodash/head';
import identity from 'lodash/identity';
import template from './template.html';

export default
	angular.module('zsCaseActionList', [
		angularUiRouterModule,
		composedReducerModule,
		zsTooltipModule,
		zsModalModule,
		zsCaseActionFormModule,
		snackbarServiceModule
	])
		.directive('zsCaseActionList', [ '$state', '$compile', '$q', 'composedReducer', 'zsModal', 'snackbarService', ( $state, $compile, $q, composedReducer, zsModal, snackbarService ) => {

			let iconMappings = {
				template: 'file-outline',
				email: 'email',
				allocation: 'account-multiple',
				case: ( action ) => {

					let icon;

					switch (action.data.relatie_type) {
						case 'deelzaak':
						icon = 'folder-download';
						break;

						case 'vervolgzaak':
						case 'vervolgzaak_datum':
						icon = 'folder-move';
						break;

						case 'gerelateerd':
						icon = 'folder-multiple-outline';
						break;
					}

					return icon;
				},
				defaults: 'play-circle-outline'
			};

			let getIcon = ( action ) => {
				let getter = iconMappings[action.type] || iconMappings.defaults,
					icon = getter;

				if (typeof getter === 'function') {
					icon = getter(action);
				}

				return icon;
			};

			return {
				restrict: 'E',
				template,
				scope: {
					actions: '&',
					onActionAutomaticToggle: '&',
					onActionUntaint: '&',
					onActionTrigger: '&',
					requestor: '&',
					recipient: '&',
					assignee: '&',
					phases: '&',
					templates: '&',
					caseDocuments: '&',
					disabled: '&'
				},
				bindToController: true,
				controller: [ '$scope', function ( $scope ) {

					let ctrl = this,
						actionReducer;

					actionReducer = composedReducer({ scope: $scope }, ctrl.actions, ctrl.disabled)
						.reduce( ( actions, disabled ) => {

							let sortedActions = sortBy(actions, ( action ) => [ 'template', 'email', 'case', 'allocation' ].indexOf(action.type) );

							return sortedActions.map(action => {

								return {
									id: action.id,
									type: action.type,
									icon: getIcon(action),
									label: action.label,
									checked: action.automatic,
									tainted: action.tainted,
									description: action.description,
									disabled: disabled || get(action, 'data.relation_type') === 'deelzaak',
									classes: {
										disabled
									}
								};
							});
						});

					ctrl.getActions = actionReducer.data;

					ctrl.handleActionClick = ( action ) => {

						if (action.disabled) {
							return;
						}

						let modal,
							scope = $scope.$new(true),
							el = angular.element(
								`<zs-case-action-form
									data-action="action"
									on-action-save="handleActionSave($data)"
									on-action-execute="handleActionExecute($data)"
								></zs-case-action-form>`
							),
							keys = keyBy('requestor recipient assignee phases templates case-documents'.split(' '));

						scope.vm = ctrl;
						scope.action = find(ctrl.actions(), { id: action.id });

						let startAsync = ( type, data ) => {

							let promise,
								verb,
								redirect = {
									allocate: (type === 'execute' && scope.action.type === 'allocation'),
									template: (type === 'execute' && scope.action.type === 'template')
								},
								reason = first(_keys(pickBy(redirect, identity))),
								newState;

							if (type === 'save') {
								verb = 'opgeslagen';
							} else if (type === 'execute') {
								verb = 'uitgevoerd';
							}

							if (redirect.allocate) {
								newState = 'home';
							}

							modal.hide();

							promise = ctrl.onActionTrigger({ $action: scope.action, $trigger: type, $data: assign({}, action.data, data) })
								.then(( responseData ) => {
									modal.close();
									scope.$destroy();
									return $q.resolve(responseData);
								})
								.catch(( err ) => {
									modal.show();
									return $q.reject(err);
								});

							snackbarService.wait(`De actie wordt ${verb}.`, {
								promise,
								then: ( mutationData ) => {

									let msg,
										actions;

									switch (reason) {

										case 'allocate':
										msg = 'De toewijzing is gewijzigd. U bent doorverwezen naar het dashboard.';
										break;

										case 'template':
										msg = 'Uw document is aangemaakt.';
										actions = $state.current.name === 'case.docs' ?
											null
											: [
												{
													type: 'link',
													label: 'Documenten bekijken',
													link: $state.href('case.docs', null, { reload: true })
												}
											];
										break;

										default:
										msg = get(mutationData, 'flash_message') || `De actie is succesvol ${verb}`;
										actions = [];
										break;

									}
									
									return {
										message: msg,
										actions
									};
								},
								catch: ( ) => `De actie kon niet worden ${verb}. Neem contact op met uw beheerder voor meer informatie.`
							})
								.then( ( ) => {

									if (newState) {

										$state.go(newState);

									}

								});
						};

						scope.handleActionSave = ( data ) => {

							startAsync('save', data);
						};

						scope.handleActionExecute = ( data ) => {

							startAsync('execute', data);

						};

						each(keys, attr => {

							let value = attr.replace(/-([a-z])/g, ( str, match ) => {
								return match.toUpperCase();
							});

							el.attr(`data-${attr}`, `vm.${value}()`);
						});

						modal = zsModal({
							title: `${action.label}: ${action.description}`,
							el: $compile(el)(scope),
							classes: action.type
						});

						modal.open();

						modal.onClose(( ) => {
							scope.$destroy();
						});

					};

					ctrl.handleActionToggle = ( action, $event ) => {

						$event.stopPropagation();

						ctrl.onActionAutomaticToggle({
							$actionId: action.id,
							$automatic: !action.checked
						});

					};

					ctrl.handleUntaintClick = ( action, $event ) => {

						$event.stopPropagation();

						ctrl.onActionUntaint({
							$actionId: action.id
						});

					};

				}],
				controllerAs: 'vm'
			};

		}])
		.name;
