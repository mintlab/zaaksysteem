import angular from 'angular';
import 'angular-i18n/angular-locale_nl';
import ngAnimate from 'angular-animate';
import routing from './routing';
import resourceModule from './../shared/api/resource';
import zsResourceConfiguration from './../shared/api/resource/resourceReducer/zsResourceConfiguration';
import serviceworkerModule from './../shared/util/serviceworker';
import zsVormTemplateModifierModule from './../shared/zs/vorm/zsVormTemplateModifier';
import shouldReloadModule from './../shared/util/route/shouldReload';
import zsSnackbarModule from './../shared/ui/zsSnackbar';
import zsUiViewProgressModule from './../shared/ui/zsNProgress/zsUiViewProgress';
import zsStorageModule from './../shared/util/zsStorage';
import isEqual from 'lodash/isEqual';
import omit from 'lodash/omit';
import get from 'lodash/get';
import './styles.scss';

// require all actions to ensure they're available on page load

let context = require.context('./', true, /\/actions\.js$/),
	actionModules = context.keys().map(context);

if (DEV) { //eslint-disable-line
	require('lodash-migrate');
}

export default
	angular.module('Zaaksysteem.intern',
		[
			routing,
			resourceModule,
			serviceworkerModule,
			ngAnimate,
			zsVormTemplateModifierModule,
			shouldReloadModule,
			zsSnackbarModule,
			zsUiViewProgressModule,
			zsStorageModule
		].concat(actionModules)
	)
	.config([ 'apiCacherProvider', 'resourceProvider', ( apiCacherProvider, resourceProvider ) => {

		zsResourceConfiguration(resourceProvider.configure);

		apiCacherProvider.setIsEqual(( a, b ) => {

			let firstVal = a,
				secondVal = b;

			if (firstVal && typeof firstVal !== 'string' && 'request_id' in firstVal) {
				firstVal = omit(firstVal, 'request_id');
			}

			if (secondVal && typeof secondVal !== 'string' && 'request_id' in secondVal) {
				secondVal = omit(secondVal, 'request_id');
			}

			return isEqual(firstVal, secondVal);

		});

	}])

	.config([ '$httpProvider', ( $httpProvider ) => {

		$httpProvider.defaults.withCredentials = true;

		$httpProvider.defaults.headers.common['X-Client-Type'] = 'web';

		$httpProvider.useApplyAsync(true);
			
		$httpProvider.interceptors.push([ 'zsStorage', '$window', '$q', ( zsStorage, $window, $q ) => {

			return {
				responseError: ( rejection ) => {
					
					if (rejection.status === 401
						// configuration_incomplete implies the API is not configured for public accces,
						// which means we don't have a logged in user
						|| get(rejection, 'data.result.instance.type') === 'api/v1/configuration_incomplete'
					) {
						zsStorage.clear();
						$window.location.href = `/auth/login?referer=${$window.location.pathname}`;
					}

					return $q.reject(rejection);
				}
			};

		}] );

	}])
	// ocLazyLoad calls the ngModule config block twice due to
	// a bug in ocLazyLoad and angular-dragula defining ng as a
	// dependency, causing the $$animateJs service provider to be
	// overridden with the defaults, breaking animations
	// so we inject it at runtime immediately to instantiate
	// the factory
	.run([ '$$animateJs', angular.noop ])
	.run([ '$window', 'serviceWorker', '$animate', ( $window, serviceWorker, $animate ) => {

		let enable = PROD, //eslint-disable-line
			worker = serviceWorker('/intern/');

		$animate.enabled(true);
		
		if (worker.isSupported()) {
			worker.isEnabled()
				.then( ( enabled ) => {
					if (enabled !== enable) {
						return (enable ?
							worker.enable()
							: worker.disable()
								.then(( ) => {
									if (enabled) {
										$window.location.reload();
									}
								})
						);
					}
				});
		}

	}])
		.name;
