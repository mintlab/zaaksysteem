import angular from 'angular';
import 'angular-i18n/angular-locale_nl';
import ngAnimate from 'angular-animate';
import routing from './routing';
import resourceModule from './../shared/api/resource';
import zsResourceConfiguration from './../shared/api/resource/resourceReducer/zsResourceConfiguration';
import configServiceModule from './shared/configService';
import appServiceModule from './shared/appService';
import serviceworkerModule from './../shared/util/serviceworker';
import get from 'lodash/get';
import without from 'lodash/without';
import uniq from 'lodash/uniq';
import createManifest from './shared/manifest';
import sessionServiceModule from '../shared/user/sessionService';

export default
	angular.module('Zaaksysteem.mor', [
		routing,
		resourceModule,
		ngAnimate,
		configServiceModule,
		appServiceModule,
		serviceworkerModule,
		sessionServiceModule
	])
	.config(['$provide', ( $provide ) => {
		
		// make sure sourcemaps work
		// from https://github.com/angular/angular.js/issues/5217

		$provide.decorator('$exceptionHandler', [ '$delegate', ( $delegate ) => {

			return ( exception, cause ) => {
				$delegate(exception, cause);
				setTimeout(( ) => {
					console.error(exception.stack);
				});
			};

		}]);

	}])
	.config([ 'appServiceProvider', ( appServiceProvider ) => {

		appServiceProvider.setDefaultState({ filters: [ ] })
			.reduce('filter_add', 'filters', ( filters, filterToAdd ) => uniq(filters.concat(filterToAdd)))
			.reduce('filter_remove', 'filters', ( filters, filterToRemove ) => without(filters, filterToRemove))
			.reduce('filter_clear', 'filters', ( ) => [ ]);
			
	}])
	.config([ 'resourceProvider', ( resourceProvider ) => {

		zsResourceConfiguration(resourceProvider.configure);

	}])
	.run([ '$document', 'configService', ( $document, configService ) => {

		let styleEl;

		configService.getResource().onUpdate( data => {

			createManifest( get(data, 'instance.interface_config'), $document );

			let color = get(data, 'instance.interface_config.accent_color', '#FFF'),
				rules = [
					`#nprogress .bar {
					  background: ${color};
					  height: 100%;
					}`,
					`#nprogress .peg {
					  box-shadow: 0 0 20px ${color}, 0 0 10px #FFF;
					}`,
					`#nprogress .spinner-icon {
					  border-top-color: ${color};
					  border-left-color: ${color};
					}`,
					`case-list-item-list-item .case-list-item--title{
					  color: ${color} !important;
					}`,
					`case-list-item-list-item > a:hover{
						color: ${color} !important;
					}`,
					`.case-list-view__supportlink a{
						color: ${color} !important;
					}`
				];

			if (styleEl) {
				styleEl.remove();
			}

			styleEl = $document[0].createElement('style');

			$document[0].head.appendChild(styleEl);

			rules.forEach(( rule, index ) => {
				styleEl.sheet.insertRule(rule, index);
			});

		});

	}])
	.run([ '$window', 'serviceWorker', '$animate', ( $window, serviceWorker, $animate ) => {

		let enable = PROD, //eslint-disable-line
			worker = serviceWorker();

		$animate.enabled(true);

		if (worker.isSupported()) {
			worker.isEnabled()
				.then( ( enabled ) => {
					if (enabled !== enable) {
						return (enable ?
							worker.enable()
							: worker.disable()
								.then(( ) => {
									if (enabled) {
										$window.location.reload();
									}
								})
						);
					}
				});
		}

	}])
		.name;
