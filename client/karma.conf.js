/*global module,require*/
var merge = require("lodash/merge"),
	HtmlWebpackPlugin = require('html-webpack-plugin'),
	webpackConfig = merge(require('./webpack.config.js'),
		{
			devtool: 'eval-source-map',
			module: {
				preLoaders: [
					{
						test: /\.js$/,
						loader: 'isparta',
						exclude: [ /bower_components/, /node_modules/, /test/ ]
					}
				]
			}
		}
	);

// HtmlWebpackPlugin causes errors
webpackConfig.plugins = webpackConfig.plugins.filter( ( plugin ) => {
	return !(plugin instanceof HtmlWebpackPlugin);
});

module.exports = function ( config ) {
	
	config.set({
		basePath: '.',
		frameworks: [ 'jasmine' ],
		browsers: [ 'PhantomJS'],
		plugins: [
			require('karma-phantomjs-launcher'),
			require('karma-jasmine'),
			require('karma-webpack'),
			require('karma-coverage'),
			require('phantomjs-polyfill'),
			require('karma-sourcemap-loader')
		],
		reporters: [ 'dots', 'coverage' ],
		preprocessors: {
			'test/unit/index.js': [ 'webpack', 'sourcemap' ]
		},
		files: [ 'test/unit/index.js' ],
		webpack: webpackConfig,
		webpackServer: {
			noInfo: true
		},
		coverageReporter: {
			includeAllSources: true
		}
	});
	
};
